/**
 * Created by Sourav on 8/31/2017.
 */

$(function () {
    $(".content_inner").click(function () {
        var container_title = $(this).attr("title");
        var title = container_title.split("|");
        var container = title[0];
        var image = title[1];
        var src = "images/" + container + "/" + image;
        $("." + container).removeClass("active");
        $("#img_" + container).attr("src", src);
        $(this).addClass("active");
    });
});
function showPanel(id) {
    $(".mol_panel").css("display", "none");

    $("#" + id).css("display", "flex");
    $("#" + id).css("width", window.innerWidth/2);
    $("#" + id).css("flex-direction", "column");
    $("#" + id).css("height", "200px");
    $(".content").css("width",window.innerWidth/2);



    if (id == "panel_bottomcut") {
        var eleid = $(".Fit.active").attr("id");
        if (eleid == "fit_BodyFitModern") {
            $(".bc_bodyfit").show();
            $(".bc_normalfit").hide();
            $(".bc_slimfit").hide();
            $(".bc_loosefit").hide();
        }
        if (eleid == "fit_NormalFitModern") {
            $(".bc_bodyfit").hide();
            $(".bc_normalfit").show();
            $(".bc_slimfit").hide();
            $(".bc_loosefit").hide();
        }
        if (eleid == "fit_SlimFitModern") {
            $(".bc_bodyfit").hide();
            $(".bc_normalfit").hide();
            $(".bc_slimfit").show();
            $(".bc_loosefit").hide();
        }
        if (eleid == "fit_LooseFitModern") {
            $(".bc_bodyfit").hide();
            $(".bc_normalfit").hide();
            $(".bc_slimfit").hide();
            $(".bc_loosefit").show();
        }
    }
    else if (id == "panel_back") {
        var eleid = $(".Fit.active").attr("id");
        if (eleid == "fit_BodyFitModern") {
            $(".bd_bodyfit").show();
            $(".bd_slimfit").hide();
            $(".bd_regularfit").hide();
            $(".bd_loosefit").hide();
        }
        if (eleid == "fit_NormalFitModern") {
            $(".bd_bodyfit").hide();
            $(".bd_slimfit").hide();
            $(".bd_regularfit").show();
            $(".bd_loosefit").hide();
        }
        if (eleid == "fit_SlimFitModern") {
            $(".bd_bodyfit").hide();
            $(".bd_slimfit").show();
            $(".bd_regularfit").hide();
            $(".bd_loosefit").hide();
        }
        if (eleid == "fit_LooseFitModern") {
            $(".bd_bodyfit").hide();
            $(".bd_slimfit").hide();
            $(".bd_regularfit").hide();
            $(".bd_loosefit").show();
        }
    }
    else if (id == "panel_placket") {
        var eleid = $(".Fit.active").attr("id");
        if (eleid == "fit_BodyFitModern") {
            $(".tux_body").show();
            $(".tux_slim").hide();
            $(".tux_regular").hide();
            $(".tux_loose").hide();
        }
        if (eleid == "fit_NormalFitModern") {
            $(".tux_body").hide();
            $(".tux_slim").hide();
            $(".tux_regular").show();
            $(".tux_loose").hide();
        }
        if (eleid == "fit_SlimFitModern") {
            $(".tux_body").hide();
            $(".tux_slim").show();
            $(".tux_regular").hide();
            $(".tux_loose").hide();
        }
        if (eleid == "fit_LooseFitModern") {
            $(".tux_body").hide();
            $(".tux_slim").hide();
            $(".tux_regular").hide();
            $(".tux_loose").show();
        }
    }
    else if (id == "panel_pocket_style") {
        var eleid = $(".Pocket.active").attr("id");
        if (eleid == "pocket_Lt-Straight_Poc") {
            $(".left_style").show();
            $(".both_style").hide();
        }
        if (eleid == "pocket_Straight_Poc") {
            $(".left_style").hide();
            $(".both_style").show();
        }
    }
    else if (id == "panel_flap") {
        var eleid = $(".Pocket.active").attr("id");
        if (eleid == "pocket_Lt-Straight_Poc") {
            $(".left_flap").show();
            $(".both_flap").hide();
        }
        if (eleid == "pocket_Straight_Poc") {
            $(".left_flap").hide();
            $(".both_flap").show();
        }
    }
}

function closePanel() {
    $(".mol_panel").css("display", "none");
    defaultCam();
}
function hideDiv() {
    document.getElementById("pocket_style-icon-div").style.pointerEvents = 'none';
    document.getElementById("pocket_style-icon-div").style.color = "blue";
    document.getElementById("pocket_flap-icon-div").style.pointerEvents = 'none';
    document.getElementById("pocket_flap-icon-div").style.color = "blue";
    document.getElementById("style").style.pointerEvents = 'none';
    document.getElementById("flap").style.pointerEvents = 'none';
    document.getElementById("style").style.color = "blue";
    document.getElementById("flap").style.color = "blue";
    document.getElementById("filte").style.display = 'none';
    document.getElementById("filte1").style.display = 'none';
    document.getElementById("contrast_fabric-icon-div").style.pointerEvents = 'none';
    document.getElementById("contrast_fabric-icon-div").style.color = "blue";
    document.getElementById("constra").style.pointerEvents = 'none';
    document.getElementById("constra").style.color = "blue";
}

$(document).ready(function () {
    $("#contrast_check").click(function () {
        document.getElementById("contrast_fabric-icon-div").style.pointerEvents = 'auto';
        document.getElementById("contrast_fabric-icon-div").style.color = "black";
        document.getElementById("constra").style.pointerEvents = 'auto';
        document.getElementById("constra").style.color = "black";
    });
});
$(document).ready(function () {
    $("#dilip").click(function () {
        var checked = $(this).data('checked');
        $('.chkaction').prop('checked', false);
        document.getElementById("contrast_fabric-icon-div").style.pointerEvents = 'none';
        document.getElementById("contrast_fabric-icon-div").style.color = "blue";
        document.getElementById("constra").style.pointerEvents = 'none';
        document.getElementById("constra").style.color = "blue";
    });
});
$(document).ready(function () {
    $("#pocket_no").click(function () {
        document.getElementById("pocket_style-icon-div").style.pointerEvents = 'none';
        document.getElementById("pocket_flap-icon-div").style.pointerEvents = 'none';
        document.getElementById("pocket_style-icon-div").style.color = "blue";
        document.getElementById("pocket_flap-icon-div").style.color = "blue";
        document.getElementById("style").style.pointerEvents = 'none';
        document.getElementById("flap").style.pointerEvents = 'none';
        document.getElementById("style").style.color = "blue";
        document.getElementById("flap").style.color = "blue";
    });
    // $("#enable1").click(function()
    // {
    // document.getElementById("embroidery-hide").style.pointerEvents = 'auto';
    // });

    $("#sleeve_long").click(function () {
        $("#cuff-icon-div").show();
    });
    $("#sleeve_Rollup").click(function () {
        $("#cuff-icon-div").hide();
    });
    $("#sleeve_Short").click(function () {
        $("#cuff-icon-div").hide();
    });
});

$(document).ready(function () {
    $("#pocket_Lt-Straight_Poc").click(function () {
        document.getElementById("pocket_style-icon-div").style.pointerEvents = 'auto';
        document.getElementById("pocket_flap-icon-div").style.pointerEvents = 'auto';
        document.getElementById("pocket_style-icon-div").style.color = "black";
        document.getElementById("pocket_flap-icon-div").style.color = "black";
        document.getElementById("style").style.pointerEvents = 'auto';
        document.getElementById("flap").style.pointerEvents = 'auto';
        document.getElementById("style").style.color = "black";
        document.getElementById("flap").style.color = "black";
    });
});

$(document).ready(function () {
    $("#pocket_Straight_Poc").click(function () {
        document.getElementById("pocket_style-icon-div").style.pointerEvents = 'auto';
        document.getElementById("pocket_flap-icon-div").style.pointerEvents = 'auto';
        document.getElementById("pocket_style-icon-div").style.color = "black";
        document.getElementById("pocket_flap-icon-div").style.color = "black";
        document.getElementById("style").style.pointerEvents = 'auto';
        document.getElementById("flap").style.pointerEvents = 'auto';
        document.getElementById("style").style.color = "black";
        document.getElementById("flap").style.color = "black";
    });
});

$(document).ready(function () {
    $(":checkbox").on("change", function () {
        var that = this;
        $(this).parent().css("background-color", function () {
            return that.checked ? "" : "";
        });
    });
});
$(document).ready(function () {

    $(".checkboxes img").click(function () {
        $(this).prevUntil("input").prop("checked", true);
        $(this).prev().show();
        //console.log($(this).closest('input').id);
    }); //document-ready end

}); //document-ready end
//toggle visibility
var divs = ["panel_fit", "panel_fabric", "panel_sleeve",
    "panel_collar", "panel_cuff", "panel_placket",
    "panel_bottomcut", "panel_back", "panel_pocket_placement",
    "panel_pocket_style", "panel_flap", "panel_elbow", "panel_button",
    "panel_thread", "panel_contrast", "panel_contrast_fabric", "panel_embroidery"];
var visibleDivId = null;
function toggleVisibility(divId) {
    if (visibleDivId === divId) {
        visibleDivId = null;
    } else {
        visibleDivId = divId;
    }
    hideNonVisibleDivs();
}
function hideNonVisibleDivs() {
    var i, divId, div;
    for (i = 0; i < divs.length; i++) {
        divId = divs[i];
        div = document.getElementById(divId);
        if (visibleDivId === divId) {
            div.style.display = "block";

        } else {
            div.style.display = "none";
        }
    }
}
//Font change style script for roman
function Avantgarde() {
    document.getElementById("font_change").style.fontFamily = "Avantgarde, Serif";
}
function CourierNew() {
    document.getElementById("font_change").style.fontFamily = "Courier New, monospace";
}
function Georgia() {
    document.getElementById("font_change").style.fontFamily = "Georgia, serif";
}
function AppleChancery() {
    document.getElementById("font_change").style.fontFamily = "Apple Chancery, cursive";
}
function BrushScript() {
    document.getElementById("font_change").style.fontFamily = "Brush Script Std, Brush Script MT, cursive";
}
function fantasy() {
    document.getElementById("font_change").style.fontFamily = "fantasy";
}
// change font color
function makewhite() {
    document.getElementById('font_change').style.color = "white";
};
function makeSilverGray() {
    document.getElementById('font_change').style.color = "#C0C0C0";
};
function makeSteelGray() {
    document.getElementById('font_change').style.color = "#A9A9A9";
};
function makeMediumBrown() {
    document.getElementById('font_change').style.color = "#6E6A6A";
};
function makeCoffee() {
    document.getElementById('font_change').style.color = "#574444";
};
function makeYellow() {
    document.getElementById('font_change').style.color = "yellow";
};
function makeBlack() {
    document.getElementById('font_change').style.color = "#000000";
};
function makeBrightyellow() {
    document.getElementById('font_change').style.color = "#FFFFDF";
};
function makeOrange() {
    document.getElementById('font_change').style.color = "orange";
};
function makePink() {
    document.getElementById('font_change').style.color = "pink";
};
function makeHotPink() {
    document.getElementById('font_change').style.color = "#B75A95";
};
function makeCherryRed() {
    document.getElementById('font_change').style.color = "#AB1313";
};
function makeDeepRed() {
    document.getElementById('font_change').style.color = "#8B1212";
};
function makeMauve() {
    document.getElementById('font_change').style.color = "#624949";
};
function makeDeepPurple() {
    document.getElementById('font_change').style.color = "#4C3954";
};
function makeBordeauxRed() {
    document.getElementById('font_change').style.color = "#330019";
};
function makePurple() {
    document.getElementById('font_change').style.color = "purple";
};
function makeDistinctPurple() {
    document.getElementById('font_change').style.color = "#330066";
};
function makeLavender() {
    document.getElementById('font_change').style.color = "#E5CCFF";
};
function makeBrightGreen() {
    document.getElementById('font_change').style.color = "#99E28E";
};
function makeLime() {
    document.getElementById('font_change').style.color = "#59A64E";
};
function makeRacingGreen() {
    document.getElementById('font_change').style.color = "#055A1C";
};
function makeOliveGreen() {
    document.getElementById('font_change').style.color = "#31523A";
};
function makeDeepTurqouise() {
    document.getElementById('font_change').style.color = "#539DA8";
};
function makeBabyBlue() {
    document.getElementById('font_change').style.color = "#B0C4DE";
};
function makeSkyBlue() {
    document.getElementById('font_change').style.color = "#87C2E5";
};
function makeAzureBlue() {
    document.getElementById('font_change').style.color = "#29739E";
};
function makeOceanBlue() {
    document.getElementById('font_change').style.color = "#0E4A6C";
};
