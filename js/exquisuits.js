    function heightp() {
        var param = $("#heightparam").val();
        if(param == "Feets") {
            $("#dynaheight").html("<div class='row'><div class='col-md-12' style='margin-left:14px;padding-left:0'>" +
                "<input id='height0' class='form-control' type='text' placeholder='Feets' value='' style='float:left;" +
                "width:65px' /><input type='text' id='height1' class='form-control' style='border-left:1px solid #fff;" +
                "float:left;width:78px' placeholder='Inches' value='' /></div></div>");
        }
        else
        {
            $("#dynaheight").html("<input type='text' name='height' id='height' class='form-control' placeholder='Enter Height'>");
        }
    }
    function forgetpasspop(){
        $(".modal-title").html("<h4 style='color:red'>Enter E-Mail Address</h4>");
        $(".modal-body").css("height","115px")
        $(".modal-body").html("<p id='error' style='color:red;text-align: center'></p><div class='col-md-8'>" +
        "<div class='form-group'><label>Enter Email</label><input type='text' class='form-control' id='fp_email' " +
        "value='' placeholder='Enter Registered E-Mail Address'/></div></div>");
        $(".modal-footer").html("<input type='button' value='Submit' onclick='forgetpass()' " +
        " class='btn btn-danger pull-right' />")
        $("#modal").modal("show");
    }
    function forgetpass()
    {
        var email = $("#fp_email").val();
        if(email != "")
        {
            if(validateEmail(email)) {
                var url = "../admin/api/registerUser.php";
                $.post(url,{"type":"forgotPassword","email":email},function (data) {
                    var Status = data.Status;
                    if (Status == "Success") {
                        $(".modal-title").html("<h4 style='color:green'>Forgot Password</h4>");
                        $(".modal-body").html("<label>Reset Password Link has been sent to your Registered E-Mail Address</label>");
                        setTimeout(function () {
                            $("#modal").modal("hide");
                        }, 1000);
                    }
                    else if (Status == "Failure") {
                        $("#error").html("Entered E-Mail is not a Registered E-Mail Address");
                        return false;
                    } else {
                        $("#error").html("Server Error !!! Please Try After Some Time");
                        return false;
                    }
                }).fail(function () {
                    $("#error").html("Server Error !!! Please Try After Some Time");
                    return false;
                });
            }else{
                $("#error").html("Please Enter Valid E-Mail Address");
                return false;
            }
        }
        else{
            $("#error").html("Please Enter Your Registered E-Mail Address");
            return false;
        }
    }
    $(".signup_link").click(function () {

        $(".signin_form").css('opacity', '0');
        $(".signup_form").css('opacity', '100');


        $("#card").flip(true);

        return false;
    });

    
    $(document).ready(function () {
        $('#inputdob').datetimepicker({
            singleDatePicker: true,
            inputdob: moment().subtract(6, 'days')
        });
    });
	
    $("#loginbtn").click(function(){
        $(".backloader").show();
        var username = $("#logemail").val();
        var password = $("#logpassword").val();
        if(username == "" || password == ""){
             $(".backloader").hide();
            $(".error").html("All Feilds are Required Filled");
            return false;

        }

        if(validateEmail(username)){
            var url = "admin/api/registerUser.php";
            $.post(url, {"type": "Login","email":username,"password":password}, function (data) {
                var status = data.Status;
                if (status == "Success") {
                    $(".backloader").hide();
                    window.location="customize.php";
                }
                else {
                    $(".backloader").hide();
                    $(".error").html(data.Message);
                }
            }).fail(function(){
                $(".backloader").hide();
                $(".error").html("Server Error !!! Please Try After Some Time....");
            });
        }
        else{
            $(".backloader").hide();
            $(".error").html("Invalid E-Mail Address Entered");
            return false;
        }
    });

    function register(){
        $(".backloader").show();
        var fname = $("#fname").val();
        var lname = $("#lname").val();
        var countryId = $("#countryId").val();
        var stateId = $("#stateId").val();
        var cityId = $("#cityId").val();
        var dob = $("#inputdob").val();
        var heightparam = $("#heightparam").val();
        var stylist = $("#stylist").val();
        var gender = $("#gender").val();
        var language = $("#language").val();
        var weightparam = $("#weightparam").val();
        var email = $("#email").val();
        var password = $("#password").val();


        if(fname == "" || lname == "" || countryId == "" || stateId == "" || cityId == "" || dob == "" ||
        heightparam == "" || email == "" || password == "" || gender == "" || dob == "" ||  weightparam == "" || weight == ""){
             $(".backloader").hide();
            $(".error").html("All Feilds Should Required Filled");
            return false;
        }
        if(!(validateEmail(email))){
             $(".backloader").hide();
            $(".error").html("Invalid Email Address Used");
            return false;
        }
        //default height in inches and weight in pounds
        ////calculations for height
        var height;
        var weight;
        if(heightparam == "Feets"){
             $(".backloader").hide();
            if($("#height0").val() == "" || $("#height1").val() == "") {
                $(".error").html("Height Should not be Blank");
                return false;
            }
            height = parseInt($("#height0").val() * 12) + parseInt($("#height1").val());
        }
        else if(heightparam == "Centimeters"){
            if(!($("#height").val() == "")){
                height = parseInt($("#height").val()*0.393701);
            }
            else{
                 $(".backloader").hide();
                $(".error").html("Height Should not be Blank");
                return false;
            }
        }
        else{
            if($("#height").val() == "") {
                 $(".backloader").hide();
                $(".error").html("Height Should not be Blank");
                return false;
            }
            height = $("#height").val();
        }
        ////calculations for weight
        if(weightparam == "Kilograms"){
            weight = Math.round($("#weight").val()*2.20462);
        }
        else if(weightparam == "Stones"){
            weight = Math.round($("#weight").val()*14);
        }
        else{
            weight = Math.round($("#weight").val());
        }
        var data = new FormData();
        if($("#file0").val() != ""){
            var user_profile = $("#file0").val();
            var file_ext = user_profile.split(".");
            file_ext = file_ext[file_ext.length - 1];
            if (file_ext == "jpeg" || file_ext == "png" || file_ext == "jpg" || file_ext == "gif") {
                var _file = document.getElementById('file0');
                data.append('user_profile', _file.files[0]);
            }
            else 
            {
                 $(".backloader").hide();
                $(".error").html("File Should Be JPG,PNG,JPEG,GIF Formats Only");
                return false;
            }
        }
        data.append("type", "Register");
        data.append("fname",fname);
        data.append("lname",lname);
        data.append("countryId",countryId);
        data.append("stateId",stateId);
        data.append("cityId",cityId);
        data.append("dob",dob);
        data.append("height",height);
        data.append("stylist",stylist);
        data.append("gender",gender);
        data.append("language",language);
        data.append("weight",weight);
        data.append("email",email);
        data.append("password",password);

        $(".loadimage").show();
        var request = new XMLHttpRequest();
        request.onreadystatechange = function(){
            if(request.readyState == 4) {
                var response = $.parseJSON(request.response);
                var status = response.Status;
                if (status == "Success") {
                    $(".backloader").hide();
                    location.reload(true);
                }
                else {
                    $(".backloader").hide();
                    $(".error").html(response.Message);
                    $(".loadimage").hide();
                }
            }
        };
        request.open('POST', 'admin/api/registerUser.php');
        request.send(data);
    }

    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
