

var sctm =["logo.ctm",


"collar_ModernClub.ctm",
"collar_ModernClub_Btn.ctm",
"collar_ModernClub_Hole.ctm",
"collar_ModernClub_Thread.ctm",




"pocket_Straight.ctm",
"pocket_Straight_Thread.ctm",



"cuff_SingleButtonRounded.ctm",
"cuff_SingleButtonRounded_Btn.ctm",
"cuff_SingleButtonRounded_Hole.ctm",
"cuff_SingleButtonRounded_Thread.ctm",


"shoulder_back.ctm",
"shoulder_back_Thread.ctm",


"placket_WithPlacket.ctm",
"placket_WithPlacket_Btn.ctm",
"placket_WithPlacket_Hole.ctm",
"placket_WithPlacket_Thread.ctm",


			
			
			"fit_SlimFitModern.ctm",
			"fit_SlimFitModern_Thread.ctm",
			"fit_SlimFitClassic.ctm",			
			"fit_SlimFitClassic_Thread.ctm",			
			"fit_SlimFitStraight.ctm",
			"fit_SlimFitStraight_Thread.ctm",
			
			
			"sleeve_long.ctm",
			"sleeve_long_Btn.ctm",
			"sleeve_long_Hole.ctm",
			"sleeve_long_Thread.ctm"
			
			];
			
			
var texture =["Beige Fox Print.jpg",
			  "Anchor Print.jpg",
			  "Beige Purple Dobby.jpg",
			  "Black Fill a Fill.jpg",
			  "Black Matt.jpg",
			  "Black White Stripe.jpg",
			  "Blue Check.jpg",
			  "Blue Dobby.jpg",
			  "Blue Dots Print.jpg",
			  "Blue HB Twill.jpg",
			  "Blue Oxford.jpg",
			  "Blue Purple Dobby.jpg",
			  "Blue Satin.jpg",
			  "Blue Twill.jpg",
			  "Blue White Check.jpg",
			  "Blue White Stripe.jpg",
			  "Blue Yellow Check.jpg",
			  "Bow Print.jpg",
			  "Brown Check.jpg",
			  "Brown Fill a Fill.jpg",
			  "Brown Purple Check.jpg",
			  "Brown White Check.jpg",
			  "Brown Yellow Check.jpg",
			  "Dotted Dobby.jpg",
			  "Fil-A-Fil Blue.jpg",
			  "Fil-A-Fil_Green.jpg",
			  "Fil-A-Fil_Light Blue.jpg",
			  "Fil-A-Fil_Purple.jpg",
			  "Fil-A-Fil_Red.jpg",
			  "Floral Burnout.jpg",
			  "Floral Star Burnout.jpg",
			  "Gray Check.jpg",
			  "Gray Fill a Fill.jpg",
			  "Gray Fox Print.jpg",
			  "Gray Horse Print.jpg",
			  "Gray Purple Check.jpg",
			  "Gray Purple Dobby.jpg",
			  "Gray Stripe.jpg",
			  "Gray White Dobby.jpg",
			  "Green Blue Stripe.jpg",
			  "Green HB Twill.jpg",
			  "Grey White Stripe.jpg",
			  "Lotus Print.jpg",
			  "Marron Horse Print.jpg",
			  "Multi Check.jpg",
			  "Multi Red Check.jpg",
			  "Multi Yellow Check.jpg",
			  "Mustard Blue Check.jpg",
			  "Mustard Check.jpg",
			  "Mustard Dobby.jpg",
			  "Mustard Navy Check.jpg",
			  "Navy Blue Check.jpg",
			  "Navy Fill a Fill.jpg",
			  "Navy Green Dobby.jpg",
			  "Navy Green HB Twill.jpg",
			  "Navy Yellow Check.jpg",
			  "Palmtree Print.jpg",
			  "Pink Oxford.jpg",
			  "Pink Twill.jpg",
			  "Purple Navy Check.jpg",
			  "Purple Oxford.jpg",
			  "Purple White Dobby.jpg",
			  "Red Blue Check.jpg",
			  "Red Check.jpg",
			  "Red HB Twill.jpg",
			  "Red Navy Check.jpg",
			  "Red Reverse Fill a fill.jpg",
			  "Red White Stripe.jpg",
			  "Royal Blue Dobby.jpg",
			  "School Print.jpg",
			  "Stag Print.jpg",
			  "Twill-Chamrey.jpg",
			  "White Check.jpg",
			  "White Dots Print.jpg",
			  "White Pink Check.jpg",
			  "White Red Dobby.jpg",
			  "White Structured.jpg",
			  "Yellow HB Twill.jpg",
			  "Yellow navy Check.jpg",
			  "Yellow Oxford.jpg",
			  "Yellow Twill.jpg",
			  "Brown Floral Dobby.jpg",			  
			  "Floral Burnout Car.jpg",			  
			  "Floral Burnout Cycle.jpg",			  
			  "Floral Burnout Shoe.jpg",		  
			  "Floral Burnout Star.jpg",		  
			  "Navy Floral Dobby.jpg",		  
			  "Twill-Solid_CoconutTree Print.jpg",			  			  
			  "Twill-Solid_Planes Print.jpg"			  
			  ];
var xCod =[".020",".1",".006",".05",".1",".09",".02",  ".02",".02",".05",".05",".006",".05",".05",".03",".01",".02",".0085",".018",".03",
		   ".008",".05",".02", ".02",".05",".05",".05",".05",".05",   ".05",".02",  ".06",".05",".015",".015",".007",".006",".13",  ".0047",".059",
		   ".05",   ".02",  ".007",  ".01",".008",".023",".075",".05",".02",".02",".03",".025",".05",".025",".01",".02",".005",".05",".05",".046",
		   ".05",".0047",".035",".02",".05",".04",".05",".12",".02",".01",".01",".05",".02" ,".02",".05",".018",".01",".05",  ".07",".05",".05",
		   ".04",".02",".02",".02",".02",".04",".02",".02"];
		   
var yCod =[".020",".1",".003",".05",".1",".09",".02",  ".006",".02",".05",".05",".003",".05",".05",".03",".05",".02",".0058",".015",".01",
			".004",".05",".02", ".02",".05",".05",".05",".05",".05",  ".05",".02",  ".09",".05", ".015",".015",".006",".003",".05",  ".003",".05",
			".05",  ".05"  ,".007"  ,".01",".01",".02",".075",".05",".028",".019",".03",".03",".05",".03",".01",".024",".005",".05",".05",".046",
			".05",".002",".03",".02",".05",".04",".05",".05",".02",".01",".01",".05",".016" ,".02",".05",".017",".01",".05",  ".06",".05",".05",
			".03",".02",".02",".02",".02",".03",".02",".02"];