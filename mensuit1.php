<?php
include("header3.php");

if(!isset($_SESSION['benj_user_id'])){
    echo "<input type='hidden' value='' id='user_id' />";
}
else{
    echo "<input type='hidden' value='".$_SESSION['benj_user_id']."' id='user_id' />";

}
//if($user_id == "")
?>
    <head>
        <link rel="stylesheet" href="css/shirtstyle1.css">
        <link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script src="js/libs/jquery-1.11.3.js"></script>
        <script src="js/libs/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/ms-Suit/Men_suit-script1.js"></script>
        <script src="js/libs/jquery-ui.min.js"></script>
        <style>
            .progdiv {
                left: 40%;
                position: absolute;
                top: 50%;
                width: 30%;
                display:none;
            }
            .pload{
                left: 40%;
                position: absolute;
                top: 46%;
                width: 30%;
                display:none;
                text-align: center;
            }
            .cartbtn {
                position: fixed;
                right: 10px;
                top: 300px;
                width: 200px;
                z-index: 999999;
                display: none;
            }
        </style>
    </head>
    <body onload="hideDiv();">
    <div id="main-container" class="Main_cont">
        <!--Start Of Left Container-->
        <div id="left_container" class="left-container">
            <div id="fabric-icon-div" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_fabric');toggleVisibility('panel_fabric');">
                <img id="img_fabric"  src="images/fabric/AR 2231 01 600 all wool 260G.jpg"/>
                <div class="left_font">Fabric</div>
            </div>
            <div id="fit-icon-div" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_fit');toggleVisibility('panel_fit');">
                <img id="img_fit" style="border-radius:24px;" src="images/fit/Regular Fit.svg"/>
                <div class="left_font">Fit</div>
            </div>
            <div id="button1-icon-div" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_regular_button');toggleVisibility('panel_regular_button');">
                <img id="img_Breasted1" style="border-radius:24px;" alt="Breasted1" src="images/Breasted1/One Button.svg" width="60" height="60"/>
                <div class="left_font">Button</div>
            </div>
            <div id="lapel-icon-div" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_lapel_1');toggleVisibility('panel_lapel_1');">
                <img id="img_Lapel1" style="border-radius:24px;" alt="Lapel1" src="images/Lapel1/Standard Lapel.svg"/>
                <div class="left_font">Lapel</div>
            </div>
            <div id="lapel1-icon-div" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_sleeve');toggleVisibility('panel_sleeve');">
                <img id="img_sleeve" style="border-radius:24px;" alt="sleeve" src="images/sleeve/Sleeve 4 Button.svg"/>
                <div class="left_font">Sleeve</div>
            </div>
            <div id="stitch-icon-div" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_stitch');toggleVisibility('panel_stitch');">
                <img id="img_Stich" style="border-radius:24px;" alt="Stich" src="images/Stich/Lapel Stitch.svg"/>
                <div class="left_font">Lapel Stitch</div>
            </div>
            <div id="pocket-icon-div" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_pocket');toggleVisibility('panel_pocket');">
                <img id="img_pocket" style="border-radius:24px;" alt="pocket" src="images/pocket/Welt Pocket.svg"/>
                <div class="left_font">Pocket</div>
            </div>
            <div id="ticket-icon-div" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_ticket');toggleVisibility('panel_ticket');">
                <img id="img_ticket" style="border-radius:24px;" alt="ticket" src="images/ticket/Ticket Pocket.svg"/>
                <div class="left_font">Ticket Pocket</div>
            </div>
            <div id="vent-icon-div" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_regular_vent');toggleVisibility('panel_regular_vent');">
                <img id="img_vent" style="border-radius:24px;" alt="vent" src="images/vent/Single vent.svg"/>
                <div class="left_font">Vent</div>
            </div>
            <div id="elbow-icon-div" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_elbow');toggleVisibility('panel_elbow');">
                <img id="img_elbow" style="border-radius:24px;" alt="elbow" src="images/elbow/Elbow Patch.svg"/>
                <div class="left_font">Patch</div>
            </div>
            <div id="elbow-fabric-icon-div" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_elbow_fabric');toggleVisibility('panel_elbow_fabric');">
                <img id="img_elbowfabric" style="width:60;height:60;border-radius:26px;" src="images/elbowfabric/AR 2231 01 600 all wool 260G.jpg"/>
                <div class="left_font">Elbow Fabric</div>
            </div>
            <div id="Suit-lining-icon-div" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_lining');toggleVisibility('panel_lining');">
                <img id="img_Lining" style="border-radius:24px;" src="images/Lining/Black.png"/>
                <div class="left_font">Suit Lining</div>
            </div>
            <div id="button-icon-div" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_button_color');toggleVisibility('panel_button_color');">
                <img id="img_button" style="border-radius:24px;" src="images/button/Sorrento-white.png"/>
                <div class="left_font">Button Color</div>
            </div>
            <div id="thread-icon-div" class="left_container_img " onclick="camerAnim(this.id);showPanel('panel_thread');toggleVisibility('panel_thread');">
                <img id="img_thread" style="border-radius:24px;width:50px; height:70px;" src="images/thread/White.png"/>
                <div class="left_font">Thread</div>
            </div>
        </div>
        <p class="pload">Please Wait ... Object Is Loading</p>
        <div class="progress progdiv">
            <div id="prog" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40"
                 aria-valuemin="0" aria-valuemax="100" style="width:0%">0%</div>
        </div>
    </div>
    <!--Start Of Fabric Fabric -->
    <div id="panel_fabric" class="panel mol_panel" style="background-color:transparent;" >
        <div class="heading" style="background-color:transparent;">
            <div class="title">Fabric</div>
            <img id="close" class="close" src="images/close.png" onclick="camerAnim(this.id);closePanel();closeNav();"/>
        </div>
        <div class="content" style="overflow-y:scroll;">
            <div id="fabric_0" onclick="textureChange(this.id);" class="content_inner Navy fabric stripe active" title="fabric|AR 2231 01 600 all wool 260G.jpg">
                <img src="images/fabric/AR 2231 01 600 all wool 260G.jpg"  />
                <div>Navy Stripe</div>
            </div>
            <div id="fabric_1" onclick="textureChange(this.id);" class="content_inner Black stripe fabric " title="fabric|AR 2235 05 580 73 wool 27 mohair 290 gr.jpg">
                <img src="images/fabric/AR 2235 05 580 73 wool 27 mohair 290 gr.jpg"  />
                <div>Black Stripe</div>
            </div>
            <div id="fabric_2" onclick="textureChange(this.id);" class="content_inner Blue stripe fabric " title="fabric|AR 2251 01 600 all wool 260G.jpg">
                <img src="images/fabric/AR 2251 01 600 all wool 260G.jpg"  />
                <div>Blue Stripe</div>
            </div>
            <div id="fabric_3" onclick="textureChange(this.id);" class="content_inner Grey stripe fabric " title="fabric|AR 2251 02 600 all wool 260g.jpg">
                <img src="images/fabric/AR 2251 02 600 all wool 260g.jpg"  />
                <div>Dark Grey Stripe</div>
            </div>
            <div id="fabric_4" onclick="textureChange(this.id);" class="content_inner stripe Grey fabric " title="fabric|AR 2251 16 600 all wool 260gr.jpg">
                <img src="images/fabric/AR 2251 16 600 all wool 260gr.jpg"  />
                <div>Light Grey Stripe</div>
            </div>
            <div id="fabric_5" onclick="textureChange(this.id);" class="content_inner Grey stripe fabric " title="fabric|AR 2251 17 600 all wool 260gr.jpg">
                <img src="images/fabric/AR 2251 17 600 all wool 260gr.jpg"  />
                <div>Steel Grey Stripe</div>
            </div>
            <div id="fabric_6" onclick="textureChange(this.id);" class="content_inner Blue check fabric " title="fabric|AR 2251 49 600 all wool 260gr.jpg">
                <img src="images/fabric/AR 2251 49 600 all wool 260gr.jpg"  />
                <div>Blue Check</div>
            </div>
            <div id="fabric_7" onclick="textureChange(this.id);" class="content_inner Grey check fabric " title="fabric|AR 2251 50 600 all wool 260g.jpg">
                <img src="images/fabric/AR 2251 50 600 all wool 260g.jpg"  />
                <div>Grey Check</div>
            </div>
            <div id="fabric_8" onclick="textureChange(this.id);" class="content_inner Navy check fabric " title="fabric|AR 2251 55 600 all wool 260gr.jpg">
                <img src="images/fabric/AR 2251 55 600 all wool 260gr.jpg"  />
                <div>Navy Check</div>
            </div>
            <div id="fabric_9" onclick="textureChange(this.id);" class="content_inner Navy check fabric" title="fabric|AR 2251 56 600 all wool 260gr.jpg">
                <img src="images/fabric/AR 2251 56 600 all wool 260gr.jpg"  />
                <div>Navy Check</div>
            </div>
            <div id="fabric_10" onclick="textureChange(this.id);" class="content_inner Brown check fabric" title="fabric|AR 2251 56 600 all wool 260gr_1.jpg">
                <img src="images/fabric/AR 2251 56 600 all wool 260gr_1.jpg"  />
                <div>Brown Check</div>
            </div>
            <div id="fabric_11" onclick="textureChange(this.id);" class="content_inner Gray check fabric" title="fabric|AR 2251 58 600 all wool 260gr_1.jpg">
                <img src="images/fabric/AR 2251 58 600 all wool 260gr_1.jpg"  />
                <div>Gray Check</div>
            </div>
            <div id="fabric_12" onclick="textureChange(this.id);" class="content_inner Brown stripe fabric" title="fabric|AR 2258 74 560 sunny season.jpg">
                <img src="images/fabric/AR 2258 74 560 sunny season.jpg"  />
                <div>Brown HB</div>
            </div>
            <div id="fabric_13" onclick="textureChange(this.id);" class="content_inner Gray stripe fabric" title="fabric|AR 2258 75 560 sunny season.jpg">
                <img src="images/fabric/AR 2258 75 560 sunny season.jpg"  />
                <div>Gray HB</div>
            </div>
        </div>
    </div>
    <!--End Of fabric-->
    <!--Start of suit Fit -->
    <div id="panel_fit" class="panel mol_panel" style="background-color:transparent;">
        <div class="heading" style="background-color:transparent;">
            <div  class="title">Suit Fit</div>
            <img id="close1" class="close" src="images/close.png" onclick="camerAnim(this.id);closePanel()"/>
        </div>
        <div class="content">
            <div id="MS-B-Panel_R-fit"  onclick="fitMeshChange(this.id);" class="content_inner fit active" title="fit|Regular Fit.svg">
                <img  src="images/fit/Regular Fit.svg" />
                <div style="font-family: 'Times New Roman', Times, serif;">Regular Fit</div>
            </div>
            <div  id="MS-B-Panel_S-fit"  onclick="fitMeshChange(this.id);" class="content_inner fit" title="fit|Slim Fit1.svg">
                <img  src="images/fit/Slim Fit1.svg" />
                <div style="font-family: 'Times New Roman', Times, serif;">Slim  Fit</div>
            </div>
        </div>
    </div>
    <!--End Of suit Fit-->
    <!--Start Of Regular Vent -->
    <div id="panel_regular_vent" class="panel mol_panel" style="background-color:transparent;">
        <div class="heading reg_vent" style="background-color:transparent;">
            <div  class="title">Regular Fit Vent</div>
            <img id="close2" class="close" src="images/close.png" onclick="camerAnim(this.id);closePanel()"/>
        </div>
        <div class="content reg_vent">
            <div id="MS-B-Panel_R-fit_WithSingleVent" onclick="ventChange(this.id);" class="content_inner vent active" title="vent|Single vent.svg">
                <img  src="images/vent/Single vent.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Single vent</div>
            </div>
            <div id="MS-B-Panel_R-fit_WithOutVent" onclick="ventChange(this.id);" class="content_inner vent" title="vent|No Vent.svg">
                <img  src="images/vent/No Vent.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">No vent</div>
            </div>
            <div id="MS-B-Panel_R-fit_WithDoubleVent" onclick="ventChange(this.id);" class="content_inner vent" title="vent|Double vent.svg">
                <img  src="images/vent/Double vent.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Double vent</div>
            </div>
        </div>
        <div class="heading slim_vent" style="background-color:transparent;">
            <div class="title"> Slim Fit vent</div>
            <img id="close9" class="close" src="images/close.png" onclick="closePanel()"/>
        </div>
        <div class="content slim_vent">
            <div id="MS-B-Panel_S-fit_WithSingleVent" onclick="ventChange(this.id);" class="content_inner vent active" title="vent|Single vent.svg">
                <img  src="images/vent/Single vent.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Single vent</div>
            </div>
            <div id="MS-B-Panel_S-fit_WithOutVent" onclick="ventChange(this.id);" class="content_inner vent" title="vent|No Vent.svg">
                <img  src="images/vent/No Vent.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">No vent</div>
            </div>
            <div id="MS-B-Panel_S-fit_WithDoubleVent" onclick="ventChange(this.id);" class="content_inner vent" title="vent|Double vent.svg">
                <img  src="images/vent/Double vent.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Double vent</div>
            </div>
        </div>
    </div>
    <!--End of Regular Vent -->
    <!-- Start of Regular Button -->
    <div id="panel_regular_button" class="panel mol_panel" style="background-color:transparent;">
        <div class="heading reg_button" style="background-color:transparent;">
            <div class="title">Regular Suit Button</div>
            <img id="close3" class="close" src="images/close.png" onclick="camerAnim(this.id);closePanel()"/>
        </div>
        <div class="content reg_button">
            <div id="MS-F-Panel_R-fit_One" onclick="meshChange(this.id);" class="content_inner Breasted1 active" title="Breasted1|One Button.svg">
                <img  src="images/Breasted1/One Button.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">One Button</div>
            </div>
            <div id="MS-F-Panel_R-fit_Two"  onclick="meshChange(this.id);" class="content_inner Breasted1" title="Breasted1|Two Button.svg">
                <img  src="images/Breasted1/Two Button.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Two Button</div>
            </div>
            <div id="MS-F-Panel_R-fit_Three" onclick="meshChange(this.id);" class="content_inner Breasted1" title="Breasted1|Three Button.svg">
                <img  src="images/Breasted1/Three Button.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Three Button</div>
            </div>
            <div id="MS-F-Panel_R-fit_Four" onclick="meshChange(this.id);" class="content_inner Breasted1" title="Breasted1|Double Breasted.svg">
                <img  src="images/Breasted1/Double Breasted.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Double Breasted</div>
            </div>
        </div>
        <div class="heading slim_button" style="background-color:transparent;">
            <div class="title">Slim Suit Button</div>
            <img id="close10" class="close" src="images/close.png" onclick="closePanel()"/>
        </div>
        <div class="content slim_button">
            <div id="MS-F-Panel_S-fit_One" onclick="meshChange(this.id);" class="content_inner Breasted1 active" title="Breasted1|One Button.svg">
                <img  src="images/Breasted1/One Button.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">One Button</div>
            </div>
            <div id="MS-F-Panel_S-fit_Two"  onclick="meshChange(this.id);" class="content_inner Breasted1" title="Breasted1|Two Button.svg">
                <img  src="images/Breasted1/Two Button.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Two Button</div>
            </div>
            <div id="MS-F-Panel_S-fit_Three" onclick="meshChange(this.id);" class="content_inner Breasted1" title="Breasted1|Three Button.svg">
                <img  src="images/Breasted1/Three Button.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Three Button</div>
            </div>
            <div id="MS-F-Panel_S-fit_Four" onclick="meshChange(this.id);" class="content_inner Breasted1" title="Breasted1|Double Breasted.svg">
                <img  src="images/Breasted1/Double Breasted.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Double Breasted</div>
            </div>
        </div>
    </div>
    <!--End of regular Button -->
    <!-- Start Of Buttons Lapel -->
    <div id="panel_lapel_1" class="panel mol_panel" style="background-color:transparent;">
        <div class="heading but_one_lapel" style="background-color:transparent;">
            <div class="title">Lapel</div>
            <img id="close5" class="close" src="images/close.png" onclick="camerAnim(this.id);closePanel()"/>
        </div>
        <div class="content but_one_lapel">
            <div id="MS-Lap_Std_One" onclick="meshChange(this.id);" class="content_inner Lapel1 active" title="Lapel1|Standard Lapel.svg">
                <img  src="images/Lapel1/Standard Lapel.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Standard Lapel</div>
            </div>
            <div id="MS-Lap_Peak_One" onclick="meshChange(this.id);" class="content_inner Lapel1 " title="Lapel1|Peak Lapel.svg">
                <img  src="images/Lapel1/Peak Lapel.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Peak Lapel</div>
            </div>
            <div id="MS-Lap_Slim_One" onclick="meshChange(this.id);" class="content_inner Lapel1" title="Lapel1|Slim Lapel.svg">
                <img  src="images/Lapel1/Slim Lapel.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Slim Lapel</div>
            </div>
            <div id="MS-Lap_Shawl_One" onclick="meshChange(this.id);" class="content_inner Lapel1" title="Lapel1|Shawl Lapel.svg">
                <img  src="images/Lapel1/Shawl Lapel.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Shawl Lapel</div>
            </div>
        </div>
        <div class="heading but_two_lapel" style="background-color:transparent;">
            <div class="title">Lapel 2</div>
            <img id="close6" class="close" src="images/close.png" onclick="closePanel()"/>
        </div>
        <div class="content but_two_lapel">
            <div id="MS-Lap_Std_Two" onclick="meshChange(this.id);" class="content_inner Lapel1  active" title="Lapel1|Standard Lapel.svg">
                <img  src="images/Lapel1/Standard Lapel.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Standard Lapel</div>
            </div>
            <div id="MS-Lap_Peak_Two" onclick="meshChange(this.id);" class="content_inner Lapel1" title="Lapel1|Peak Lapel.svg">
                <img  src="images/Lapel1/Peak Lapel.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Peak Lapel</div>
            </div>
            <div id="MS-Lap_Slim_Two" onclick="meshChange(this.id);" class="content_inner Lapel1" title="Lapel1|Slim Lapel.svg">
                <img  src="images/Lapel1/Slim Lapel.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Slim Lapel</div>
            </div>
            <div id="MS-Lap_Shawl_Two" onclick="meshChange(this.id);" class="content_inner Lapel1" title="Lapel1|Shawl Lapel.svg">
                <img  src="images/Lapel1/Shawl Lapel.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Shawl Lapel</div>
            </div>
        </div>
        <div class="heading but_three_lapel" style="background-color:transparent;">
            <div class="title">Lapel 3</div>
            <img id="close7" class="close" src="images/close.png" onclick="closePanel()"/>
        </div>
        <div class="content but_three_lapel">
            <div id="MS-Lap_Std_Three" onclick="meshChange(this.id);" class="content_inner Lapel1 active" title="Lapel1|Standard Lapel.svg">
                <img  src="images/Lapel1/Standard Lapel.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Standard Lapel</div>
            </div>
            <div id="MS-Lap_Peak_Three" onclick="meshChange(this.id);" class="content_inner Lapel1 " title="Lapel1|Peak Lapel.svg">
                <img  src="images/Lapel1/Peak Lapel.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Peak Lapel</div>
            </div>
            <div id="MS-Lap_Slim_Three" onclick="meshChange(this.id);" class="content_inner Lapel1" title="Lapel1|Slim Lapel.svg">
                <img  src="images/Lapel1/Slim Lapel.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Slim Lapel</div>
            </div>
            <div id="MS-Lap_Shawl_Three" onclick="meshChange(this.id);" class="content_inner Lapel1" title="Lapel1|Shawl Lapel.svg">
                <img  src="images/Lapel1/Shawl Lapel.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Shawl Lapel</div>
            </div>
        </div>
        <div class="heading but_four_lapel" style="background-color:transparent;">
            <div class="title">Lapel 4</div>
            <img id="close8" class="close" src="images/close.png" onclick="closePanel()"/>
        </div>
        <div class="content but_four_lapel">
            <div id="MS-Lap_Std_Four" onclick="meshChange(this.id);" class="content_inner Lapel1 active" title="Lapel1|Standard Lapel.svg">
                <img  src="images/Lapel1/Standard Lapel.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Standard Lapel</div>
            </div>
            <div id="MS-Lap_Peak_Four" onclick="meshChange(this.id);" class="content_inner Lapel1" title="Lapel1|Peak Lapel.svg">
                <img  src="images/Lapel1/Peak Lapel.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Peak Lapel</div>
            </div>
            <div id="MS-Lap_Slim_Four" onclick="meshChange(this.id);" class="content_inner Lapel1" title="Lapel1|Slim Lapel.svg">
                <img  src="images/Lapel1/Slim Lapel.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Slim Lapel</div>
            </div>
            <div id="MS-Lap_Shawl_Four" onclick="meshChange(this.id);" class="content_inner Lapel1" title="Lapel1|Shawl Lapel.svg">
                <img  src="images/Lapel1/Shawl Lapel.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Shawl Lapel</div>
            </div>
        </div>
    </div>
    <!-- End Of Buttons  Lapel -->
    <!-- Start of suting lining -->
    <div id="panel_lining" class="panel mol_panel"  style="background-color:transparent;">
        <div class="heading" style="background-color:transparent;">
            <div class="title">Suit Lining</div>
            <img id="close11" class="close" src="images/close.png" onclick="camerAnim(this.id);closePanel()"/>
        </div>
        <div class="content" style="overflow: scroll;">
            <div id="000000" onclick="liningTexChange(this.id);" class="content_inner Lining active" title="Lining|Black.png">
                <img  src="images/Lining/Black.png"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Black</div>
            </div>
            <div id="151b28" onclick="liningTexChange(this.id);" class="content_inner Lining" title="Lining|Blue.png">
                <img  src="images/Lining/Blue.png"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Blue</div>
            </div>
            <div id="1c0800" onclick="liningTexChange(this.id);" class="content_inner Lining" title="Lining|Brown.png">
                <img  src="images/Lining/Brown.png"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Brown</div>
            </div>
            <div id="1e130e" onclick="liningTexChange(this.id);" class="content_inner Lining" title="Lining|Coffee.png">
                <img  src="images/Lining/Coffee.png"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Coffee</div>
            </div>
            <div id="000f26" onclick="liningTexChange(this.id);" class="content_inner Lining" title="Lining|Dark Blue.png">
                <img  src="images/Lining/Dark Blue.png"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Dark Blue</div>
            </div>
            <div id="081108" onclick="liningTexChange(this.id);" class="content_inner Lining" title="Lining|Green.png">
                <img  src="images/Lining/Green.png"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Green</div>
            </div>
            <div id="05010f" onclick="liningTexChange(this.id);" class="content_inner Lining" title="Lining|Navy.png">
                <img  src="images/Lining/Navy.png"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Navy</div>
            </div>
            <div id="080119" onclick="liningTexChange(this.id);" class="content_inner Lining" title="Lining|Royal.png">
                <img  src="images/Lining/Royal.png"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Royal</div>
            </div>
            <div id="7c7310" onclick="liningTexChange(this.id);" class="content_inner placket" title="Lining|Yellow.png">
                <img  src="images/Lining/Yellow.png"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Yellow</div>
            </div>
        </div>
    </div>
    <!--End of Lining -->
    <!-- Start Of Lapel sleeve-->
    <div id="panel_sleeve" class="panel mol_panel" style="background-color:transparent;">
        <div class="heading" style="background-color:transparent;">
            <div class="title">sleeve</div>
            <img id="close12" class="close" src="images/close.png" onclick="camerAnim(this.id);closePanel()"/>
        </div>
        <div class="content">
            <div id="MS-SleeveBtn_Four_B" onclick="btnMeshChange(this.id);" class="content_inner sleeve active" title="sleeve|Sleeve 4 Button.svg">
                <img  src="images/sleeve/Sleeve 4 Button.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Sleeve 4 button<br/></div>
            </div>
            <div id="MS-SleeveBtn_Three_B" onclick="btnMeshChange(this.id);" class="content_inner sleeve" title="sleeve|Sleeve 3 Button.svg">
                <img  src="images/sleeve/Sleeve 3 Button.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">sleeve 3 button<br/></div>
            </div>
            <div id="MS-SleeveBtn" onclick="btnMeshChange(this.id);" class="content_inner sleeve" title="sleeve|Sleeve without Button.svg">
                <img  src="images/sleeve/Sleeve without Button.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">No thanks<br/></div>
            </div>
        </div>
    </div>
    <!-- End Of Lapel sleeve -->
    <!-- Start Of Lapel stitch-->
    <div id="panel_stitch" class="panel mol_panel" style="background-color:transparent;">
        <div class="heading" style="background-color:transparent;">
            <div class="title">Stitch</div>
            <img id="close13" class="close" src="images/close.png" onclick="camerAnim(this.id);closePanel()"/>
        </div>
        <div class="content">
            <div id="MS-Lap_Std-One" onclick="addThread(this.id);" class="content_inner Stich active" title="Stich|Lapel Stitch.svg">
                <img  src="images/Stich/Lapel Stitch.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Lapel Stitch<br/></div>
            </div>
            <div id="MS-Lap_Std-One_T" onclick="removeThread(this.id);" class="content_inner Stich" title="Stich|Without Lapel Stitch.svg">
                <img  src="images/Stich/Without Lapel Stitch.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Without Lapel Stitch<br/></div>
            </div>
        </div>
    </div>
    <!-- End Of Lapel stitch -->
    <!-- Start Of Pocket -->
    <div id="panel_pocket" class="panel mol_panel" style="background-color:transparent;" >
        <div class="heading" style="background-color:transparent;">
            <div class="title">Pocket</div>
            <img id="close14" class="close" src="images/close.png" onclick="camerAnim(this.id);closePanel()"/>
        </div>
        <div class="content">
            <div id="MS-Poc_Welt" onclick="pocketChange(this.id);" class="content_inner pocket active" title="pocket|Welt Pocket.svg">
                <img  src="images/pocket/Welt Pocket.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Welt pocket</div>
            </div>
            <div id="MS-Poc_Flap" onclick="pocketChange(this.id);" class="content_inner pocket" title="pocket|Pocket with flap.svg">
                <img  src="images/pocket/Pocket with flap.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Pocket with flap</div>
            </div>
        </div>
    </div>
    <!--End Of Pocket-->
    <!-- Start Of ticket -->
    <div id="panel_ticket" class="panel mol_panel" style="background-color:transparent;">
        <div class="heading regular_ticket" style="background-color:transparent;">
            <div class="title">Regular Ticket Pocket</div>
            <img id="close15" class="close" src="images/close.png" onclick="camerAnim(this.id);closePanel()"/>
        </div>
        <div class="content regular_ticket">
            <div id="MS-TKPoc" onclick="ticketPocChange(this.id);" class="content_inner ticket active" title="ticket|Ticket Pocket.svg">
                <img  src="images/ticket/No Ticket Pocket.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">No Thanks</div>
            </div>
            <div id="MS-TKPoc_R" onclick="ticketPocChange(this.id);" class="content_inner ticket" title="ticket|Ticket Pocket.svg">
                <img  src="images/ticket/Ticket Pocket.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Ticket Pocket</div>
            </div>
        </div>
        <div class="heading slim_ticket" style="background-color:transparent;">
            <div class="title">Slim Ticket Pocket</div>
            <img id="close16" class="close" src="images/close.png" onclick="closePanel()"/>
        </div>
        <div class="content slim_ticket">
            <div id="MS-TKPoc" onclick="ticketPocChange(this.id);" class="content_inner ticket active" title="ticket|Ticket Pocket.svg">
                <img  src="images/ticket/No Ticket Pocket.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">No Thanks</div>
            </div>
            <div id="MS-TKPoc_S" onclick="ticketPocChange(this.id);" class="content_inner ticket" title="ticket|Ticket Pocket.svg">
                <img  src="images/ticket/Ticket Pocket.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Ticket Pocket</div>
            </div>
        </div>
    </div>
    <!--End Of ticket-->
    <!--Start Of Patch -->
    <div id="panel_elbow" class="panel mol_panel" style="background-color:transparent;">
        <div class="heading" style="background-color:transparent;">
            <div class="title">Elbow Fit</div>
            <img id="close17" class="close" src="images/close.png" onclick="camerAnim(this.id);closePanel()"/>
        </div>
        <div class="content">
            <div id="MS-Elbow" onclick="elbowChange(this.id);" class="content_inner elbow active" title="elbow|No Elbow Patch.svg">
                <img  src="images/elbow/No Elbow Patch.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">No Elbow patch</div>
            </div>
            <div id="MS-Elbow_Patch" onclick="elbowChange(this.id);" class="content_inner elbow" title="elbow|Elbow Patch.svg">
                <img  src="images/elbow/Elbow Patch.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Elbow patch</div>
            </div>
        </div>
    </div>
    <!--End of patch-->
    <!--Start of Elbow Fabric -->
    <div id="panel_elbow_fabric" class="panel mol_panel" style="background-color:transparent;">
        <div class="heading" style="background-color:transparent;">
            <div class="title">Elbow Fabric</div>
            <img id="close18" class="close" src="images/close.png" onclick="camerAnim(this.id);closePanel();closePanel1();"/>
        </div>
        <div class="content" style="overflow-y:scroll;">
            <div id="elbow_0" onclick="elbowFab(this.id);" class="content_inner Navy stripe elbowfabric active" title="elbowfabric|AR 2231 01 600 all wool 260G.jpg">
                <img src="images/elbowfabric/AR 2231 01 600 all wool 260G.jpg"  />
                <div>Navy Stripe</div>
            </div>
            <div id="elbow_1" onclick="elbowFab(this.id);" class="content_inner Black stripe elbowfabric" title="elbowfabric|AR 2235 05 580 73 wool 27 mohair 290 gr.jpg">
                <img src="images/elbowfabric/AR 2235 05 580 73 wool 27 mohair 290 gr.jpg"  />
                <div>Black Stripe</div>
            </div>
            <div id="elbow_2" onclick="elbowFab(this.id);" class="content_inner Blue stripe elbowfabric" title="elbowfabric|AR 2251 01 600 all wool 260G.jpg">
                <img src="images/elbowfabric/AR 2251 01 600 all wool 260G.jpg"  />
                <div>Blue Stripe</div>
            </div>
            <div id="elbow_3" onclick="elbowFab(this.id);" class="content_inner Grey stripe elbowfabric" title="elbowfabric|AR 2251 02 600 all wool 260g.jpg">
                <img src="images/elbowfabric/AR 2251 02 600 all wool 260g.jpg"  />
                <div>Dark Grey Stripe</div>
            </div>
            <div id="elbow_4" onclick="elbowFab(this.id);" class="content_inner stripe Grey elbowfabric" title="elbowfabric|AR 2251 16 600 all wool 260gr.jpg">
                <img src="images/elbowfabric/AR 2251 16 600 all wool 260gr.jpg"  />
                <div>Light Grey Stripe</div>
            </div>
            <div id="elbow_5" onclick="elbowFab(this.id);" class="content_inner Grey stripe elbowfabric" title="elbowfabric|AR 2251 17 600 all wool 260gr.jpg">
                <img src="images/elbowfabric/AR 2251 17 600 all wool 260gr.jpg"  />
                <div>Steel Grey Stripe</div>
            </div>
            <div id="elbow_6" onclick="elbowFab(this.id);" class="content_inner Blue check elbowfabric" title="elbowfabric|AR 2251 49 600 all wool 260gr.jpg">
                <img src="images/elbowfabric/AR 2251 49 600 all wool 260gr.jpg"  />
                <div>Blue Check</div>
            </div>
            <div id="elbow_7" onclick="elbowFab(this.id);" class="content_inner Grey check elbowfabric" title="elbowfabric|AR 2251 50 600 all wool 260g.jpg">
                <img src="images/elbowfabric/AR 2251 50 600 all wool 260g.jpg"  />
                <div>Grey Check</div>
            </div>
            <div id="elbow_8" onclick="elbowFab(this.id);" class="content_inner Navy check elbowfabric" title="elbowfabric|AR 2251 55 600 all wool 260gr.jpg">
                <img src="images/elbowfabric/AR 2251 55 600 all wool 260gr.jpg"  />
                <div>Navy Check</div>
            </div>
            <div id="elbow_9" onclick="elbowFab(this.id);" class="content_inner Navy check elbowfabric" title="elbowfabric|AR 2251 56 600 all wool 260gr.jpg">
                <img src="images/elbowfabric/AR 2251 56 600 all wool 260gr.jpg"  />
                <div>Navy Check</div>
            </div>
            <div id="elbow_10" onclick="elbowFab(this.id);" class="content_inner Brown check elbowfabric" title="elbowfabric|AR 2251 56 600 all wool 260gr_1.jpg">
                <img src="images/elbowfabric/AR 2251 56 600 all wool 260gr_1.jpg"  />
                <div>Brown Check</div>
            </div>
            <div id="elbow_11" onclick="elbowFab(this.id);" class="content_inner Gray check elbowfabric" title="elbowfabric|AR 2251 58 600 all wool 260gr_1.jpg">
                <img src="images/elbowfabric/AR 2251 58 600 all wool 260gr_1.jpg"  />
                <div>Gray Check</div>
            </div>
            <div id="elbow_12" onclick="elbowFab(this.id);" class="content_inner Brown stripe elbowfabric" title="elbowfabric|AR 2258 74 560 sunny season.jpg">
                <img src="images/elbowfabric/AR 2258 74 560 sunny season.jpg"  />
                <div>Brown HB</div>
            </div>
            <div id="elbow_13" onclick="elbowFab(this.id);" class="content_inner Gray stripe elbowfabric" title="elbowfabric|AR 2258 75 560 sunny season.jpg">
                <img src="images/elbowfabric/AR 2258 75 560 sunny season.jpg"  />
                <div>Gray HB</div>
            </div>
        </div>
    </div>
    <!--End Of Elbow fabric-->
    <!--Start OF Thread -->
    <div id="panel_thread" class="panel mol_panel" style="background-color:transparent;">
        <div class="heading" style="background-color:transparent;">
            <div class="title">Thread</div>
            <img id="close19" class="close" src="images/close.png" onclick="closePanel()"/>
        </div>
        <div class="content" style="overflow-y:scroll;">
            <div id="ffffff" onclick="threadColor(this.id);" class="content_inner thread active" title="thread|white.png">
                <img class="bottom_img" src="images/thread/White.png"/>
                <div style="font-family: 'Times New Roman', Times, serif;">White</div>
            </div>
            <div id="000000" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Black.png">
                <img class="bottom_img" src="images/thread/Black.png"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Black</div>
            </div>
            <div id="331f1d" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Brown.png">
                <img class="bottom_img"src="images/thread/Brown.png"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Brown</div>
            </div>
            <div id="8e8e8e" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Gray.png">
                <img class="bottom_img" src="images/thread/Gray.png"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Gray</div>
            </div>
            <div id="fff4db" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Ivory.png">
                <img class="bottom_img" src="images/thread/Ivory.png"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Ivory</div>
            </div>
            <div id="252847" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Navy.png">
                <img class="bottom_img" src="images/thread/Navy.png"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Navy</div>
            </div>
        </div>
    </div>
    <!--End of Thread-->
    <!--Start Of button_color -->
    <div id="panel_button_color" class="panel mol_panel" style="background-color:transparent;">
        <div class="heading" style="background-color:transparent;">
            <div class="title">Button</div>
            <img id="close20" class="close" src="images/close.png" onclick="camerAnim(this.id);closePanel()"/>
        </div>
        <div class="content" style="overflow-y:scroll;">
            <div id="f1f4e5" onclick="buttonColor(this.id);" class="content_inner button active" title="button|Sorrento-white.png">
                <img  src="images/button/Sorrento-white.png"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Sorrento white</div>
            </div>
            <div id="161817" onclick="buttonColor(this.id);" class="content_inner button" title="button|Sorrento-black.png">
                <img  src="images/button/Sorrento-black.png"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Sorrento black</div>
            </div>
            <div id="f27597" onclick="buttonColor(this.id);" class="content_inner button" title="button|Langa-Cerise.png">
                <img  src="images/button/Langa-Cerise.png"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Langa Cerise</div>
            </div>
            <div id="ece1d5" onclick="buttonColor(this.id);" class="content_inner button" title="button|Tineo-Ivory.png">
                <img  src="images/button/Tineo-Ivory.png"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Tineo Ivory</div>
            </div>
            <div id="55161e" onclick="buttonColor(this.id);" class="content_inner button" title="button|Coria-Wine.png">
                <img  src="images/button/Coria-Wine.png"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Coria Wine</div>
            </div>
            <div id="8a5c61" onclick="buttonColor(this.id);" class="content_inner button" title="button|Sorrento-maroon.png">
                <img  src="images/button/Sorrento-maroon.png"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Sorrento maroon</div>
            </div>
            <div id="243a2e" onclick="buttonColor(this.id);" class="content_inner button" title="button|Sorrento-military green.png">
                <img  src="images/button/Sorrento-military green.png"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Sorrento-military green</div>
            </div>
            <div id="72275a" onclick="buttonColor(this.id);" class="content_inner button" title="button|Langa-Light Purple.png">
                <img  src="images/button/Langa-Light Purple.png"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Langa-Light Purple</div>
            </div>
            <div id="707370" onclick="buttonColor(this.id);" class="content_inner button" title="button|Sorrento-light gray.png">
                <img  src="images/button/Sorrento-light gray.png"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Sorrento-light gray</div>
            </div>
            <div id="4a67af" onclick="buttonColor(this.id);" class="content_inner button" title="button|Langa-Blue.png">
                <img  src="images/button/Langa-Blue.png" />
                <div style="font-family: 'Times New Roman', Times, serif;">Langa-Blue</div>
            </div>
            <div id="2b564d" onclick="buttonColor(this.id);" class="content_inner button" title="button|Langa-Green.png">
                <img  src="images/button/Langa-Green.png"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Langa-Green</div>
            </div>
            <div id="21204c" onclick="buttonColor(this.id);" class="content_inner button" title="button|Coria-Dark Blue.png">
                <img  src="images/button/Coria-Dark Blue.png"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Coria-Dark Blue</div>
            </div>
            <div id="1b1a1b" onclick="buttonColor(this.id);" class="content_inner button" title="button|Coria-Black.png">
                <img  src="images/button/Coria-Black.png"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Coria-Black</div>
            </div>
            <div id="772326" onclick="buttonColor(this.id);" class="content_inner button" title="button|Coria-Wine1.png">
                <img  src="images/button/Coria-Wine1.png"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Coria-Wine1</div>
            </div>
            <div id="bf653b" onclick="buttonColor(this.id);" class="content_inner button" title="button|Langa-Orange.png">
                <img  src="images/button/Langa-Orange.png"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Langa-Orange</div>
            </div>
            <div id="e7dcd1" onclick="buttonColor(this.id);" class="content_inner button" title="button|Girona-Ivory.png">
                <img  src="images/button/Girona-Ivory.png"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Girona-Ivory</div>
            </div>
            <div id="9ba0a6" onclick="buttonColor(this.id);" class="content_inner button" title="button|Girona-Light Gray.png">
                <img  src="images/button/Girona-Light Gray.png"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Girona-Light Gray</div>
            </div>
            <div id="3e2a22" onclick="buttonColor(this.id);" class="content_inner button" title="button|Coria-Brown.png">
                <img  src="images/button/Coria-Brown.png"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Coria-Brown</div>
            </div>
            <div id="06082d" onclick="buttonColor(this.id);" class="content_inner button" title="button|Coria-Dark Blue1.png">
                <img  src="images/button/Coria-Dark Blue1.png"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Coria-Dark Blue1</div>
            </div>
            <div id="ae89b4" onclick="buttonColor(this.id);" class="content_inner button" title="button|Langa-Dark Purple.png">
                <img  src="images/button/Langa-Dark Purple.png"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Langa-Dark Purple</div>
            </div>
            <div id="61b53a" onclick="buttonColor(this.id);" class="content_inner button" title="button|Langa-Lime.png">
                <img  src="images/button/Langa-Lime.png"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Langa-Lime</div>
            </div>
            <div id="8d8c8c" onclick="buttonColor(this.id);" class="content_inner button" title="button|Silver-(Push).png">
                <img  src="images/button/Silver-(Push).png"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Silver-(Push)</div>
            </div>
            <div id="a89860" onclick="buttonColor(this.id);" class="content_inner button" title="button|Gold-(Push).png">
                <img  src="images/button/Gold-(Push).png"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Gold-(Push)</div>
            </div>
            <div id="61b53a" onclick="buttonColor(this.id);" class="content_inner button" title="button|Langa-Lime.png">
                <img  src="images/button/Langa-Lime.png"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Langa-Lime</div>
            </div>
            <div id="5b5d62" onclick="buttonColor(this.id);" class="content_inner button" title="button|Black-(Push).png">
                <img  src="images/button/Black-(Push).png"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Black-(Push)</div>
            </div>
            <div id="b1ae99" onclick="buttonColor(this.id);" class="content_inner button" title="button|Ivory-(Push).png">
                <img  src="images/button/Ivory-(Push).png"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Ivory-(Push)</div>
            </div>
        </div>
    </div>
    <!--End of button_color-->
<!--    <input type="button" value="Add to Cart" onclick="addToCart()" class="btn cartbtn" />-->
    </body>
    <script>
        var selectedfabric = "fabric_0";
        var selectedfit = "regular";
        var selectedbutton = "one";
        var selectedlapel = "standard";
        var selectedsleevebtn = "four";
        var selectedlapelstich = "yes";
        var selectedpocket = "welt";
        var selectedticketpocket = "no";
        var selectedvent = "one";
        var selectedelbowpatch = "no";
        var selectedelbowfabric = "";
        var selectedsuitlinning = "000000";
        var selectedbuttoncolor = "f1f4e5";
        var selectedthreadcolor = "ffffff";
    </script>
    <script src="js/libs/uiblock.js"></script>
    <script src="js/libs/TweenMax.js"></script>
    <script src="js/libs/three.min.js"></script>
    <script src="js/libs/OrbitControls.js"></script>
    <script src="js/libs/lzma.js"></script>
    <script src="js/libs/ctm.js"></script>
    <script src="js/libs/CTMLoader.js"></script>
    <script src="js/ms-Suit/m-Suit_JSON.js"></script>
    <script src="js/ms-Suit/otherLoad.js"></script>
    <script src="js/ms-Suit/m-Suit.js"></script>
    <script src="js/ms-Suit/ms-Suit-MeshChange.js"></script>
    <script src="js/ms-Suit/m-Suit_Cam.js"></script>
    <script>
        function addToCart() {
            var user_id = $("#user_id").val();
            if(user_id=="") {
                $("#myModalLogin").modal("show");
                return false;
            }
            $(".backloader").show();
            var product_name = "Men Suit"+Math.floor(Math.random()*10000);
            var url = "admin/api/orderProcess.php";
            $.post(url,{"type":"addToCart_su","product_type":"men_suit","product_name":product_name,"product_price":"550"
                ,"user_id":user_id,"quantity":"1","total_amount":"550","su_fabric":selectedfabric,"su_fit":selectedfit,
                "su_button":selectedbutton,"su_lapel":selectedlapel,"su_sleeve_button":selectedsleevebtn,
                "su_lapel_stich":selectedlapelstich,"su_pocket":selectedpocket,"su_ticket_pocket":selectedticketpocket,
                "su_vent":selectedvent,"su_elbow_patch":selectedelbowpatch,"su_elbow_fabric":selectedelbowfabric,
                "su_suit_linning":selectedsuitlinning,"su_button_color":selectedbuttoncolor,
                "su_thread_color":selectedthreadcolor},function(data){
                var Status = data.Status;
                if(Status == "Success"){
                    $(".backloader").hide();
                    showCartCount(data.cartCount);
                    window.location="cart.php";
                }else{
                    $(".backloader").hide();
                    $(".modal-title").html("<label style='color:red'>Error Occured</label>");
                    $(".modal-body").html(data.Message);
                    $("#modal").modal("show");
                }
            }).fail(function(){
                $(".backloader").hide();
                $(".modal-title").html("<label style='color:red'>Error Occured</label>");
                $(".modal-body").html("Server Error !!! Please Try After Some Time...");
                $("#modal").modal("show");
            });
        }
        function getCartCount(){
            var user_id = $("#user_id").val();
            var url = "admin/api/orderProcess.php";
            $.post(url,{"type":"getCartCount","user_id":user_id},function(data){
                var Status = data.Status;
                if(Status == "Success"){
                    showCartCount(data.count);
                }
            }).fail(function(){
                $(".modal-title").html("<label style='color:red'>Error Occured</label>");
                $(".modal-body").html("Server Error !!! Please Try After Some Time...");
                $("#modal").modal("show");
            });
        }
        function showCartCount(count){
            $("#cartCount").html(count);
        }
        function opencart(){
            window.location="cart.php";
        }
        getCartCount();
    </script>
    <div id="modal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                </div>
                <div class="modal-body">
                    <p>Some text in the modal.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
<?php include("footer3.php"); ?>