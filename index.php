<?php 
error_reporting(0);
include("header.php");?>
<!-- body start here -->
<!-- banner-bottom -->
	<div id="about" class="banner-bottom">
		<div class="container">
		<div class="tittle-agileinfo">
				<h3 class="heading-text color-gold text-shadow-gold">About Us</h3>
			</div>
			<div class="w3ls_banner_bottom_grid1">
				<div class="col-md-6 w3l_banner_bottom_left">	
					<!-- <img src="images/exq1.jpg" alt=" " class="img-responsive" /> -->
					<a href="customize.php"><img src="images/exq1.jpg" alt=" " class="img-responsive" /></a>
				</div>
				<div class="col-md-6 w3l_banner_bottom_right">
					<h3 class="subheading-w3l sub-heading-text ">Welcome to <span>Exquisuits</span> Designs</h3>
					<!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vel gravida tortor, molestie venenatis mauris.<br> Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec at vestibulum nunc.</p> -->
					<p class="normal-text">EXQUISUITS IS A NEW SARTORIAL CONCEPT THAT ENABLES YOU TO CUSTOMIZE YOUR SUITS ONLINE TO 
						YOUR STYLE, WITH THE MAXIMUM PERSONALIZATION AND DESIGN OPTIONS, AS WELL AS THE BEST 
						QUALITY-PRICE RELATIONSHIP.</p>
				<ul>
					<li class="normal-text"><i class="fa fa-clone" aria-hidden="true"></i>Exquisuits – Garments that produce sensation</li>
					<li class="normal-text"><i class="fa fa-clone" aria-hidden="true"></i>Exquisuits  Proverbial quality </li>
					<li class="normal-text"><i class="fa fa-clone" aria-hidden="true"></i>Hight design by Javier De Juana</li>
					<li class="normal-text"><i class="fa fa-clone" aria-hidden="true"></i>Maximum level of service</li>
				</ul>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
<!-- //banner-bottom -->
<!--latest-works-->
<div class="latest-works" id="works">
<div class="container">
			<div class="tittle-agileinfo">
				<h3 class="white-w3ls heading-text color-gold text-shadow-gold">Latest Works</h3>
			</div>
			<div class="col-md-8 agileits_banner_bottom_grid_three">
			<a href="customize.php">
				<div class="agileinfo_banner_bottom_grid_three_left">
					<div class="wthree_banner_bottom_grid_three_left1 grid">
						<figure class="effect-roxy">
							<img src="images/a2.jpg" alt=" " class="img-responsive" />
							<figcaption>
								<h3  class=" sub-heading-text color-gold"><span>M</span>ending</h3>
								<p class="normal-text">Our approach is a tad rakish but rooted in classicism.</p>
							</figcaption>			
						</figure>
					</div>
					<p class="w3_agileits_para normal-text">During your first appointment, you will peruse fabrics, discuss your preferences for fit and style. Additionally, we will scan, measure you and have you slip on some try on garments we keep in house for reference.</p>
				</div>
				</a>
				<a href="customize.php">
				<div class="agileinfo_banner_bottom_grid_three_left">
				<p class="w3_agileits_para normal-text">Once you hear from us, you can book your next fitting online. When you come in, we will slip on the unfinished garment, make all sizing adjustments and finish tailoring your order. You will hear from us within 5-7 business days with a new appointment time.</p>
					<div class="wthree_banner_bottom_grid_three_left1 grid">
						<figure class="effect-roxy">
							<img src="images/a3.jpg" alt=" " class="img-responsive" />
							<figcaption>
								<h3  class=" sub-heading-text color-gold"><span>D</span>esigning</h3>
								<p class="normal-text">Our custom shirt offering combines the best craftsmanship with world class fabrics and scientific precision. </p>
							</figcaption>			
						</figure>
					</div>
					
				</div>
				</a>
				<div class="clearfix"> </div>
				</div>
				<div class="col-md-4 agileinfo_banner_bottom_grid_three_left bnr-btm3">
				<a href="customize.php">
					<div class="wthree_banner_bottom_grid_three_left1 grid">
						<figure class="effect-roxy">
							<img src="images/a4.jpg" alt=" " class="img-responsive" />
							<figcaption>
								<h3  class="sub-heading-text color-gold"><span>A</span>lteration</h3>
								<p class="normal-text">Each shirt pattern is individually drafted for each client to shape the torso seamlessly and provide ease of movement.</p>
							</figcaption>			
						</figure>
					</div>
					</a>
					<p class="w3_agileits_para normal-text">Once we have perfected your fit, the order is complete and you can reorder with minimal to no future alterations.</p>
				</div>
				
				<div class="clearfix"> </div>
				<div class="botton-agileits">
					<a href="customize.php" class="hvr-rectangle-in heading-text start-designing-btn">START DESIGNING</a>
				</div>
</div>
</div>
<!--//Latest works-->
<!-- services -->
	<div class="services" id="services">
		<div class="container">
		<div class="tittle-agileinfo">
				<h3 class="heading-text color-gold text-shadow-gold">Featured Services</h3>
			</div>
			<div class="w3_agileits_services_grids">
			<div class="w3_agileits_services_left">
				<img src="images/gents1.png" alt=""/>
			</div>
			<div class="w3_agileits_services_right">
				<div class="col-md-6 w3_agileits_services_grid">
					<div class="w3_agileits_services_grid_agile">
						<div class="w3_agileits_services_grid_1">
							<i class="fa fa-male" aria-hidden="true"></i>
						</div>
						<h3 class="sub-heading-text on-hover-gold-color">Designing</h3>
						<p class="normal-text">When you wear our clothing, you will see and feel why quality should never be compromised.</p>
					</div>
				</div>
				<div class="col-md-6 w3_agileits_services_grid">
					<div class="w3_agileits_services_grid_agile">
						<div class="w3_agileits_services_grid_1">
							<i class="fa fa-scissors" aria-hidden="true"></i>
						</div>
						<h3 class="sub-heading-text on-hover-gold-color">Alteration</h3>
						<p class="normal-text">We use hand work where it adds functional value and premier machine work for precision.</p>
					</div>
				</div>
				<div class="col-md-6 w3_agileits_services_grid">
					<div class="w3_agileits_services_grid_agile">
						<div class="w3_agileits_services_grid_1">
							<i class="fa fa-thumb-tack" aria-hidden="true"></i>
						</div>
						<h3 class="sub-heading-text on-hover-gold-color">Sewing</h3>
						<p class="normal-text">We offer best-in-class pricing for quality clothing that will stand the test of time.</p>
					</div>
				</div>
				<div class="col-md-6 w3_agileits_services_grid">
					<div class="w3_agileits_services_grid_agile">
						<div class="w3_agileits_services_grid_1">
							<i class="fa fa-puzzle-piece" aria-hidden="true"></i>
						</div>
						<h3 class="sub-heading-text  on-hover-gold-color">applique</h3>
						<p class="normal-text">Our prices also represent significant value as our clothing stands the test of time.</p>
					</div>
				</div>
				<div class="col-md-6 w3_agileits_services_grid">
					<div class="w3_agileits_services_grid_agile">
						<div class="w3_agileits_services_grid_1">
							<i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
						</div>
						<h3 class="sub-heading-text on-hover-gold-color">modernization</h3>
						<p class="normal-text">We are happy to offer premium fabrics at incredible prices</p>
					</div>
				</div>
				<div class="col-md-6 w3_agileits_services_grid">
					<div class="w3_agileits_services_grid_agile">
						<div class="w3_agileits_services_grid_1">
							<i class="fa fa-link" aria-hidden="true"></i>
						</div>
						<h3 class="sub-heading-text on-hover-gold-color">Mending</h3>
						<p class="normal-text">Fabric is the foundation of clothing and it is a requirement for quality.</p>
					</div>
				</div>
				<div class="clearfix"> </div>
				</div>
			</div>
		</div>
	</div>
<!-- //services -->
<!-- about -->
<div class="about-w3layouts" >
<div class="container">
	<div class="tittle-agileinfo">
				<h3 class="heading-text white-w3ls color-gold ">Some Interesting Facts</h3>
			</div>
	<!-- schedule-bottom -->
	<div class="schedule-bottom">
		<div class="agileits_schedule_bottom_right">
			<div class="w3ls_schedule_bottom_right_grid">
				<h3 class="subheading-w3l sub-heading-text  white-w3ls color-gold">Exquisuits <span>Designs</span> can change how you look at <span>yourself</span>.</h3>
				<p class="normal-text">We begin with top tier fabrics which tailor distinctly better resulting in a much more elegant suit with a very notable difference in ease of movement, durability and most importantly the drape of the suit.</p>
				<div class="col-md-4 w3l_schedule_bottom_right_grid1">
					<i class="fa fa-user-o" aria-hidden="true"></i>
					<h4 class="normal-text">Customers</h4>
					<h5 class="counter">792</h5>
				</div>
				<div class="col-md-4 w3l_schedule_bottom_right_grid1">
					<i class="fa fa-calendar-o" aria-hidden="true"></i>
					<h4 class="normal-text">Designs</h4>
					<h5 class="normal-text counter">533</h5>
				</div>
				<div class="col-md-4 w3l_schedule_bottom_right_grid1">
					<i class="fa fa-shield" aria-hidden="true"></i>
					<h4 class="normal-text">Awards</h4>
					<h5 class="normal-text counter">95</h5>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
		<div class="clearfix"> </div>
	</div>
	<div class="botton-agileits">
		<a href="customize.php" class="hvr-rectangle-in heading-text start-designing-btn">START DESIGNING</a>
	</div>
<!-- //schedule-bottom -->
</div>
</div>
<!-- //about-bottom -->
<!-- team -->
<!-- <div class="team" id="experts">
	 <div class="container">
		<div class="tittle-agileinfo">
		<h3>Meet our experts</h3>
		</div>
		<div class="w3ls_banner_bottom_grids">
		<div class="col-md-8 team-agileits-left">
			<div class="w3_agile_team_grid">
				<div class="w3layouts_team_grid team1">
					<div class="w3layouts_team_grid_pos">
						<div class="wthree_text">
							<ul class="agileits_social_list">
								<li><a href="#" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
								<li><a href="#" class="agile_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
								<li><a href="#" class="w3_agile_dribble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
				
					<div class="team1-info-w3ls">
					<h6>Regan Cole</h6>
					<span>Designer</span>
					<p>Donec semper rutrum ipsum et bibendum.</p>
					</div>
					<div class="clearfix"> </div>
			</div>
			<div class="w3_agile_team_grid">
				<div class="w3layouts_team_grid team2">
					<div class="w3layouts_team_grid_pos">
						<div class="wthree_text">
							<ul class="agileits_social_list">
								<li><a href="#" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
								<li><a href="#" class="agile_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
								<li><a href="#" class="w3_agile_dribble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
				
					<div class="team2-info-w3ls">
					<h6>Sydney Taft</h6>
					<span>Designer</span>
					<p>Donec semper rutrum ipsum et bibendum.</p>
					</div>
					<div class="clearfix"> </div>
			</div>
			</div>
			<div class="col-md-4 team-agileits-right">
			<div class="w3_agile_team_grid">
				<div class="w3layouts_team_grid team3">
					<div class="w3layouts_team_grid_pos">
						<div class="wthree_text">
							<ul class="agileits_social_list">
								<li><a href="#" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
								<li><a href="#" class="agile_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
								<li><a href="#" class="w3_agile_dribble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="team3-info-w3ls">
					<h6>Chris Robinson</h6>
					<span>Designer</span>
					<p>Donec semper rutrum ipsum et bibendum.</p>
				</div>
				<div class="clearfix"> </div>
			</div>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div> 
</div> -->
<!-- //team -->
<!-- gallery -->	
	<div id="gallery" class="gallery">
		<div class="tittle-agileinfo">
			<h3 class="heading-text color-gold text-shadow-gold">Designs & works</h3>
		</div>
		<div class="w3ls_banner_bottom_grids">
			<div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
				<ul id="myTab" class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active normal-text"><a href="#designs" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true" class="normal-text">Designs</a></li>
					<li role="presentation" class="normal-text"><a href="#Alterations" role="tab" id="Alterations-tab" data-toggle="tab" aria-controls="Alterations" class="normal-text">Alterations</a></li>
					<li role="presentation normal-text"><a class="normal-text" href="#Mending" role="tab" id="Mending-tab" data-toggle="tab" aria-controls="Mending">Mending</a></li>
				</ul>
				<div id="myTabContent" class="tab-content">
					<div role="tabpanel" class="tab-pane fade in active" id="designs" aria-labelledby="home-tab">
						<div class="col-md-3 w3layouts_gallery_grid">
							<a class="normal-text" title="Donec sapien massa, placerat ac sodales ac, feugiat quis est." href="images/gents3.png">
								<div class="w3layouts_team_grid">
									<img src="images/gents3.png" alt=" " class="img-responsive" />
									<div class="w3layouts_team_grid_pos">
										<div class="wthree_text">
											<h4 class="heading-text color-gold">Exquisuits</h4>
											<p class="normal-text">Great look in Outfit</p>
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="col-md-3 w3layouts_gallery_grid">
							<a class="normal-text" title="Donec sapien massa, placerat ac sodales ac, feugiat quis est." href="images/gents6.png">
								<div class="w3layouts_team_grid">
									<img src="images/gents6.png" alt=" " class="img-responsive" />
									<div class="w3layouts_team_grid_pos">
										<div class="wthree_text">
											<h4 class="heading-text color-gold">Exquisuits</h4>
											<p class="normal-text">Great look in Outfit</p>
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="col-md-3 w3layouts_gallery_grid">
							<a class="normal-text" title="Donec sapien massa, placerat ac sodales ac, feugiat quis est." href="images/g3.jpg">
								<div class="w3layouts_team_grid">
									<img src="images/g3.jpg" alt=" " class="img-responsive" />
									<div class="w3layouts_team_grid_pos">
										<div class="wthree_text">
											<h4 class="heading-text color-gold">Exquisuits</h4>
											<p class="normal-text">Great look in Outfit</p>
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="col-md-3 w3layouts_gallery_grid">
							<a class="normal-text" title="Donec sapien massa, placerat ac sodales ac, feugiat quis est." href="images/g4.jpg">
								<div class="w3layouts_team_grid">
									<img src="images/g4.jpg" alt=" " class="img-responsive" />
									<div class="w3layouts_team_grid_pos">
										<div class="wthree_text">
											<h4 class="heading-text color-gold">Exquisuits</h4>
											<p class="normal-text">Great look in Outfit</p>
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="col-md-3 w3layouts_gallery_grid">
							<a class="normal-text" title="Donec sapien massa, placerat ac sodales ac, feugiat quis est." href="images/gents5.png">
								<div class="w3layouts_team_grid">
									<img src="images/gents5.png" alt=" " class="img-responsive" />
									<div class="w3layouts_team_grid_pos">
										<div class="wthree_text">
											<h4  class="heading-text color-gold">Exquisuits</h4>
											<p class="normal-text">Great look in Outfit</p>
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="col-md-3 w3layouts_gallery_grid">
							<a class="normal-text" title="Donec sapien massa, placerat ac sodales ac, feugiat quis est." href="images/garantia-imagen-web.png">
								<div class="w3layouts_team_grid">
									<img src="images/garantia-imagen-web.png" alt=" " class="img-responsive" />
									<div class="w3layouts_team_grid_pos">
										<div class="wthree_text">
											<h4 class="heading-text color-gold">Exquisuits</h4>
											<p class="normal-text">Great look in Outfit</p>
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="col-md-3 w3layouts_gallery_grid">
							<a class="normal-text"  title="Donec sapien massa, placerat ac sodales ac, feugiat quis est." href="images/gents4.png">
								<div class="w3layouts_team_grid">
									<img src="images/gents4.png" alt=" " class="img-responsive" />
									<div class="w3layouts_team_grid_pos">
										<div class="wthree_text">
											<h4>Exquisuits</h4>
											<p class="normal-text" >Great look in Outfit</p>
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="col-md-3 w3layouts_gallery_grid">
							<a class="normal-text"  title="Donec sapien massa, placerat ac sodales ac, feugiat quis est." href="images/g8.jpg">
								<div class="w3layouts_team_grid">
									<img src="images/g8.jpg" alt=" " class="img-responsive" />
									<div class="w3layouts_team_grid_pos">
										<div class="wthree_text">
											<h4  class="heading-text color-gold">Exquisuits</h4>
											<p class="normal-text" >Great look in Outfit</p>
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div role="tabpanel" class="tab-pane fade" id="Alterations" aria-labelledby="Alterations-tab">
						<div class="col-md-3 w3layouts_gallery_grid">
							<a  class="normal-text" title="Donec sapien massa, placerat ac sodales ac, feugiat quis est." href="images/banner-man3.png">
								<div class="w3layouts_team_grid">
									<img src="images/banner-man3.png" alt=" " class="img-responsive">
									<div class="w3layouts_team_grid_pos">
										<div class="wthree_text">
											<h4  class="heading-text color-gold">Exquisuits</h4>
											<p class="normal-text" >Great look in Outfit</p>
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="col-md-3 w3layouts_gallery_grid">
							<a class="normal-text"  title="Donec sapien massa, placerat ac sodales ac, feugiat quis est." href="images/luxury2a.png">
								<div class="w3layouts_team_grid">
									<img src="images/luxury2a.png" alt=" " class="img-responsive">
									<div class="w3layouts_team_grid_pos">
										<div class="wthree_text">
											<h4 class="heading-text color-gold">Exquisuits</h4>
											<p class="normal-text" >Great look in Outfit</p>
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="col-md-3 w3layouts_gallery_grid">
							<a class="normal-text"  title="Donec sapien massa, placerat ac sodales ac, feugiat quis est." href="images/g4.jpg">
								<div class="w3layouts_team_grid">
									<img src="images/g4.jpg" alt=" " class="img-responsive">
									<div class="w3layouts_team_grid_pos">
										<div class="wthree_text">
											<h4 class="heading-text color-gold">Exquisuits</h4>
											<p class="normal-text" >Great look in Outfit</p>
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="col-md-3 w3layouts_gallery_grid">
							<a  class="normal-text" title="Donec sapien massa, placerat ac sodales ac, feugiat quis est." href="images/banner-coleccion-luxury2.png">
								<div class="w3layouts_team_grid">
									<img src="images/banner-coleccion-luxury2.png" alt=" " class="img-responsive">
									<div class="w3layouts_team_grid_pos">
										<div class="wthree_text">
											<h4 class="heading-text color-gold">Exquisuits</h4>
											<p class="normal-text">Great look in Outfit</p>
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div role="tabpanel" class="tab-pane fade" id="Mending" aria-labelledby="Mending-tab">
						<div class="col-md-3 w3layouts_gallery_grid">
							<a class="normal-text" title="Donec sapien massa, placerat ac sodales ac, feugiat quis est." href="images/g8.jpg">
								<div class="w3layouts_team_grid">
									<img src="images/g8.jpg" alt=" " class="img-responsive">
									<div class="w3layouts_team_grid_pos">
										<div class="wthree_text">
											<h4 class="heading-text color-gold">Exquisuits</h4>
											<p class="normal-text">Great look in Outfit</p>
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="col-md-3 w3layouts_gallery_grid">
							<a class="normal-text" title="Donec sapien massa, placerat ac sodales ac, feugiat quis est." href="images/gents5.png">
								<div class="w3layouts_team_grid">
									<img src="images/gents5.png" alt=" " class="img-responsive">
									<div class="w3layouts_team_grid_pos">
										<div class="wthree_text">
											<h4 class="heading-text color-gold">Exquisuits</h4>
											<p class="normal-text">Great look in Outfit</p>
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="col-md-3 w3layouts_gallery_grid">
							<a  class="normal-text" title="Donec sapien massa, placerat ac sodales ac, feugiat quis est." href="images/g14.jpg">
								<div class="w3layouts_team_grid">
									<img src="images/g14.jpg" alt=" " class="img-responsive">
									<div class="w3layouts_team_grid_pos">
										<div class="wthree_text">
											<h4 class="heading-text color-gold">Exquisuits</h4>
											<p class="normal-text">Great look in Outfit</p>
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="col-md-3 w3layouts_gallery_grid">
							<a class="normal-text" title="Donec sapien massa, placerat ac sodales ac, feugiat quis est." href="images/garantia-imagen-web.png">
								<div class="w3layouts_team_grid">
									<img src="images/garantia-imagen-web.png" alt=" " class="img-responsive">
									<div class="w3layouts_team_grid_pos">
										<div class="wthree_text">
											<h4 class="heading-text color-gold">Exquisuits</h4>
											<p class="normal-text">Great look in Outfit</p>
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="col-md-3 w3layouts_gallery_grid">
							<a class="normal-text" title="Donec sapien massa, placerat ac sodales ac, feugiat quis est." href="images/gents4.png">
								<div class="w3layouts_team_grid">
									<img src="images/gents4.png" alt=" " class="img-responsive">
									<div class="w3layouts_team_grid_pos">
										<div class="wthree_text">
											<h4 class="heading-text color-gold">Exquisuits</h4>
											<p class="normal-text">Great look in Outfit</p>
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="col-md-3 w3layouts_gallery_grid">
							<a class="normal-text" title="Donec sapien massa, placerat ac sodales ac, feugiat quis est." href="images/banner-man3.png">
								<div class="w3layouts_team_grid">
									<img src="images/banner-man3.png" alt=" " class="img-responsive">
									<div class="w3layouts_team_grid_pos">
										<div class="wthree_text">
											<h4 class="heading-text color-gold">Exquisuits</h4>
											<p class="normal-text">Great look in Outfit</p>
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- //gallery -->
<div class="contact" id="contact">
	<div class="container">
		<div class="tittle-agileinfo">
			<h3 class="heading-text color-gold text-shadow-gold">contact us</h3>
		</div>
		<div class="agile-contact-grids">
			<div class="agile-contact-left">
				<div class="contact-form background-grey">
					<h3 class="white-w3ls subheading-w3l sub-heading-text color-gold">Contact Form</h3>
					<form action="#" method="post">
							<input class="color-black" type="text" name="Name" placeholder="Name" required="">
							<input class="color-black" type="email" name="Email" placeholder="Email" required=""> 
							<input class="color-black" type="text" name="Subject" placeholder="Subject" required="">
							<textarea class="color-black" name="Message" placeholder="Message" required=""></textarea>
						<input type="submit" value="SEND">
					</form>
				</div>
				<div class=" tittle-agileinfo address-grid">
					<h3 class="subheading-w3l sub-heading-text color-gold">Our Address</h3>
					<ul class="w3_address">
						<li class="sub-heading-text"><i class="fa fa-map-marker" aria-hidden="true"></i><span>Location</span><br>Avenida de Leioa, 14-B 48992 Neguri (Getxo) Bizkaia</li>
						<li class="sub-heading-text"><i class="fa fa-mobile" aria-hidden="true"></i><span>Phone</span><br>(+34) 944 914 137</li>
						<li class="sub-heading-text"><i class="fa fa-envelope-o" aria-hidden="true"></i><span>Email</span><br><a href="mailto:info@exquisuits.com">info@exquisuits.com</a></li>
					</ul>
				</div>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
</div>

<?php include("footer.php"); ?>