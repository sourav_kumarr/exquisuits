<h6 class="contact-w3ls heading-text">Call <span>(+34) 944 914 137</span> to make an appointment</h6>
<footer>
    <div class="container-fluid" style="padding: 0px">
        <div class="col-md-12" style="padding: 0px;margin:0px"><h6 class="contact-w3ls">Call <span>(+34) 944 914 137</span> to make an appointment</h6></div>

        <div class="footer well-7">
            <div class="row">
                <div class="grid-footer">
                    <a href="#"><img class="logopie" src="images/logopie.png" alt="Logo Javier de Juana sastrería a medida"></a>
                    <p>Avenida de Leioa, 14B 48992 Neguri (Getxo). Bizkaia - SPAIN</p>
                    <p><span>Tlf: (+34) 944 914 137 - Call to Make an Appointment</span></p>
                    <p><a target="_blank" href="https://www.exquisuits.com">https://www.exquisuits.com</a></p>
                </div>
            </div>
        </div>
    </div>
    <div class="grid-copyright">
        <p>&copy; 2017 Exquisuits by de Juana - <a href="#">Legal Terms</a> / <a href="#">Cookies Policy</a></p>
    </div>
</footer>
<!--<div class="footer-w3layouts background-grey">
<div class="container">
	<div class="col-md-4 footer-grids">
		<h3 class="color-gold heading-text">Services</h3>					
		<ul class="b-nav">
			<li><a class="scroll normal-text color-black" href="index.php">Home</a></li>
			<li><a class="scroll normal-text color-black" href="index.php" >About</a></li>
			<li><a class="scroll normal-text color-black" href="index.php">Customer Reviews</a></li>
			<li><a class="scroll normal-text color-black" href="index.php">Customer</a></li>
			<li><a class="scroll normal-text color-black" href="index.php">Gallery</a></li>
		</ul>
	</div>
	<div class="col-md-4 footer-grids">
		<h3 class="color-gold heading-text">About Exquisuits</h3>
		<p class="color-black normal-text">EXQUISUITS IS A NEW SARTORIAL CONCEPT THAT ENABLES YOU TO CUSTOMIZE YOUR SUITS ONLINE TO 
						YOUR STYLE.</p>
		<div class="botton-agileits">
			<a  class="hvr-rectangle-in scroll"  href="#about" >know more</a>
		</div>
		<ul class="agileits_social_list">
			<li><a href="https://www.facebook.com/Exquisuits/" class="w3_agile_facebook on-hover-background-black">
				<i class="fa fa-facebook" aria-hidden="true"></i></a></li>
			<li><a href="https://twitter.com/exquisuits" class="agile_twitter on-hover-background-black">
				<i class="fa fa-twitter" aria-hidden="true"></i></a></li>
			<li><a href="https://pinterest.com/exquisuits/" class="w3_agile_dribble on-hover-background-black">
				<i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
		</ul>
	</div>
	<div class="col-md-4 footer-grids">
		<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d11607.49745727848!2d-3.005152!3d43.337818!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x7b1eab41db51b696!2sExquisuits+by+de+Juana!5e0!3m2!1sen!2sin!4v1496988525076" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div>
</div>
</div>-->
	<!-- //contact -->
<!--footer-->
<!--/footer -->
<!-- js -->
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<script src="js/modernizr.js"></script>	<!--banner text -->
<script src="js/main.js"></script><!--banner text -->
<!-- stats -->
	<script src="js/jquery.waypoints.min.js"></script>
	<script src="js/jquery.countup.js"></script>
	<script>
		$('.counter').countUp();
	</script>
<!-- //stats -->
<!-- Lightbox -->
	<script src="js/simpleLightbox.js"></script>
	<script>
		$('.w3layouts_gallery_grid a').simpleLightbox();
	</script>
<!-- //Lightbox -->
<script src="js/SmoothScroll.min.js"></script>
<!-- smooth scrolling -->
	<script type="text/javascript">
		$(document).ready(function() {
		/*
			var defaults = {
			containerID: 'toTop', // fading element id
			containerHoverID: 'toTopHover', // fading element hover id
			scrollSpeed: 1200,
			easingType: 'linear' 
			};
		*/								
		$().UItoTop({ easingType: 'easeOutQuart' });
		});
	</script>
	<a href="#home" class="scroll" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<!-- //smooth scrolling -->
	<!-- start-smoth-scrolling -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="js/bootstrap-3.1.1.min.js"></script>
<script type="text/javascript" src="js/location.js"></script>
<!-- / end-smoth-scrolling -->

<!--modal-->
<script type="text/javascript" src="js/model.js"></script>
<!--/modal-->
<script type="text/javascript" src="js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript">

function f1() {
	    var a = $("#heightparam").val();
	   
	    if(a == "Feets") {
	        $("#heightp").html("<div class='row'>" +
	            "<input id='height0' class='form-control' type='text' placeholder='Feets' value='' style='float:left;" +
	            "margin-right:0px;width:40%' /><input type='text' id='height1' class='form-control' style='" +
	            "float:left;width:50%' placeholder='Inches' value='' /></div>");
	    }
	    else
	    {
	        $("#heightp").html("<input type='text' name='height' id='height' class='form-control' placeholder='Enter Height'>");
	    }
	}
	/*login check start here*/
	function loginCheck()
	{
		$(".error").html("");
		$("#logemail").val("");
		$("#logpassword").val("");
	}
	/*login check end   here*/

function setdate()
{
	var CountryName = $(".countries").val();
	if(CountryName == "United States")
	{
			$('.form_date').datetimepicker({
	       language:  'fr',
	        weekStart: 1,
	        //todayBtn:  1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 2,
			minView: 2,
			format: "mm/dd/yyyy",
			forceParse: 0
	    });			
  	}
	else
	{
			$('.form_date').datetimepicker({
	       language:  'fr',
	        weekStart: 1,
	        //todayBtn:  1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 2,
			minView: 2,
			format: "dd/mm/yyyy",
			forceParse: 0
	    });
	}
}

	

    $().ready(function () {
        $("#card").flip({
            trigger: 'manual'
        });
    });

    $("#unflip-btn").click(function () {
        $(".signin_form").css('opacity', '100');
        $(".signup_form").css('opacity', '0');
        $("#card").flip(false);
        return false;

    });

    /*cart start here*/
		function getCartCount(){
		    var user_id = $("#user_id").val();
		    if(user_id != ""){
		        var url = "admin/api/orderProcess.php";
		        $.post(url,{"type":"getCartCount","user_id":user_id},function(data){
		            var Status = data.Status;
		            if(Status == "Success"){
		                showCartCount(data.count);
		            }
		        }).fail(function(){
		            alert("error occured on url");
		        });
		    }
		}
		function showCartCount(count){
		    $("#cartCount").html(count);
		}
		function opencart(){
		    window.location="cart.php";
		}
		getCartCount();  


    /*cart end here*/
    /*logout start here*/
    function logout()
    {
    	window.location="logout.php";
    }
    /*logout end here*/

    /*function redirectTodiv()
	{
		var curUrl = window.location.href;
		var redirectTab=  curUrl.indexOf("?");
		if(redirectTab != -1)
		{
			redirectTab = redirectTab.split("?");
			redirectTab = redirectTab[1];
			alert("redirectTab = "+redirectTab);
		}
	}
	redirectTodiv();*/
/*
	setTimeout(function(){ alert("Hello"); }, 5000);*/
    
</script>
<script type="text/javascript" src="js/exquisuits.js"></script>
<link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css" />

</body>
</html>