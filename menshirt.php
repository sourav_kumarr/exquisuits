<?php
include("header2.php");

if(!isset($_SESSION['benj_user_id'])){
    echo "<input type='hidden' value='' id='user_id' />";
}
else{
    echo "<input type='hidden' value='".$_SESSION['benj_user_id']."' id='user_id' />";

}
?>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <link rel="stylesheet" href="css/shirtstyle.css">
        <!--    <link rel="stylesheet" href="css/bootstrap.min.css">-->
        <link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script src="js/Men-shirt_script.js"></script>
        <script src="js/libs/jquery-ui.min.js"></script>
        <script src="js/libs/angular.min.js"></script>
        <style>
            .progdiv {
                left: 40%;
                position: absolute;
                top: 50%;
                width: 30%;
                display:none;
            }
            .pload{
                left: 40%;
                position: absolute;
                top: 46%;
                width: 30%;
                display:none;
                text-align: center;
            }
            .cartbtn {
                position: fixed;
                right: 10px;
                top: 300px;
                width: 200px;
                z-index: 999999;
                display: none;
            }
        </style>
    </head>
    <body onload="hideDiv();">
    <div id="main-container">
        <div id="left_container" class="left-container">
            <div id="fit-icon-div" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_fit');toggleVisibility('panel_fit');">
                <img id="img_Fit" src="images/Fit/Regular_Fit.svg" />
                <div class="left_font">Shirt Fit</div>
            </div>
            <div id="fabric-icon-div" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_fabric');toggleVisibility('panel_fabric');">
                <img id="img_fabric" src="images/fabric/01_Blue_White_Dobby.jpg"/>
                <div class="left_font">Fabric</div>
            </div>
            <div id="sleeve-icon-div" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_sleeve');toggleVisibility('panel_sleeve');">
                <img id="img_Sleeve" src="images/Sleeve/01. Long Sleeve.svg" />
                <div class="left_font">Sleeve</div>
            </div>
            <div id="collar-icon-div" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_collar');toggleVisibility('panel_collar');">
                <img id="img_Collar" src="images/Collar/01_Business Classic.svg"/>
                <div class="left_font">Collar</div>
            </div>
            <div id="cuff-icon-div" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_cuff');toggleVisibility('panel_cuff');">
                <img id="img_Cuff" src="images/Cuff/01_Single Button Rounded.svg"/>
                <div class="left_font">Cuff</div>
            </div>
            <div id="placket-icon-div" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_placket');toggleVisibility('panel_placket');">
                <img id="img_Placket" src="images/Placket/01_With Placket.svg"/>
                <div class="left_font">Placket</div>
            </div>
            <div id="back_details-icon-div" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_back');toggleVisibility('panel_back');">
                <img id="img_Back_Details" src="images/Back_Details/01_No_Back_Details.svg"/>
                <div class="left_font">Back Details</div>
            </div>
            <div id="bottom-icon-div" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_bottomcut');toggleVisibility('panel_bottomcut');">
                <img id="img_Bottom_Cut" src="images/Bottom_Cut/01_Classic.svg"/>
                <div class="left_font">Bottom Cut</div>
            </div>
            <div id="pocket-icon-div" class="left_container_img " onclick="camerAnim(this.id);showPanel('panel_pocket_placement');toggleVisibility('panel_pocket_placement');">
                <img id="img_Pocket" src="images/Pocket/01. No Pocket.svg"/>
                <div class="left_font">Pocket</div>
            </div>
            <div id="pocket_style-icon-div" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_pocket_style');toggleVisibility('panel_pocket_style');">
                <img id="img_Pocket_Style"  src="images/Pocket_Style/01. Straight Pocket.svg" />
                <div class="left_font">Pocket Style</div>
            </div>
            <div id="pocket_flap-icon-div" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_flap');toggleVisibility('panel_flap');">
                <img id="img_Pocket_Flap" src="images/Pocket_Flap/02. Yes.svg" />
                <div class="left_font">Pocket Flap</div>
            </div>
            <div id="elbow-icon-div" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_elbow');toggleVisibility('panel_elbow');">
                <img id="img_elbow" style="border-radius:24px;" alt="elbow" src="images/elbow/Elbow Patch.svg"/>
                <div class="left_font">Patch</div>
            </div>
            <div id="button-icon-div" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_button');toggleVisibility('panel_button');">
                <img id="img_button" src="images/button/Sorrento-white.png" />
                <div class="left_font">Button</div>
            </div>
            <div id="thread-icon-div" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_thread');toggleVisibility('panel_thread');">
                <img id="img_thread"  src="images/thread/White.png" />
                <div class="left_font">Thread</div>
            </div>
            <div id="contrast-icon-div" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_contrast');toggleVisibility('panel_contrast');">
                <img  src="images/Contrast/00_Contrasts.svg" />
                <div class="left_font">Contrast</div>
            </div>
            <div id="contrast_fabric-icon-div" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_contrast_fabric');toggleVisibility('panel_contrast_fabric');">
                <img id="img_fabric1" src="images/fabric1/01_Blue_White_Dobby.jpg"/>
                <div class="left_font">Contrast Fabric</div>
            </div>
            <div id="embroidery-icon-div" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_embroidery');toggleVisibility('panel_embroidery');">
                <img id="img_embroidery" src="images/Embroidery.png"/>
                <div class="left_font">Embroidery</div>
            </div>
        </div>
        <!--End Of Left Container -->
    </div>
    <!--Start of Shirt Fit -->
    <p class="pload">Please Wait ... Object Is Loading</p>
    <div class="progress progdiv">
        <div id="prog" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40"
             aria-valuemin="0" aria-valuemax="100" style="width:0%">
            0%
        </div>
    </div>
    <div id="panel_fit" class="panel mol_panel" style="background-color:transparent;">
        <div class="heading" style="background-color:transparent;">
            <div class="title">Shirt Fit</div>
            <img id="close" class="close pull-left" src="images/close.png" onclick="camerAnim(this.id);closePanel()"/>
        </div>
        <div class="content">
            <div  id="fit_SlimFitModern"  onclick="collorMeshChange(this.id);" class="content_inner Fit active" title="Fit|Slim_Fit.svg">
                <img src="images/Fit/Slim_Fit.svg"/>
                <div>Slim  Fit</div>
            </div>
            <div id="fit_BodyFitModern"  onclick="collorMeshChange(this.id);" class="content_inner Fit" title="Fit|Body_Fit.svg">
                <img  src="images/Fit/Body_Fit.svg"/>
                <div>Body fit</div>
            </div>
            <div id="fit_NormalFitModern"  onclick="collorMeshChange(this.id);" class="content_inner Fit" title="Fit|Regular_Fit.svg">
                <img  src="images/Fit/Regular_Fit.svg"/>
                <div>Regular Fit</div>
            </div>
            <div  id="fit_LooseFitModern"  onclick="collorMeshChange(this.id);" class="content_inner Fit" title="Fit|Loose_Fit.svg">
                <img   src="images/Fit/Loose_Fit.svg"/>
                <div>Loose  Fit</div>
            </div>
        </div>
    </div>
    <!--End Of suit Fit-->
    <!--Start Of Collar -->
    <div id="panel_collar" class="panel mol_panel" style="background-color:transparent;">
        <div class="heading" style="background-color:transparent;">
            <div class="title">Collar</div>
            <img id="close1" class="close pull-left" src="images/close.png" onclick="camerAnim(this.id);closePanel()"/>
        </div>
        <div class="content" style="overflow-y:scroll;">
            <div id="collar_BusinessClassic" onclick="collorMeshChange(this.id);" class="content_inner Collar active" title="Collar|01_Business Classic.svg">
                <img  src="images/Collar/01_Business Classic.svg"/>
                <div>Business Classic</div>
            </div>
            <div id="collar_BusinessSuperior" onclick="collorMeshChange(this.id);" class="content_inner Collar" title="Collar|02_Business Superior.svg">
                <img  src="images/Collar/02_Business Superior.svg"/>
                <div>Business Superior</div>
            </div>
            <div id="collar_ButtonDownClassic" onclick="collorMeshChange(this.id);" class="content_inner Collar" title="Collar|03_Button-down Classic.svg">
                <img  src="images/Collar/03_Button-down Classic.svg"/>
                <div>Button-down Classic</div>
            </div>
            <div id="collar_ButtonDownModern" onclick="collorMeshChange(this.id);" class="content_inner Collar" title="Collar|04_Button-down Modern.svg">
                <img  src="images/Collar/04_Button-down Modern.svg"/>
                <div>Button-down Modern</div>
            </div>
            <div id="collar_Club" onclick="collorMeshChange(this.id);" class="content_inner Collar" title="Collar|05_Club.svg">
                <img  src="images/Collar/05_Club.svg"/>
                <div>Club</div>
            </div>
            <div id="collar_ModernClub" onclick="collorMeshChange(this.id);" class="content_inner Collar" title="Collar|06_Club Modern.svg">
                <img  src="images/Collar/06_Club Modern.svg"/>
                <div>Club Modern</div>
            </div>
            <div id="collar_CutAwayClassic" onclick="collorMeshChange(this.id);" class="content_inner Collar" title="Collar|07_Cut Away Classic.svg">
                <img  src="images/Collar/07_Cut Away Classic.svg"/>
                <div>Cut Away Classic</div>
            </div>
            <div id="collar_CutAwayCasual" onclick="collorMeshChange(this.id);" class="content_inner Collar" title="Collar|08_Cut Away Casual.svg">
                <img  src="images/Collar/08_Cut Away Casual.svg"/>
                <div>Cut Away Casual</div>
            </div>
            <div id="collar_CutAwayExtreme" onclick="collorMeshChange(this.id);" class="content_inner Collar" title="Collar|09_Cut Away Extreme.svg">
                <img  src="images/Collar/09_Cut Away Extreme.svg"/>
                <div>Cut Away Extreme</div>
            </div>
            <div id="collar_CutAwayModern" onclick="collorMeshChange(this.id);" class="content_inner Collar" title="Collar|10_Cut Away Modern.svg">
                <img  src="images/Collar/10_Cut Away Modern.svg"/>
                <div>Cut Away Modern</div>
            </div>
            <div id="collar_CutAwaySuperior" onclick="collorMeshChange(this.id);" class="content_inner Collar" title="Collar|11_Cut Away Superior.svg">
                <img  src="images/Collar/11_Cut Away Superior.svg"/>
                <div>Cut Away Superior</div>
            </div>
            <div id="collar_CutAwayTwoBtn" onclick="collorMeshChange(this.id);" class="content_inner Collar" title="Collar|12_Cut Away Two Button.svg">
                <img  src="images/Collar/12_Cut Away Two Button.svg"/>
                <div>Cut Away Two Button</div>
            </div>
            <div id="collar_TurnDown_Sperior" onclick="collorMeshChange(this.id);" class="content_inner Collar" title="Collar|13_Turndown Superior.svg">
                <img  src="images/Collar/13_Turndown Superior.svg"/>
                <div>Turndown Superior</div>
            </div>
            <div id="collar_Tab" onclick="collorMeshChange(this.id);" class="content_inner Collar" title="Collar|14_Tab.svg">
                <img  src="images/Collar/14_Tab.svg"/>
                <div>Tab</div>
            </div>
            <div id="collar_Wing" onclick="collorMeshChange(this.id);" class="content_inner Collar" title="Collar|15_Wing Collar.svg">
                <img  src="images/Collar/15_Wing Collar.svg"/>
                <div>Wing Collar</div>
            </div>
            <div id="collar_Mao" onclick="collorMeshChange(this.id);" class="content_inner Collar" title="Collar|16_Mao.svg">
                <img  src="images/Collar/16_Mao.svg"/>
                <div>Mao</div>
            </div>
            <div id="collar_Pin" onclick="collorMeshChange(this.id);" class="content_inner Collar" title="Collar|17_Pin.svg">
                <img src="images/Collar/17_Pin.svg" />
                <div>Pin</div>
            </div>
        </div>
    </div>
    <!--End Of Collar-->
    <!--Start Of Sleeve-->
    <div id="panel_sleeve" class="panel mol_panel" style="background-color:transparent;">
        <div class="heading" style="background-color:transparent;">
            <div class="title">Sleeve</div>
            <img id="close2" class="close pull-left" src="images/close.png" onclick="camerAnim(this.id);closePanel()"/>
        </div>
        <div class="content">
            <div id="sleeve_long" onclick="longSleeveChange(this.id);" class="content_inner Sleeve active" title="Sleeve|01. Long Sleeve.svg">
                <img  src="images/Sleeve/01. Long Sleeve.svg"/>
                <div>Long Sleeve</div>
            </div>
            <div id="sleeve_Rollup" onclick="longSleeveChange(this.id);" class="content_inner Sleeve" title="Sleeve|02. Roll-up Sleeve.svg">
                <img  src="images/Sleeve/02. Roll-up Sleeve.svg"/>
                <div>Roll-Up Sleeve</div>
            </div>
            <div id="sleeve_Short" onclick="longSleeveChange(this.id);" class="content_inner Sleeve" title="Sleeve|03. Short Sleeve.svg">
                <img  src="images/Sleeve/03. Short Sleeve.svg" />
                <div>Short Sleeve</div>
            </div>
        </div>
    </div>
    <!--End of Sleeve-->
    <!--Start Of Patch -->
    <div id="panel_elbow" class="panel mol_panel" style="background-color:transparent;">
        <div class="heading" style="background-color:transparent;">
            <div class="title">Elbow Fit</div>
            <img id="close17" class="close pull-left" src="images/close.png" onclick="camerAnim(this.id);closePanel()"/>
        </div>
        <div class="content">
            <div id="elbow" onclick="elbowChange(this.id);" class="content_inner elbow active" title="elbow|No Elbow Patch.svg">
                <img  src="images/elbow/No Elbow Patch.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">No Elbow patch</div>
            </div>
            <div id="elbow_Patch" onclick="elbowChange(this.id);" class="content_inner elbow" title="elbow|Elbow Patch.svg">
                <img  src="images/elbow/Elbow Patch.svg"/>
                <div style="font-family: 'Times New Roman', Times, serif;">Elbow patch</div>
            </div>
        </div>
    </div>
    <!--End of patch-->
    <!--Start Of Cuff -->
    <div id="panel_cuff" class="panel mol_panel" style="background-color:transparent;">
        <div class="heading" style="background-color:transparent;">
            <div class="title">Cuff</div>
            <img id="close3" class="close pull-left" src="images/close.png" onclick="camerAnim(this.id);closePanel()"/>
        </div>
        <div class="content" style="overflow-y:scroll;">
            <div id="cuff_SingleButtonRounded" onclick="cuffMeshChange(this.id);" class="content_inner Cuff active" title="Cuff|01_Single Button Rounded.svg">
                <img  src="images/Cuff/01_Single Button Rounded.svg"/>
                <div>Single Button Rounded</div>
            </div>
            <div id="cuff_SingleButtonBeveled" onclick="cuffMeshChange(this.id);" class="content_inner Cuff" title="Cuff|02_Single Button Beveled.svg">
                <img  src="images/Cuff/02_Single Button Beveled.svg"/>
                <div>Single Button Beveled</div>
            </div>
            <div id="cuff_SingleButtonStraight" onclick="cuffMeshChange(this.id);" class="content_inner Cuff" title="Cuff|03_Single Button Straight.svg">
                <img  src="images/Cuff/03_Single Button Straight.svg"/>
                <div>Single Button Straight</div>
            </div>
            <div id="cuff_ConvertibleRounded" onclick="cuffMeshChange(this.id);" class="content_inner Cuff" title="Cuff|04_Convertible Rounded.svg">
                <img  src="images/Cuff/04_Convertible Rounded.svg"/>
                <div>Convertible Rounded</div>
            </div>
            <div id="cuff_DoubleButtonRounded" onclick="cuffMeshChange(this.id);" class="content_inner Cuff" title="Cuff|05_Double Button Rounded.svg">
                <img  src="images/Cuff/05_Double Button Rounded.svg"/>
                <div>Double Button Rounded</div>
            </div>
            <div id="cuff_DoubleButtonBeveled" onclick="cuffMeshChange(this.id);" class="content_inner Cuff" title="Cuff|06_Double Button Beveled.svg">
                <img  src="images/Cuff/06_Double Button Beveled.svg"/>
                <div>Double Button Beveled</div>
            </div>
            <div id="cuff_French" onclick="linkFrenchCuff(this.id);" class="content_inner Cuff" title="Cuff|07_French Cuff.svg">
                <img  src="images/Cuff/07_French Cuff.svg"/>
                <div>French Cuff</div>
            </div>
            <div id="cuff_Link" onclick="linkFrenchCuff(this.id);" class="content_inner Cuff" title="Cuff|08_Link Cuff.svg">
                <img  src="images/Cuff/08_Link Cuff.svg"/>
                <div>Link Cuff</div>
            </div>
            <div id="cuff_Narrow" onclick="cuffMeshChange(this.id);" class="content_inner Cuff" title="Cuff|09_Narrow Cuff.svg">
                <img  src="images/Cuff/09_Narrow Cuff.svg"/>
                <div>Narrow Cuff</div>
            </div>
            <div id="cuff_SingleButtonCasual" onclick="cuffMeshChange(this.id);" class="content_inner Cuff" title="Cuff|10_Casual Single Button.svg" >
                <img src="images/Cuff/10_Casual Single Button.svg" />
                <div>Casual single button</div>
            </div>
        </div>
    </div>
    <!--End Of Cuff-->
    <!--Start Of Placket -->
    <div id="panel_placket" class="panel mol_panel" style="background-color:transparent;">
        <div class="heading" style="background-color:transparent;">
            <div class="title">Placket</div>
            <img id="close4" class="close pull-left" src="images/close.png" onclick="camerAnim(this.id);closePanel()"/>
        </div>
        <div class="content" >
            <div id="placket_WithPlacket" onclick="collorMeshChange(this.id);" class="content_inner Placket active" title="Placket|01_With Placket.svg">
                <img  src="images/Placket/01_With Placket.svg"/>
                <div>With Placket</div>
            </div>
            <div id="placket_WithoutPlacket" onclick="collorMeshChange(this.id);" class="content_inner Placket" title="Placket|02_Without Placket.svg">
                <img  src="images/Placket/02_Without Placket.svg" />
                <div>Without Placket</div>
            </div>
            <div id="placket_hiddenButton" onclick="collorMeshChange(this.id);" class="content_inner Placket" title="Placket|03_Hidden Buttons.svg">
                <img  src="images/Placket/03_Hidden Buttons.svg" />
                <div>Hidden Buttons</div>
            </div>
            <div id="placket_Narrow" onclick="collorMeshChange(this.id);" class="content_inner Placket" title="Placket|04_Narrow Placket.svg">
                <img  src="images/Placket/04_Narrow Placket.svg" />
                <div>Narrow Placket</div>
            </div>
            <div id="placket_S-Fit_Tux"  onclick="tuxedoChange(this.id);" class="content_inner tux_slim Placket" title="Placket|05_Tuxedo Placket.svg">
                <img  src="images/Placket/05_Tuxedo Placket.svg" />
                <div>Tuxedo Placket-Slim</div>
            </div>
            <div id="placket_B-Fit_Tux"  onclick="tuxedoChange(this.id);" class="content_inner tux_body Placket" title="Placket|05_Tuxedo Placket.svg">
                <img  src="images/Placket/05_Tuxedo Placket.svg" />
                <div>Tuxedo Placket-Body</div>
            </div>
            <div id="placket_R-Fit_Tux"  onclick="tuxedoChange(this.id);" class="content_inner tux_regular Placket" title="Placket|05_Tuxedo Placket.svg">
                <img  src="images/Placket/05_Tuxedo Placket.svg" />
                <div>Tuxedo Placket-Regular</div>
            </div>
            <div id="placket_L-Fit_Tux"  onclick="tuxedoChange(this.id);" class="content_inner tux_loose Placket" title="Placket|05_Tuxedo Placket.svg">
                <img  src="images/Placket/05_Tuxedo Placket.svg" />
                <div>Tuxedo Placket-Loose</div>
            </div>
            <div id="placket_B-Fit_Tie" onclick="tuxedoChange(this.id);" class="content_inner Placket" title="Placket|07_White Tie Placket.svg">
                <img  src="images/Placket/07_White Tie Placket.svg" />
                <div>White Tie Placket</div>
            </div>
        </div>
    </div>
    <!--End Of Placket-->
    <!--Start Of Bottom Cut -->
    <div id="panel_bottomcut" class="panel mol_panel" style="background-color:transparent;">
        <div class="heading bc_bodyfit" style="background-color:transparent;">
            <div class="title">Body Fit</div>
            <img id="close5" class="close pull-left" src="images/close.png" onclick="camerAnim(this.id);closePanel()"/>
        </div>
        <!--Body Fit-->
        <div class="content bc_bodyfit">
            <div id="fit_BodyFitClassic" onclick="collorMeshChange(this.id);" class="content_inner Bottom_Cut active" title="Bottom_Cut|01_Classic.svg">
                <img  src="images/Bottom_Cut/01_Classic.svg" />
                <div>Classic</div>
            </div>
            <div id="fit_BodyFitModern" onclick="collorMeshChange(this.id);" class="content_inner Bottom_Cut" title="Bottom_Cut|02_Modern.svg">
                <img  src="images/Bottom_Cut/02_Modern.svg" />
                <div>Modern</div>
            </div>
            <div id="fit_BodyFitStraight" onclick="collorMeshChange(this.id);" class="content_inner Bottom_Cut" title="Bottom_Cut|03_Straight.svg">
                <img  src="images/Bottom_Cut/03_Straight.svg" />
                <div>Straight</div>
            </div>
        </div>
        <!--Loose Fit-->
        <div class="heading bc_loosefit" style="background-color:transparent;">
            <div class="title">Loose Fit</div>
            <img class="close pull-left" src="images/close.png" onclick="closePanel()"/>
        </div>
        <div class="content bc_loosefit">
            <div id="fit_LooseFitClassic" onclick="collorMeshChange(this.id);" class="content_inner Bottom_Cut active" title="Bottom_Cut|01_Classic.svg">
                <img  src="images/Bottom_Cut/01_Classic.svg" />
                <div>Classic</div>
            </div>
            <div id="fit_LooseFitModern" onclick="collorMeshChange(this.id);" class="content_inner Bottom_Cut" title="Bottom_Cut|02_Modern.svg">
                <img  src="images/Bottom_Cut/02_Modern.svg" />
                <div>Modern</div>
            </div>
            <div id="fit_LooseFitStraight" onclick="collorMeshChange(this.id);" class="content_inner Bottom_Cut" title="Bottom_Cut|03_Straight.svg">
                <img  src="images/Bottom_Cut/03_Straight.svg" />
                <div>Straight</div>
            </div>
        </div>
        <!--Normal Fit-->
        <div class="heading bc_normalfit" style="background-color:transparent;">
            <div class="title">Regular Fit</div>
            <img class="close pull-left" src="images/close.png" onclick="closePanel()"/>
        </div>
        <div class="content bc_normalfit">
            <div id="fit_NormalFitClassic" onclick="collorMeshChange(this.id);" class="content_inner Bottom_Cut active" title="Bottom_Cut|01_Classic.svg">
                <img  src="images/Bottom_Cut/01_Classic.svg" />
                <div>Classic</div>
            </div>
            <div id="fit_NormalFitModern" onclick="collorMeshChange(this.id);" class="content_inner Bottom_Cut" title="Bottom_Cut|02_Modern.svg">
                <img  src="images/Bottom_Cut/02_Modern.svg" />
                <div>Modern</div>
            </div>
            <div id="fit_NormalFitStraight" onclick="collorMeshChange(this.id);" class="content_inner Bottom_Cut" title="Bottom_Cut|03_Straight.svg">
                <img  src="images/Bottom_Cut/03_Straight.svg" />
                <div>Straight</div>
            </div>
        </div>
        <!--Slim Fit-->
        <div class="heading bc_slimfit" style="background-color:transparent;">
            <div class="title">Slim Fit</div>
            <img class="close pull-left" src="images/close.png" onclick="closePanel()"/>
        </div>
        <div class="content bc_slimfit">
            <div id="fit_SlimFitClassic" onclick="collorMeshChange(this.id);"  class="content_inner Bottom_Cut active" title="Bottom_Cut|01_Classic.svg">
                <img  src="images/Bottom_Cut/01_Classic.svg" />
                <div>Classic</div>
            </div>
            <div id="fit_SlimFitModern" onclick="collorMeshChange(this.id);"class="content_inner Bottom_Cut" title="Bottom_Cut|02_Modern.svg">
                <img  src="images/Bottom_Cut/02_Modern.svg" />
                <div>Modern</div>
            </div>
            <div id="fit_SlimFitStraight" onclick="collorMeshChange(this.id);" class="content_inner Bottom_Cut" title="Bottom_Cut|03_Straight.svg">
                <img  src="images/Bottom_Cut/03_Straight.svg" />
                <div>Straight</div>
            </div>
        </div>
    </div>
    <!--End of Bottom Cut body fit-->
    <!-- Start of pocket placement -->
    <div id="panel_pocket_placement" class="panel mol_panel" style="background-color:transparent;">
        <div class="heading" style="background-color:transparent;">
            <div class="title">Pocket</div>
            <img id="close6" class="close pull-left" src="images/close.png" onclick="camerAnim(this.id);closePanel()"/>
        </div>
        <div class="content">
            <div id="pocket_no" onclick="collorMeshChange(this.id);" class="content_inner Pocket active" title="Pocket|01. No Pocket.svg">
                <img src="images/Pocket/01. No Pocket.svg"/>
                <div>No Pocket</div>
            </div>
            <div id="pocket_Lt-Straight_Poc" onclick="collorMeshChange(this.id);" class="content_inner Pocket" title="Pocket|02. Left.svg">
                <img  src="images/Pocket/02. Left.svg"/>
                <div>Left</div>
            </div>
            <div id="pocket_Straight_Poc" onclick="collorMeshChange(this.id);" class="content_inner Pocket" title="Pocket|03. Both.svg">
                <img  src="images/Pocket/03. Both.svg"/>
                <div>Both</div>
            </div>
        </div>
    </div>
    <!--End of pocket placement -->
    <!-- Start of pocket Style -->
    <div id="panel_pocket_style" class="panel mol_panel" style="background-color:transparent;">
        <div class="heading left_style" style="background-color:transparent;">
            <div class="title">left Pocket Style</div>
            <img id="close7" class="close pull-left" src="images/close.png" onclick="camerAnim(this.id);closePanel()"/>
        </div>
        <div class="content left_style">
            <div id="pocket_Lt-Straight_Poc" onclick="collorMeshChange(this.id);" class="content_inner Pocket_Style active" title="Pocket_Style|01. Straight Pocket.svg">
                <img  src="images/Pocket_Style/01. Straight Pocket.svg"/>
                <div>Straight Pocket</div>
            </div>
            <div id="pocket_Lt-Vshaped_Poc" onclick="collorMeshChange(this.id);" class="content_inner Pocket_Style" title="Pocket_Style|02. V-Shaped.svg">
                <img  src="images/Pocket_Style/02. V-Shaped.svg"/>
                <div>V-Shaped</div>
            </div>
            <div id="pocket_Lt-Ushaped_Poc" onclick="collorMeshChange(this.id);" class="content_inner Pocket_Style" title="Pocket_Style|03. U-Shaped.svg">
                <img  src="images/Pocket_Style/03. U-Shaped.svg"/>
                <div>U-Shaped</div>
            </div>
        </div>
        <!--both pocket-->
        <div class="heading both_style" style="background-color:transparent;">
            <div class="title">both Pocket Style</div>
            <img id="close7" class="close pull-left" src="images/close.png" onclick="closePanel()"/>
        </div>
        <div class="content both_style">
            <div id="pocket_Straight_Poc" onclick="collorMeshChange(this.id);" class="content_inner Pocket_Style active" title="Pocket_Style|01. Straight Pocket.svg">
                <img  src="images/Pocket_Style/01. Straight Pocket.svg"/>
                <div>Straight Pocket</div>
            </div>
            <div id="pocket_Vshaped_Poc" onclick="collorMeshChange(this.id);" class="content_inner Pocket_Style" title="Pocket_Style|02. V-Shaped.svg">
                <img  src="images/Pocket_Style/02. V-Shaped.svg"/>
                <div>V-Shaped</div>
            </div>
            <div id="pocket_Ushaped_Poc" onclick="collorMeshChange(this.id);" class="content_inner Pocket_Style" title="Pocket_Style|03. U-Shaped.svg">
                <img  src="images/Pocket_Style/03. U-Shaped.svg"/>
                <div>U-Shaped</div>
            </div>
        </div>
    </div>
    <!--End of pocket style -->
    <!-- Start of pocket flap -->
    <div id="panel_flap" class="panel mol_panel" style="background-color:transparent;">
        <div class="heading left_flap" style="background-color:transparent;">
            <div class="title">Left Pocket Flap</div>
            <img id="close8" class="close pull-left" src="images/close.png" onclick="camerAnim(this.id);closePanel()"/>
        </div>
        <div class="content left_flap">
            <div id="Flap" onclick="collorMeshChange(this.id);" class="content_inner Pocket_Flap active" title="Pocket_Flap|01. No.svg">
                <img  src="images/Pocket_Flap/01. No.svg"/>
                <div>No</div>
            </div>
            <div id="Flap_Lt-Straight" onclick="collorMeshChange(this.id);" class="content_inner Pocket_Flap" title="Pocket_Flap|02. Yes.svg">
                <img  src="images/Pocket_Flap/02. Yes.svg"/>
                <div>Yes</div>
            </div>
        </div>
        <!--Both pocket flap-->
        <div class="heading both_flap" style="background-color:transparent;">
            <div class="title">Both Pocket Flap</div>
            <img id="close8" class="close" src="images/close.png" onclick="closePanel()"/>
        </div>
        <div class="content both_flap">
            <div id="Flap" onclick="collorMeshChange(this.id);" class="content_inner Pocket_Flap active" title="Pocket_Flap|01. No.svg">
                <img  src="images/Pocket_Flap/01. No.svg"/>
                <div>No</div>
            </div>
            <div id="Flap_Straight" onclick="collorMeshChange(this.id);" class="content_inner Pocket_Flap" title="Pocket_Flap|02. Yes.svg">
                <img  src="images/Pocket_Flap/02. Yes.svg"/>
                <div>Yes</div>
            </div>
        </div>
    </div>
    <!--End of pocket flap -->
    <!-- Start of Back Detail Slim Fit -->
    <div id="panel_back" class="panel mol_panel" style="background-color:transparent;">
        <div class="heading bd_slimfit" style="background-color:transparent;">
            <div class="title">Slim Fit</div>
            <img id="close9" class="close pull-left" src="images/close.png" onclick="camerAnim(this.id);closePanel()"/>
        </div>
        <div class="content bd_slimfit">
            <div id="back_01" onclick="meshChange(this.id);" class="content_inner Back_Details active" title="Back_Details|01_No_Back_Details.svg">
                <img  src="images/Back_Details/01_No_Back_Details.svg"/>
                <div>No Back Details</div>
            </div>
            <div id="back_04" onclick="meshChange(this.id);" class="content_inner Back_Details" title="Back_Details|04_Back_Darts.svg">
                <img  src="images/Back_Details/04_Back_Darts.svg"/>
                <div>Back Darts</div>
            </div>
        </div>
        <!--Back Detail Body fit-->
        <div class="heading bd_bodyfit" style="background-color:transparent;">
            <div class="title">Body Fit</div>
            <img class="close pull-left" src="images/close.png" onclick="closePanel()"/>
        </div>
        <div class="content bd_bodyfit">
            <div id="back_01" onclick="meshChange(this.id);" class="content_inner Back_Details active" title="Back_Details|01_No_Back_Details.svg">
                <img  src="images/Back_Details/01_No_Back_Details.svg"/>
                <div>No Back Details</div>
            </div>
            <div id="back_04" onclick="meshChange(this.id);" class="content_inner Back_Details" title="Back_Details|04_Back_Darts.svg">
                <img  src="images/Back_Details/04_Back_Darts.svg"/>
                <div>Back Darts</div>
            </div>
        </div>
        <!--Back detail Regular Fit-->
        <div class="heading bd_regularfit" style="background-color:transparent;">
            <div class="title">Regular Fit</div>
            <img class="close pull-left" src="images/close.png" onclick="closePanel()"/>
        </div>
        <div class="content bd_regularfit">
            <div id="back_01" onclick="meshChange(this.id);" class="content_inner Back_Details active" title="Back_Details|01_No_Back_Details.svg">
                <img  src="images/Back_Details/01_No_Back_Details.svg"/>
                <div>No Back Details</div>
            </div>
            <div id="back_02" onclick="meshChange(this.id);" class="content_inner Back_Details" title="Back_Details|02_Center_Folds.svg">
                <img  src="images/Back_Details/02_Center_Folds.svg"/>
                <div>Center Folds</div>
            </div>
            <div id="back_03"  onclick="meshChange(this.id);" class="content_inner Back_Details" title="Back_Details|03_Side_Folds.svg">
                <img  src="images/Back_Details/03_Side_Folds.svg"/>
                <div>Side Folds</div>
            </div>
        </div>
        <!--Back detail Loose fit-->
        <div class="heading bd_loosefit" style="background-color:transparent;">
            <div class="title">Loose Fit</div>
            <img class="close pull-left" src="images/close.png" onclick="closePanel()"/>
        </div>
        <div class="content bd_loosefit">
            <div id="back_01" onclick="meshChange(this.id);" class="content_inner Back_Details active" title="Back_Details|01_No_Back_Details.svg">
                <img  src="images/Back_Details/01_No_Back_Details.svg"/>
                <div>No Back Details</div>
            </div>
            <div id="back_02" onclick="meshChange(this.id);" class="content_inner Back_Details" title="Back_Details|02_Center_Folds.svg">
                <img  src="images/Back_Details/02_Center_Folds.svg"/>
                <div>Center Folds</div>
            </div>
            <div id="back_03"  onclick="meshChange(this.id);" class="content_inner Back_Details" title="Back_Details|03_Side_Folds.svg">
                <img  src="images/Back_Details/03_Side_Folds.svg"/>
                <div>Side Folds</div>
            </div>
        </div>
    </div>
    <!--End of Back Detail -->
    <!--Start Of Fabric -->
    <div id="panel_fabric" class="panel mol_panel" style="background-color:transparent;">
        <div class="heading" style="background-color:transparent;">
            <div class="title">Fabric</div>
            <img id="close10" class="close pull-left" src="images/close.png" onclick="camerAnim(this.id);closePanel()"/>
        </div>
        <div class="content" style="overflow-y:scroll;">
            <div id="fa_0" onclick="textureChange(this.id);" class="content_inner clickMe Beige print fabric active" title="fabric|01_Blue_White_Dobby.jpg">
                <img  src="images/fabric/01_Blue_White_Dobby.jpg"  />
                <div>Blue White Dobby</div>
            </div>
            <div id="fa_1" onclick="textureChange(this.id);" class="content_inner clickMe1 White print fabric" title="fabric|02_Blue_White_Broken_Twill.jpg">
                <img class="home" src="images/fabric/02_Blue_White_Broken_Twill.jpg"  />
                <div>Blue White Broken Twill</div>
            </div>
            <div id="fa_2" onclick="textureChange(this.id);" class="content_inner clickMe2 Beige check fabric " title="fabric|03_White_Matt.jpg">
                <img src="images/fabric/03_White_Matt.jpg"  />
                <div>White Matt</div>
            </div>
            <div id="fa_3" onclick="textureChange(this.id);" class="content_inner clickMe3 Black solid fabric " title="fabric|04_Blue_White_Stripe.jpg">
                <img src="images/fabric/04_Blue_White_Stripe.jpg"  />
                <div>Blue White Stripe</div>
            </div>
            <div id="fa_4" onclick="textureChange(this.id);" class="content_inner clickMe4 Black solid fabric " title="fabric|05_White_Dobby.jpg">
                <img src="images/fabric/05_White_Dobby.jpg"  />
                <div>White Dobby</div>
            </div>
            <div id="fa_5" onclick="textureChange(this.id);" class="content_inner clickMe5 Black strip fabric " title="fabric|06_Blue_Dobby.jpg">
                <img src="images/fabric/06_Blue_Dobby.jpg"  />
                <div>Blue Dobby</div>
            </div>
            <div id="fa_6" onclick="textureChange(this.id);" class="content_inner clickMe6 Blue check fabric " title="fabric|07_White_Solid.jpg">
                <img src="images/fabric/07_White_Solid.jpg"  />
                <div>White Solid</div>
            </div>
            <div id="fa_7" onclick="textureChange(this.id);" class="content_inner clickMe7 Blue solid fabric " title="fabric|08_Blue_Chamery.jpg">
                <img src="images/fabric/08_Blue_Chamery.jpg"  />
                <div>Blue Chamery</div>
            </div>
            <div id="fa_8" onclick="textureChange(this.id);" class="content_inner clickMe8 Blue solid fabric " title="fabric|09_Blue_Dobby.jpg">
                <img src="images/fabric/09_Blue_Dobby.jpg"  />
                <div>Blue Dobby</div>
            </div>
            <div id="fa_9" onclick="textureChange(this.id);" class="content_inner clickMe9 Blue check fabric " title="fabric|09_White_Dobby.jpg">
                <img src="images/fabric/09_White_Dobby.jpg"  />
                <div>White Dobby</div>
            </div>
            <div id="fa_10" onclick="textureChange(this.id);" class="content_inner clickMe10 Blue solid fabric " title="fabric|10_White_Dobby.jpg">
                <img src="images/fabric/10_White_Dobby.jpg"  />
                <div>White Dobby</div>
            </div>
            <div id="fa_11" onclick="textureChange(this.id);" class="content_inner clickMe11 Blue solid fabric " title="fabric|11_White_Blue_Solid.jpg">
                <img src="images/fabric/11_White_Blue_Solid.jpg"  />
                <div>White Blue Solid</div>
            </div>
            <div id="fa_12" onclick="textureChange(this.id);" class="content_inner clickMe15 Blue White check fabric " title="fabric|12_Blue_Fill_A_Fill.jpg">
                <img src="images/fabric/12_Blue_Fill_A_Fill.jpg"  />
                <div>Blue Fill A Fill</div>
            </div>
            <div id="fa_13" onclick="textureChange(this.id);" class="content_inner clickMe13 Blue White strip fabric " title="fabric|13_Blue_Stripe.jpg">
                <img src="images/fabric/13_Blue_Stripe.jpg"  />
                <div>Blue Stripe</div>
            </div>
            <div id="fa_14" onclick="textureChange(this.id);" class="content_inner clickMe14 Blue Yellow check fabric " title="fabric|14_Blue_Stripe.jpg">
                <img src="images/fabric/14_Blue_Stripe.jpg"  />
                <div>Blue Stripe</div>
            </div>
        </div>
    </div>
    <!--Start Of Contrast Fabric-->
    <div id="panel_contrast_fabric" class="panel mol_panel" style="background-color:transparent;">
        <div class="heading">
            <div class="title">Contrast Fabric</div>
            <img id="close11" class="close pull-left" src="images/close.png" onclick="camerAnim(this.id);closePanel()"/>
        </div>
        <div class="content" style="overflow-y:scroll;">
            <div id="fa_0" onclick="colTexChange(this.id);" class="content_inner Blue White print fabric1 active" title="fabric1|01_Blue_White_Dobby.jpg">
                <img  src="images/fabric1/01_Blue_White_Dobby.jpg"  />
                <div>Blue_White_Dobby</div>
            </div>
            <div id="fa_1" onclick="colTexChange(this.id);" class="content_inner White Blue print fabric1" title="fabric1|02_Blue_White_Broken_Twill.jpg">
                <img class="home" src="images/fabric1/02_Blue_White_Broken_Twill.jpg"  />
                <div>Blue_White_Broken_Twill</div>
            </div>
            <div id="fa_2" onclick="colTexChange(this.id);" class="content_inner White check fabric1 " title="fabric1|03_White_Matt.jpg">
                <img src="images/fabric1/03_White_Matt.jpg"  />
                <div>White_Matt</div>
            </div>
            <div id="fa_3" onclick="colTexChange(this.id);" class="content_inner Blue White strip fabric1 " title="fabric1|04_Blue_White_Stripe.jpg">
                <img src="images/fabric1/04_Blue_White_Stripe.jpg"  />
                <div>Blue_White_Stripe</div>
            </div>
            <div id="fa_4" onclick="colTexChange(this.id);" class="content_inner White solid fabric1 " title="fabric1|05_White_Dobby.jpg">
                <img src="images/fabric1/05_White_Dobby.jpg"  />
                <div>White_Dobby</div>
            </div>
            <div id="fa_5" onclick="colTexChange(this.id);" class="content_inner Blue strip fabric1 " title="fabric1|06_Blue_Dobby.jpg">
                <img src="images/fabric1/06_Blue_Dobby.jpg"  />
                <div>Blue_Dobby</div>
            </div>
            <div id="fa_6" onclick="colTexChange(this.id);" class="content_inner Bhite check fabric1 " title="fabric1|07_White_Solid.jpg">
                <img src="images/fabric1/07_White_Solid.jpg"  />
                <div>White_Solid</div>
            </div>
            <div id="fa_7" onclick="colTexChange(this.id);" class="content_inner Blue solid fabric1 " title="fabric1|08_Blue_Chamery.jpg">
                <img src="images/fabric1/08_Blue_Chamery.jpg"  />
                <div>Blue_Chamery</div>
            </div>
            <div id="fa_8" onclick="colTexChange(this.id);" class="content_inner Blue solid fabric1 " title="fabric1|09_Blue_Dobby.jpg">
                <img src="images/fabric1/09_Blue_Dobby.jpg"  />
                <div>Blue_Dobby</div>
            </div>
            <div id="fa_9" onclick="colTexChange(this.id);" class="content_inner Bhite check fabric1 " title="fabric1|09_White_Dobby.jpg">
                <img src="images/fabric1/09_White_Dobby.jpg"  />
                <div>White_Dobby</div>
            </div>
            <div id="fa_10" onclick="colTexChange(this.id);" class="content_inner white solid fabric1 " title="fabric1|10_White_Dobby.jpg">
                <img src="images/fabric1/10_White_Dobby.jpg"  />
                <div>White_Dobby</div>
            </div>
            <div id="fa_11" onclick="colTexChange(this.id);" class="content_inner Blue White solid fabric1 " title="fabric1|11_White_Blue_Solid.jpg">
                <img src="images/fabric1/11_White_Blue_Solid.jpg"  />
                <div>White_Blue_Solid</div>
            </div>
            <div id="fa_12" onclick="colTexChange(this.id);" class="content_inner Blue check fabric1 " title="fabric1|12_Blue_Fill_A_Fill.jpg">
                <img src="images/fabric1/12_Blue_Fill_A_Fill.jpg"  />
                <div>Blue_Fill_A_Fill</div>
            </div>
            <div id="fa_13" onclick="colTexChange(this.id);" class="content_inner Blue  strip fabric1 " title="fabric1|13_Blue_Stripe.jpg">
                <img src="images/fabric1/13_Blue_Stripe.jpg"  />
                <div>Blue_Stripe</div>
            </div>
            <div id="fa_14" onclick="colTexChange(this.id);" class="content_inner Blue strip fabric1 " title="fabric1|14_Blue_Stripe.jpg">
                <img src="images/fabric1/14_Blue_Stripe.jpg"  />
                <div>Blue_Stripe</div>
            </div>
        </div>
    </div>
    <!--End of Contrast Fabric-->
    <!--Start of Button -->
    <div id="panel_button" class="panel mol_panel" style="background-color:transparent;">
        <div class="heading" style="background-color:transparent;">
            <div class="title">Button</div>
            <img id="close12" class="close pull-left" src="images/close.png" onclick="camerAnim(this.id);closePanel()"/>
        </div>
        <div class="content" style="overflow-y:scroll;height:500px;">
            <div id="f1f4e5" onclick="buttonColor(this.id);" class="content_inner button active" title="button|Sorrento-white.png" >
                <img src="images/button/Sorrento-white.png" />
                <div>Sorrento white</div>
            </div>
            <div id="161817" onclick="buttonColor(this.id);" class="content_inner button" title="button|Sorrento-black.png" >
                <img src="images/button/Sorrento-black.png" />
                <div>Sorrento black</div>
            </div>
            <div id="f27597" onclick="buttonColor(this.id);" class="content_inner button" title="button|Langa-Cerise.png" >
                <img src="images/button/Langa-Cerise.png" />
                <div>Langa Cerise</div>
            </div>
            <div id="ece1d5" onclick="buttonColor(this.id);" class="content_inner button" title="button|Tineo-Ivory.png" >
                <img src="images/button/Tineo-Ivory.png" />
                <div>Tineo Ivory</div>
            </div>
            <div id="55161e" onclick="buttonColor(this.id);" class="content_inner button" title="button|Coria-Wine.png" >
                <img src="images/button/Coria-Wine.png" />
                <div>Coria Wine</div>
            </div>
            <div id="8a5c61" onclick="buttonColor(this.id);" class="content_inner button" title="button|Sorrento-maroon.png" >
                <img src="images/button/Sorrento-maroon.png" />
                <div>Sorrento maroon</div>
            </div>
            <div id="243a2e" onclick="buttonColor(this.id);" class="content_inner button" title="button|Sorrento-military green.png" >
                <img src="images/button/Sorrento-military green.png" />
                <div>Sorrento-military green</div>
            </div>
            <div id="72275a" onclick="buttonColor(this.id);" class="content_inner button" title="button|Langa-Light Purple.png" >
                <img src="images/button/Langa-Light Purple.png" />
                <div>Langa-Light Purple</div>
            </div>
            <div id="707370" onclick="buttonColor(this.id);" class="content_inner button" title="button|Sorrento-light gray.png" >
                <img src="images/button/Sorrento-light gray.png" />
                <div>Sorrento-light gray</div>
            </div>
            <div id="4a67af" onclick="buttonColor(this.id);" class="content_inner button" title="button|Langa-Blue.png" >
                <img src="images/button/Langa-Blue.png" />
                <div>Langa-Blue</div>
            </div>
            <div id="2b564d" onclick="buttonColor(this.id);" class="content_inner button" title="button|Langa-Green.png" >
                <img src="images/button/Langa-Green.png" />
                <div>Langa-Green</div>
            </div>
            <div id="21204c" onclick="buttonColor(this.id);" class="content_inner button" title="button|Coria-Dark Blue.png" >
                <img src="images/button/Coria-Dark Blue.png" />
                <div>Coria-Dark Blue</div>
            </div>
            <div id="1b1a1b" onclick="buttonColor(this.id);" class="content_inner button" title="button|Coria-Black.png" >
                <img src="images/button/Coria-Black.png" />
                <div>Coria-Black</div>
            </div>
            <div id="772326" onclick="buttonColor(this.id);" class="content_inner button" title="button|Coria-Wine1.png" >
                <img src="images/button/Coria-Wine1.png" />
                <div>Coria-Wine1</div>
            </div>
            <div id="bf653b" onclick="buttonColor(this.id);" class="content_inner button" title="button|Langa-Orange.png" >
                <img src="images/button/Langa-Orange.png" />
                <div>Langa-Orange</div>
            </div>
            <div id="e7dcd1" onclick="buttonColor(this.id);" class="content_inner button" title="button|Girona-Ivory.png" >
                <img src="images/button/Girona-Ivory.png" />
                <div>Girona-Ivory</div>
            </div>
            <div id="9ba0a6" onclick="buttonColor(this.id);" class="content_inner button" title="button|Girona-Light Gray.png" >
                <img src="images/button/Girona-Light Gray.png" />
                <div>Girona-Light Gray</div>
            </div>
            <div id="3e2a22" onclick="buttonColor(this.id);" class="content_inner button" title="button|Coria-Brown.png" >
                <img src="images/button/Coria-Brown.png" />
                <div>Coria-Brown</div>
            </div>
            <div id="06082d" onclick="buttonColor(this.id);" class="content_inner button" title="button|Coria-Dark Blue1.png" >
                <img src="images/button/Coria-Dark Blue1.png" />
                <div>Coria-Dark Blue1</div>
            </div>
            <div id="ae89b4" onclick="buttonColor(this.id);" class="content_inner button" title="button|Langa-Dark Purple.png" >
                <img src="images/button/Langa-Dark Purple.png" />
                <div>Langa-Dark Purple</div>
            </div>
            <div id="61b53a" onclick="buttonColor(this.id);" class="content_inner button" title="button|Langa-Lime.png" >
                <img src="images/button/Langa-Lime.png" />
                <div>Langa-Lime</div>
            </div>
            <div id="8d8c8c" onclick="buttonColor(this.id);" class="content_inner button" title="button|Silver-(Push).png" >
                <img src="images/button/Silver-(Push).png" />
                <div>Silver-(Push)</div>
            </div>
            <div id="a89860" onclick="buttonColor(this.id);" class="content_inner button" title="button|Gold-(Push).png" >
                <img src="images/button/Gold-(Push).png" />
                <div>Gold-(Push)</div>
            </div>
            <div id="61b53a" onclick="buttonColor(this.id);" class="content_inner button" title="button|Langa-Lime.png" >
                <img src="images/button/Langa-Lime.png" />
                <div>Langa-Lime</div>
            </div>
            <div id="5b5d62" onclick="buttonColor(this.id);" class="content_inner button" title="button|Black-(Push).png" >
                <img src="images/button/Black-(Push).png" />
                <div>Black-(Push)</div>
            </div>
            <div id="b1ae99" onclick="buttonColor(this.id);" class="content_inner button" title="button|Ivory-(Push).png" >
                <img src="images/button/Ivory-(Push).png" />
                <div>Ivory-(Push)</div>
            </div>
        </div>
    </div>
    <!-- End Of Button -->
    <!--Start of Thread -->
    <div id="panel_thread" class="panel mol_panel" style="background-color:transparent;">
        <div class="heading" style="background-color:transparent;">
            <div class="title">Thread</div>
            <img id="close13" class="close pull-left" src="images/close.png" onclick="camerAnim(this.id);closePanel()"/>
        </div>
        <div class="content" style="overflow-y:scroll;">
            <div id="e7e8e7" onclick="threadColor(this.id);"  class="content_inner thread active" title="thread|White.png" >
                <img class="bottom_img" src="images/thread/White.png" />
                <div>White</div>
            </div>
            <div id="c0c0bb" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Cream White.png" >
                <img class="bottom_img" src="images/thread/Cream White.png" />
                <div>Cream White</div>
            </div>
            <div id="f5d84b" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Yellow.png" >
                <img class="bottom_img" src="images/thread/Yellow.png" />
                <div>Yellow</div>
            </div>
            <div id="d8e09f" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Bright yellow.png" >
                <img class="bottom_img" src="images/thread/Bright yellow.png" />
                <div>Bright yellow</div>
            </div>
            <div id="e09544" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Orange.png" >
                <img class="bottom_img" src="images/thread/Orange.png" />
                <div>Orange</div>
            </div>
            <div id="d3a5b2" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Pink.png" >
                <img class="bottom_img" src="images/thread/Pink.png" />
                <div>Pink</div>
            </div>
            <div id="c14d70" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Hot Pink.png" >
                <img class="bottom_img" src="images/thread/Hot Pink.png" />
                <div>Hot Pink</div>
            </div>
            <div id="b64243" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Cherry Red.png" >
                <img class="bottom_img" src="images/thread/Cherry Red.png" />
                <div>Cherry Red</div>
            </div>
            <div id="872d2d" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Deep Red.png" >
                <img class="bottom_img" src="images/thread/Deep Red.png" />
                <div>Deep Red</div>
            </div>
            <div id="7f5450" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Mauve.png" >
                <img class="bottom_img" src="images/thread/Mauve.png" />
                <div>Mauve</div>
            </div>
            <div id="5f3c49" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Deep Purple.png" >
                <img class="bottom_img" src="images/thread/Deep Purple.png" />
                <div>Deep Purple</div>
            </div>
            <div id="4c2126" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Bordeaux Red.png" >
                <img class="bottom_img" src="images/thread/Bordeaux Red.png" />
                <div>Bordeaux Red</div>
            </div>
            <div id="6e4e7d" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Purple.png" >
                <img class="bottom_img" src="images/thread/Purple.png" />
                <div>Purple</div>
            </div>
            <div id="634572" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Distinct Purple.png" >
                <img class="bottom_img" src="images/thread/Distinct Purple.png" />
                <div>Distinct Purple</div>
            </div>
            <div id="ab8eaf" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Lavender.png" >
                <img class="bottom_img" src="images/thread/Lavender.png" />
                <div>Lavender</div>
            </div>
            <div id="7aa353" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Bright Green.png" >
                <img class="bottom_img" src="images/thread/Bright Green.png" />
                <div>Bright Green</div>
            </div>
            <div id="6d9045" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Lime.png" >
                <img class="bottom_img" src="images/thread/Lime.png" />
                <div>Lime</div>
            </div>
            <div id="466b4c" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Racing Green.png" >
                <img class="bottom_img" src="images/thread/Racing Green.png" />
                <div>Racing Green</div>
            </div>
            <div id="3a392c" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Olive Green.png" >
                <img class="bottom_img" src="images/thread/Olive Green.png" />
                <div>Olive Green</div>
            </div>
            <div id="508294" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Deep Turqouise.png" >
                <img class="bottom_img" src="images/thread/Deep Turqouise.png" />
                <div>Deep Turqouise</div>
            </div>
            <div id="8693b0" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Baby Blue.png" >
                <img class="bottom_img" src="images/thread/Baby Blue.png" />
                <div>Baby Blue.png</div>
            </div>
            <div id="7086a6" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Sky Blue.png" >
                <img class="bottom_img" src="images/thread/Sky Blue.png" />
                <div>Sky Blue.png</div>
            </div>
            <div id="455780" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Azure Blue.png" >
                <img class="bottom_img" src="images/thread/Azure Blue.png" />
                <div>Azure Blue.png</div>
            </div>
            <div id="2b2e3f" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Ocean Blue.png" >
                <img class="bottom_img" src="images/thread/Ocean Blue.png" />
                <div>Ocean Blue.png</div>
            </div>
            <div id="747270" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Silver Gray.png" >
                <img class="bottom_img" src="images/thread/Silver Gray.png" />
                <div>Silver Gray.png</div>
            </div>
            <div id="62615e" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Steel Gray.png" >
                <img class="bottom_img" src="images/thread/Steel Gray.png" />
                <div>Steel Gray.png</div>
            </div>
            <div id="564b3c" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Medium Brown.png" >
                <img class="bottom_img" src="images/thread/Medium Brown.png" />
                <div>Medium Brown.png</div>
            </div>
            <div id="27231e" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Coffee.png" >
                <img class="bottom_img" src="images/thread/Coffee.png" />
                <div>Coffee.png</div>
            </div>
            <div id="1d1e1d" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Black.png" >
                <img class="bottom_img" src="images/thread/Black.png" />
                <div>Black.png</div>
            </div>
        </div>
    </div>
    <!--End of thread-->
    <!-- Start Of Contrast-->
    <div id="panel_contrast" class="panel mol_panel" style="background-color:transparent;">
        <div class="heading">
            <div class="title">Contrast</div>
            <img id="dilip1" class="close pull-left" src="images/close.png" onclick="camerAnim(this.id);closePanel()"/>
        </div>
        <div class="content" style="overflow-y:scroll;height:500px;">
            <div class="content_inner Contrast" >
                <img id="dilip" class="bottom_img" src="images/Contrast/00_Contrasts.svg"/>
                <div>No Contrasts</div>
            </div>
            <div id="contrast_check">
                <div id="Patch" onclick="checkMate(this.id);" class="content_inner fab2  checkboxes Contrast">
                    <input type="checkbox" class="chkaction" name="rGroup"  value="Elbow Patch" id="r1"/>
                    <label  for="r1">
                        <img class="bottom_img" src="images/Contrast/11_Elbow_Patch.svg"/>
                        <div>Elbow Patch</div>
                    </label>
                </div>
                <div id="Stnd-Back" onclick="checkMate(this.id);" class="content_inner fab2 checkboxes Contrast"  >
                    <input type="checkbox" class="chkaction" name="rGroup"  value="Inside Collar" id="r3"/>
                    <label  for="r3">
                        <img class="bottom_img" src="images/Contrast/02_Inside Collar.svg"/>
                        <div>Inside Collar</div>
                    </label>
                </div>
                <div id="Stnd-Front" onclick="checkMate(this.id);" class="content_inner fab2 checkboxes Contrast" >
                    <input type="checkbox" class="chkaction" name="rGroup"  value="Collar Stand" id="r4"/>
                    <label  for="r4">
                        <img class="bottom_img" src="images/Contrast/03_Collar_Stand.svg"/>
                        <div> Collar Stand</div>
                    </label>
                </div>
                <div id="contrast_4" onclick="checkMate(this.id);" class="content_inner fab2 checkboxes Contrast" >
                    <input type="checkbox" class="chkaction" name="rGroup"  value="Under Collar" id="r5"/>
                    <label  for="r5">
                        <img class="bottom_img" src="images/Contrast/03_Under Collar.svg"/>
                        <div>Under Collar</div>
                    </label>
                </div>
                <div id="Cuff-Front" onclick="checkMate(this.id);" class="content_inner fab2 checkboxes Contrast"  >
                    <input type="checkbox" class="chkaction" name="rGroup"  value="Cuff Fabric" id="r6"/>
                    <label  for="r6">
                        <img class="bottom_img" src="images/Contrast/04_Cuff Fabric.svg"/>
                        <div>Cuff Fabric</div>
                    </label>
                </div>
                <div id="contrast_6" onclick="colTexChange(this.id);" class="content_inner fab2 checkboxes Contrast" >
                    <input type="checkbox" class="chkaction" name="rGroup"  value="Inside Cuff" id="r7"/>
                    <label  for="r7">
                        <img class="bottom_img" src="images/Contrast/05_Inside Cuff.svg"/>
                        <div>Inside Cuff</div>
                    </label>
                </div>
                <div id="OutBack" onclick="checkMate(this.id);" class="content_inner fab2 checkboxes Contrast" >
                    <input type="checkbox" class="chkaction" name="rGroup"  value="Inside Fastening" id="r8"/>
                    <label  for="r8">
                        <img class="bottom_img" src="images/Contrast/06_Inside Fastening.svg"/>
                        <div>Inside Fastening</div>
                    </label>
                </div>
                <div id="In" onclick="checkMate(this.id);" class="content_inner fab2 checkboxes Contrast" >
                    <input type="checkbox" class="chkaction" name="rGroup"  value="Button_Placket" id="r9"/>
                    <label  for="r9">
                        <img class="bottom_img" src="images/Contrast/07_Button_Placket.svg"/>
                        <div>Button_Placket</div>
                    </label>
                </div>
                <div id="OutFront" onclick="checkMate(this.id);" class="content_inner fab2 checkboxes Contrast">
                    <input type="checkbox" class="chkaction" name="rGroup"  value="Outside Fastening" id="r10"/>
                    <label  for="r10">
                        <img class="bottom_img" src="images/Contrast/08_Outside Fastening.svg"/>
                        <div>Outside Fastening</div>
                    </label>
                </div>
                <div id="Poc" onclick="checkMate(this.id);" class="content_inner fab2 checkboxes Contrast">
                    <input type="checkbox" class="chkaction" name="rGroup"  value="Pocket Fabric" id="r11"/>
                    <label  for="r11">
                        <img class="bottom_img" src="images/Contrast/09_Pocket Fabric.svg"/>
                        <div>Pocket Fabric</div>
                    </label>
                </div>
                <div id="contrast_12" onclick="colTexChange(this.id);" class="content_inner fab2 checkboxes Contrast">
                    <input type="checkbox" class="chkaction" name="rGroup"  value="Sleeve Placket" id="r12"/>
                    <label  for="r12">
                        <img class="bottom_img" src="images/Contrast/10_Sleeve Placket.svg"/>
                        <div>Sleeve Placket</div>
                    </label>
                </div>
            </div>
        </div>
    </div>
    <!-- End Of Contrast -->
    <!--Start Of Embroidery-->
    <div id="panel_embroidery" class="panel mol_panel" style="background-color:#fff;">
        <div class="heading" style="background-color:transparent;">
            <div class="title">Embroidary</div>
            <img id="close14" class="close pull-left" src="images/close.png" onclick="camerAnim(this.id);closePanel()" />
        </div>
        <div ng-app="myApp" ng-controller="myCtrl">
            <br>
            <div id="font_change" class="changeMe" style="font-size: 35px;padding: 20px;text-align: center;background-image:url(./images/image/01_Blue_White_Dobby.jpg)">{{name}}</div>
            <br>
            <div>
                <select style="width: 100%;">
                    <option class="tentOne" selected>None</option>
                    <option class="tentTwo" id="enable1">Placket bottom & cuff(+$0)</option>
                    <option class="tentTwo" id="enable-2">Front pocket(+$0)</option>
                    </option>
                    <option class="tentTwo" id="enable-3">Cuff(+$0)</option>
                    <option class="tentTwo" id="enable-4">pocket chest & cuff(+$0)</option>
                </select>
            </div>
            <div class="tent">
                <h2 style="margin-top: 0px;margin-bottom: 0px;">Embroidery text</h2>
                <p style="margin-top: 0;">For example you initials Maximum 12 character including punctuation</p>
                <span style="font-family: 'Times New Roman', Georgia, Serif;">Enter Text: </span>
                <input ng-model="name" maxlength="12">
                <div class="Em-content" style="overflow-y:scroll" style="background-color:transparent;">
                    <li style="list-style: none;" class="radioImageInput embroideryFont inputItem" id="input_embroideryFont">
                        <h3 class="Embroidery_font">Embroidery font</h3>
                        <ul style="list-style: none;">
                            <li class="radioImageOption choiceItem" id="embroideryFont_2" onclick="Avantgarde()" ;>
                                <img src="images/font/1.jpg" class="content_inner em-text active" alt="">
                                <p>Avantgarde</p>
                            </li>
                            <li class="radioImageOption choiceItem" id="embroideryFont_3" onclick="CourierNew()" ;>
                                <img src="images/font/2.jpg" class="content_inner em-text" alt="">
                                <p>Courier New</p>
                            </li>
                            <li class="radioImageOption choiceItem" id="embroideryFont_4" onclick="Georgia()" ;>
                                <img src="images/font/3.jpg" class="content_inner em-text" alt="">
                                <p>Georgia</p>
                            </li>
                            <li class="radioImageOption choiceItem" id="embroideryFont_5" onclick="AppleChancery()" ;>
                                <img src="images/font/4.jpg" class="content_inner em-text" alt="">
                                <p>Apple Chancery</p>
                            </li>
                            <li class="radioImageOption choiceItem" id="embroideryFont_6" onclick="BrushScript()" ;>
                                <img src="images/font/5.jpg" class="content_inner em-text" alt="">
                                <p>Brush Script</p>
                            </li>
                            <li class="radioImageOption choiceItem" id="embroideryFont_1" onclick="fantasy()" ;>
                                <img src="images/font/6.jpg" class="content_inner em-text" alt="">
                                <p>fantasy</p>
                            </li>
                        </ul>
                    </li>
                    <li class="radioImageInput embroideryThreadColor inputItem" id="input_embroideryThreadColor" style="list-style: none;">
                        <h3>Embroidery thread colour</h3>
                    <li class="radioImageInput embroideryThreadColor inputItem" id="input_embroideryThreadColor" style="list-style: none;height:250px;overflow-y:scroll;">
                        <ul style="list-style: none;">
                            <li style="list-style: none;" class="radioImageOption choiceItem" id="embroideryThreadColor_1" onclick="makewhite()" ;>
                                <img src="images/thread/White.png" alt="">
                                <p>White</p>
                            </li>
                            <li class="radioImageOption  choiceItem" id="embroideryThreadColor_2" onclick="makeSilverGray()" ;>
                                <img src="images/thread/Silver Gray.png" alt="">
                                <p>Sil-Gray</p>
                            </li>
                            <li class="radioImageOption choiceItem" id="embroideryThreadColor_3" onclick="makeSteelGray()" ;>
                                <img src="images/thread/Steel Gray.png" alt="">
                                <p>Steel Gray</p>
                            </li>
                            <li class="radioImageOption choiceItem" id="embroideryThreadColor_4" onclick="makeMediumBrown()" ;>
                                <img src="images/thread/Medium Brown.png" alt="">
                                <p>MedBrown</p>
                            </li>
                            <li class="radioImageOption choiceItem" id="embroideryThreadColor_5" onclick="makeCoffee()" ;>
                                <img src="images/thread/Coffee.png" alt="">
                                <p>Coffee</p>
                            </li>
                            <li class="radioImageOption choiceItem" id="embroideryThreadColor_6" onclick="makeBlack()" ;>
                                <img src="images/thread/Black.png" alt="">
                                <p>Black</p>
                            </li>
                            <li class="radioImageOption choiceItem" id="embroideryThreadColor_7" onclick="makeYellow()" ;>
                                <img src="images/thread/Yellow.png" alt="">
                                <p>Yellow</p>
                            </li>
                            <li class="radioImageOption choiceItem" id="embroideryThreadColor_8" onclick="makeBrightyellow()" ;>
                                <img src="images/thread/Bright yellow.png" alt="">
                                <p>Bri-yellow</p>
                            </li>
                            <li class="radioImageOption choiceItem" id="embroideryThreadColor_9" onclick="makeOrange()" ;>
                                <img src="images/thread/Orange.png" alt="">
                                <p>Orange</p>
                            </li>
                            <li class="radioImageOption choiceItem" id="embroideryThreadColor_10" onclick="makePink()" ;>
                                <img src="images/thread/Pink.png" alt="">
                                <p>Pink</p>
                            </li>
                            <li class="radioImageOption choiceItem" id="embroideryThreadColor_11" onclick="makeHotPink()" ;>
                                <img src="images/thread/Hot Pink.png" alt="">
                                <p>HotPink</p>
                            </li>
                            <li class="radioImageOption choiceItem" id="embroideryThreadColor_12" onclick="makeCherryRed()" ;>
                                <img src="images/thread/Cherry Red.png" alt="">
                                <p>Cher-Red</p>
                            </li>
                            <li class="radioImageOption choiceItem" id="embroideryThreadColor_13" onclick="makeDeepRed()" ;>
                                <img src="images/thread/Deep Red.png" alt="">
                                <p>Deep-Red</p>
                            </li>
                            <li class="radioImageOption choiceItem" id="embroideryThreadColor_14" onclick="makeMauve()" ;>
                                <img src="images/thread/Mauve.png" alt="">
                                <p>Mauve</p>
                            </li>
                            <li class="radioImageOption choiceItem" id="embroideryThreadColor_15" onclick="makeDeepPurple()" ;>
                                <img src="images/thread/Deep Purple.png" alt="">
                                <p>De-Purple</p>
                            </li>
                            <li class="radioImageOption choiceItem" id="embroideryThreadColor_16" onclick="makeBordeauxRed()" ;>
                                <img src="images/thread/Bordeaux Red.png" alt="">
                                <p>Bor-Red</p>
                            </li>
                            <li class="radioImageOption choiceItem" id="embroideryThreadColor_17" onclick="makePurple()" ;>
                                <img src="images/thread/Purple.png" alt="">
                                <p>Purple</p>
                            </li>
                            <li class="radioImageOption choiceItem" id="embroideryThreadColor_18" onclick="makeDistinctPurple()" ;>
                                <img src="images/thread/Distinct Purple.png" alt="">
                                <p>Dis-Purple</p>
                            </li>
                            <li class="radioImageOption choiceItem" id="embroideryThreadColor_19" onclick="makeLavender()" ;>
                                <img src="images/thread/Lavender.png" alt="">
                                <p>Lavender</p>
                            </li>
                            <li class="radioImageOption choiceItem" id="embroideryThreadColor_20" onclick="makeBrightGreen()" ;>
                                <img src="images/thread/Bright Green.png" alt="">
                                <p>Bri-Green</p>
                            </li>
                            <li class="radioImageOption choiceItem" id="embroideryThreadColor_21" onclick="makeLime()" ;>
                                <img src="images/thread/Lime.png" alt="">
                                <p>Lime</p>
                            </li>
                            <li class="radioImageOption choiceItem" id="embroideryThreadColor_22" onclick="makeRacingGreen()" ;>
                                <img src="images/thread/Racing Green.png" alt="">
                                <p>Rac-Green</p>
                            </li>
                            <li class="radioImageOption choiceItem" id="embroideryThreadColor_23" onclick="makeOliveGreen()" ;>
                                <img src="images/thread/Olive Green.png" alt="">
                                <p>Oli-Green</p>
                            </li>
                            <li class="radioImageOption choiceItem" id="embroideryThreadColor_24" onclick="makeDeepTurqouise()" ;>
                                <img src="images/thread/Deep Turqouise.png" alt="">
                                <p>Turqouise</p>
                            </li>
                            <li class="radioImageOption choiceItem" id="embroideryThreadColor_25" onclick="makeBabyBlue()" ;>
                                <img src="images/thread/Baby Blue.png" alt="">
                                <p>Baby Blue</p>
                            </li>
                            <li class="radioImageOption choiceItem" id="embroideryThreadColor_26" onclick="makeSkyBlue()" ;>
                                <img src="images/thread/Sky Blue.png" alt="">
                                <p>Sky Blue</p>
                            </li>
                            <li class="radioImageOption choiceItem" id="embroideryThreadColor_27" onclick="makeAzureBlue()" ;>
                                <img src="images/thread/Azure Blue.png" alt="">
                                <p>AzureBlue</p>
                            </li>
                            <li class="radioImageOption choiceItem" id="embroideryThreadColor_28" onclick="makeOceanBlue()" ;>
                                <img src="images/thread/Ocean Blue.png" alt="">
                                <p>OceanBlue</p>
                            </li>
                        </ul>
                    </li>
                    </li>
                </div>
            </div>
        </div>
    </div>
    <input type="button" value="Add to Cart" onclick="addToCart()" class="btn cartbtn" />
    <!--End Of Embroidery-->
    <script>
        var app = angular.module('myApp', []);
        app.controller('myCtrl', function($scope) {
            $scope.name = " ";
            $(".tent").addClass("disabledbutton");
            $('.tentOne').click(function() {
                $(".tent").addClass("disabledbutton");
            });
            $('.tentTwo').click(function() {
                $(".tent").removeClass("disabledbutton");
            });
        });
    </script>
</body>
<script src="js/libs/jquery-1.11.3.js"></script>
 <script src="js/bootstrap-3.1.1.min.js"></script>

<script src="js/libs/uiblock.js"></script>
<script src="js/libs/TweenMax.js"></script>
<script src="js/libs/three.min.js"></script>
<script src="js/libs/OrbitControls.js"></script>
<script src="js/libs/lzma.js"></script>
<script src="js/libs/ctm.js"></script>
<script src="js/libs/CTMLoader.js"></script>
<script src="js/m-Shirt_JSON.js"></script>
<script src="js/m-Shirt_meshChange.js"></script>
<script src="js/m-Shirt-RemLoad.js"></script>
<script src="js/m-Shirt.js"></script>
<script src="js/m-Shirt_cam.js"></script>
<script>
    var selectedfit = "slim_fit";
    var selectedfabric = "fa_0";
    var selectedsleeve = "long_sleeve";
    var selectedbottomcut = "modern";
    var selectedcollar = "collar_business_classic";
    var selectedcuff = "one_button_rounded";
    var selectedplacket = "yes";
    var selectedpocketplacement = "no";
    var selectedpocket = "straight";
    var selectedbutton = "f1f4e5";
    var selectedthread = "e7e8e7 ";
    function addToCart(){

        var product_name = "Men Shirt"+Math.floor(Math.random()*10000);
        var user_id = $("#user_id").val();

        if(user_id=="") {
          $("#myModalLogin").modal("show");
          return false;
        }
        $(".backloader").show();
        var url = "admin/api/orderProcess.php";
        $.post(url,{"type":"addToCart_sh","product_type":"men_shirt","product_name":product_name,"product_price":"250"
        ,"user_id":user_id,"quantity":"1","total_amount":"250","sh_fit":selectedfit,"sh_fabric":selectedfabric,
        "sh_sleeve":selectedsleeve,"sh_collar":selectedcollar,"sh_cuff":selectedcuff,"sh_placket":selectedplacket,
        "sh_bottomcut":selectedbottomcut,"sh_pocket":selectedpocket,"sh_button_color":selectedbutton,
        "sh_thread_color":selectedthread},function(data){
            var Status = data.Status;
            if(Status == "Success"){
                 $(".backloader").hide();
                showCartCount(data.cartCount);
                window.location='cart.php';
            }else{
                 $(".backloader").hide();
                $(".modal-title").html("<label style='color:red'>Error Occured</label>");
                $(".modal-body").html(data.Message);
                $("#modal").modal("show");
            }
        }).fail(function(){
             $(".backloader").hide();
            $(".modal-title").html("<label style='color:red'>Error Occured</label>");
            $(".modal-body").html("Server Error !!! Please Try After Some Time...");
            $("#modal").modal("show");
        });
    }
    function getCartCount(){
        var user_id = $("#user_id").val();
        var url = "admin/api/orderProcess.php";
        $.post(url,{"type":"getCartCount","user_id":user_id},function(data){
            var Status = data.Status;
            if(Status == "Success"){
                showCartCount(data.count);
            }
        }).fail(function(){
            $(".modal-title").html("<label style='color:red'>Error Occured</label>");
            $(".modal-body").html("Server Error !!! Please Try After Some Time...");
            $("#modal").modal("show");
        });
    }
    function showCartCount(count){
        $("#cartCount").html(count);
    }
    function opencart(){
        window.location="cart.php";
    }
    getCartCount();
</script>
<div id="modal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body">
                <p>Some text in the modal.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<?php include("footer2.php");?>