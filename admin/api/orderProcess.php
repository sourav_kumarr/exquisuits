<?php
include("Constants/configuration.php");
include("Constants/dbConfig.php");
include("Constants/functions.php");
require_once "Classes/ORDER.php";
$orderClass = new \Modals\ORDER();
$requiredFeilds = array("type");
$response = RequiredFields($_POST, $requiredFeilds);
if ($response[Status] != Success) {
    $orderClass->apiResponse($response);
    return false;
}
$type = $_POST['type'];
if ($type == "addToCart_tr") {
    $requiredFeilds = array('product_type','product_name','product_price','user_id','quantity','total_amount',
    'tr_fabric','tr_fit','tr_sidepocket','tr_backpocket','tr_button_color','tr_thread_color','tr_waistband',
    'tr_pocketfabric','tr_buttoning');
    $response = RequiredFields($_POST, $requiredFeilds);
    if ($response[Status] != Success) {
        $orderClass->apiResponse($response);
        return false;
    }
    $product_type = trim($_POST['product_type']);
    $product_name = trim($_POST['product_name']);
    $product_price = trim($_POST['product_price']);
    $user_id = trim($_POST['user_id']);
    $quantity = trim($_POST['quantity']);
    $total_amount = trim($_POST['total_amount']);
    $tr_fabric = trim($_POST['tr_fabric']);
    $tr_fit = trim($_POST['tr_fit']);
    $tr_sidepocket = trim($_POST['tr_sidepocket']);
    $tr_backpocket = trim($_POST['tr_backpocket']);
    $tr_button_color = trim($_POST['tr_button_color']);
    $tr_thread_color = trim($_POST['tr_thread_color']);
    $tr_waistband = trim($_POST['tr_waistband']);
    $tr_buttoning = trim($_POST['tr_buttoning']);
    $tr_pocketfabric = trim($_POST['tr_pocketfabric']);
    ($response = $orderClass->addToCart_tr($product_type,$product_name,$product_price,$user_id,$quantity,
    $total_amount,$tr_fabric,$tr_fit,$tr_sidepocket,$tr_backpocket,$tr_button_color,$tr_thread_color,
    $tr_waistband,$tr_buttoning,$tr_pocketfabric));
    if ($response[Status] != Success) {
        $orderClass->apiResponse($response);
        return false;
    }
    $temp = $orderClass->getCartCount($user_id);
    $count = $temp['count'];
    $response['cartCount'] = $count;
    $orderClass->apiResponse($response);
}
else if ($type == "addToCart_sh") {
    $requiredFeilds = array('product_type','product_name','product_price','user_id','quantity','total_amount',
    'sh_fit','sh_fabric','sh_sleeve','sh_collar','sh_cuff','sh_placket','sh_bottomcut','sh_pocket',
    'sh_button_color','sh_thread_color');
    $response = RequiredFields($_POST, $requiredFeilds);
    if ($response[Status] != Success) {
        $orderClass->apiResponse($response);
        return false;
    }
    $product_type = trim($_POST['product_type']);
    $product_name = trim($_POST['product_name']);
    $product_price = trim($_POST['product_price']);
    $user_id = trim($_POST['user_id']);
    $quantity = trim($_POST['quantity']);
    $total_amount = trim($_POST['total_amount']);
    $sh_fit = trim($_POST['sh_fit']);
    $sh_fabric = trim($_POST['sh_fabric']);
    $sh_sleeve = trim($_POST['sh_sleeve']);
    $sh_collar = trim($_POST['sh_collar']);
    $sh_cuff = trim($_POST['sh_cuff']);
    $sh_placket = trim($_POST['sh_placket']);
    $sh_bottomcut = trim($_POST['sh_bottomcut']);
    $sh_pocket = trim($_POST['sh_pocket']);
    $sh_button_color = trim($_POST['sh_button_color']);
    $sh_thread_color = trim($_POST['sh_thread_color']);
    ($response = $orderClass->addToCart_sh($product_type,$product_name,$product_price,$user_id,$quantity,
    $total_amount,$sh_fit,$sh_fabric,$sh_sleeve,$sh_collar,$sh_cuff,$sh_placket, $sh_bottomcut,$sh_pocket,
    $sh_button_color,$sh_thread_color));
    if ($response[Status] != Success) {
        $orderClass->apiResponse($response);
        return false;
    }
    $temp = $orderClass->getCartCount($user_id);
    $count = $temp['count'];
    $response['cartCount'] = $count;
    $orderClass->apiResponse($response);
}
else if ($type == "addToCart_su") {
    $requiredFeilds = array('product_type','product_name','product_price','user_id','quantity','total_amount',
    'su_fabric','su_fit','su_button','su_lapel','su_sleeve_button','su_lapel_stich','su_pocket','su_ticket_pocket',
    'su_vent','su_elbow_patch','su_suit_linning','su_button_color','su_thread_color');
    $response = RequiredFields($_POST, $requiredFeilds);
    if ($response[Status] != Success) {
        $orderClass->apiResponse($response);
        return false;
    }
    $product_type = trim($_POST['product_type']);
    $product_name = trim($_POST['product_name']);
    $product_price = trim($_POST['product_price']);
    $user_id = trim($_POST['user_id']);
    $quantity = trim($_POST['quantity']);
    $total_amount = trim($_POST['total_amount']);
    $su_fabric = trim($_POST['su_fabric']);
    $su_fit = trim($_POST['su_fit']);
    $su_button = trim($_POST['su_button']);
    $su_lapel = trim($_POST['su_lapel']);
    $su_sleeve_button = trim($_POST['su_sleeve_button']);
    $su_lapel_stich = trim($_POST['su_lapel_stich']);
    $su_pocket = trim($_POST['su_pocket']);
    $su_ticket_pocket = trim($_POST['su_ticket_pocket']);
    $su_vent = trim($_POST['su_vent']);
    $su_elbow_patch = trim($_POST['su_elbow_patch']);
    $su_elbow_fabric = trim($_POST['su_elbow_fabric']);
    $su_suit_linning = trim($_POST['su_suit_linning']);
    $su_button_color = trim($_POST['su_button_color']);
    $su_thread_color = trim($_POST['su_thread_color']);
    ($response = $orderClass->addToCart_su($product_type,$product_name,$product_price,$user_id,$quantity,
    $total_amount,$su_fabric,$su_fit,$su_button,$su_lapel,$su_sleeve_button,$su_lapel_stich, $su_pocket,
    $su_ticket_pocket,$su_vent,$su_elbow_patch,$su_elbow_fabric,$su_suit_linning,$su_button_color,$su_thread_color));
    if ($response[Status] != Success) {
        $orderClass->apiResponse($response);
        return false;
    }
    $temp = $orderClass->getCartCount($user_id);
    $count = $temp['count'];
    $response['cartCount'] = $count;
    $orderClass->apiResponse($response);
}
else if($type == "getCartCount"){
    $requiredFeilds = array('user_id');
    $response = RequiredFields($_POST, $requiredFeilds);
    if ($response[Status] != Success) {
        $orderClass->apiResponse($response);
        return false;
    }
    $user_id = trim($_POST['user_id']);
    ($response = $orderClass->getCartCount($user_id));
    if ($response[Status] != Success) {
        $orderClass->apiResponse($response);
        return false;
    }
    $orderClass->apiResponse($response);
}
else if($type == "getCart"){
    $requiredFeilds = array('user_id');
    $response = RequiredFields($_POST, $requiredFeilds);
    if ($response[Status] != Success) {
        $orderClass->apiResponse($response);
        return false;
    }
    $user_id = trim($_POST['user_id']);
    ($response = $orderClass->getCart($user_id));
    if ($response[Status] != Success) {
        $orderClass->apiResponse($response);
        return false;
    }
    $orderClass->apiResponse($response);
}
else if($type == "updateCart"){
    $requiredFeilds = array('pid','pprice','prod_quan','prod_total');
    $response = RequiredFields($_POST, $requiredFeilds);
    if ($response[Status] != Success) {
        $orderClass->apiResponse($response);
        return false;
    }
    $pid = trim($_POST['pid']);
    $pprice = trim($_POST['pprice']);
    $prod_quan = trim($_POST['prod_quan']);
    $prod_total = trim($_POST['prod_total']);
    ($response = $orderClass->updateCart($pid,$pprice,$prod_quan,$prod_total));
    if ($response[Status] != Success) {
        $orderClass->apiResponse($response);
        return false;
    }
    $orderClass->apiResponse($response);
}
else if($type == "removeItem"){
    $requiredFeilds = array('cart_id');
    $response = RequiredFields($_POST, $requiredFeilds);
    if ($response[Status] != Success) {
        $orderClass->apiResponse($response);
        return false;
    }
    $cart_id = trim($_POST['cart_id']);
    ($response = $orderClass->removeItem($cart_id));
    if ($response[Status] != Success) {
        $orderClass->apiResponse($response);
        return false;
    }
    $orderClass->apiResponse($response);
}
else if($type == "clearCart"){
    $requiredFeilds = array('user_id');
    $response = RequiredFields($_POST, $requiredFeilds);
    if ($response[Status] != Success) {
        $orderClass->apiResponse($response);
        return false;
    }
    $user_id = trim($_POST['user_id']);
    ($response = $orderClass->clearCart($user_id));
    if ($response[Status] != Success) {
        $orderClass->apiResponse($response);
        return false;
    }
    $orderClass->apiResponse($response);
}
else if($type == "createOrder"){
    $requiredFeilds = array('json');
    $response = RequiredFields($_POST, $requiredFeilds);
    if ($response[Status] != Success) {
        $orderClass->apiResponse($response);
        return false;
    }
    $json = trim($_POST['json']);
    ($response = $orderClass->createOrder($json));
    if ($response[Status] != Success) {
        $orderClass->apiResponse($response);
        return false;
    }
    $orderClass->apiResponse($response);
}
else
{
    $response[Status] = Error;
    $response[Message] = "502 UnAuthorized Access";
    $orderClass->apiResponse($response);
}
?>