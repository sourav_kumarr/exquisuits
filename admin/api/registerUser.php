<?php
include("Constants/configuration.php");
include("Constants/dbConfig.php");
include("Constants/functions.php");
require_once "Classes/USERS.php";
$userClass = new Modals\USERS();
if(isset($_REQUEST["_"])){
    session_start();
    $_SESSION['capd'] = $_REQUEST['_'];
    header("Location:../index.php?_=cpd");
}
else {
    $requiredFeilds = array("type");
    $response = RequiredFields($_POST, $requiredFeilds);
    if ($response[Status] != Success) {
        $userClass->apiResponse($response);
        return false;
    }
    $type = $_POST['type'];
    if ($type == "Register") {
        $requiredFeilds = array('fname', 'lname', 'countryId', 'stateId', 'cityId', 'dob', 'height',
        'gender', 'weight', 'email', 'password');
        $response = RequiredFields($_POST, $requiredFeilds);
        if ($response[Status] != Success) {
            $userClass->apiResponse($response);
            return false;
        }
        $fname = trim($_POST['fname']);
        $lname = trim($_POST['lname']);
        $countryId = trim($_POST['countryId']);
        $stateId = trim($_POST['stateId']);
        $cityId = trim($_POST['cityId']);
        $dob = trim($_POST['dob']);
        $height = trim($_POST['height']);
        $stylist = trim($_POST['stylist']);
        $gender = trim($_POST['gender']);
        $language = trim($_POST['language']);
        $weight = trim($_POST['weight']);
        $email = trim($_POST['email']);
        $password = trim($_POST['password']);
        $emailResponse = $userClass->checkEmailExistance($email);
        if ($emailResponse[Status] == Error) {
            $userClass->apiResponse($emailResponse);
            return false;
        }
        ($response = $userClass->RegisterUser($fname,$lname,$countryId,$stateId,$cityId,$dob,$height,$stylist,$gender,
        $language,$weight,$email, $password));
        if ($response[Status] != Success) {
            $userClass->apiResponse($response);
            return false;
        }
        $userId = $response['lastId'];
        $temp = $userClass->getParticularUserData($userId);
        $userData = $temp['userData'];
        $response['userData'] = $userData;
        unset($response['lastId']);
        unset($response['file_name']);
        $userClass->apiResponse($response);
    }/*else if ($type == "updateProfile") {
        $requiredFeilds = array('user_id','fname', 'lname', 'contact', 'gender', 'dob', 'height', 'weight');
        $response = RequiredFields($_POST, $requiredFeilds);
        if ($response[Status] != Success) {
            $userClass->apiResponse($response);
            return false;
        }
        $fname = trim($_POST['fname']);
        $lname = trim($_POST['lname']);
        $contact = trim($_POST['contact']);
        $gender = trim($_POST['gender']);
        $dob = trim($_POST['dob']);
        $height = trim($_POST['height']);
        $weight = trim($_POST['weight']);
        $user_id = trim($_POST['user_id']);
        $response = $userClass->updateProfile($user_id, $fname, $lname, $contact, $gender, $dob, $height, $weight);
        if ($response[Status] != Success) {
            $userClass->apiResponse($response);
            return false;
        }
        $temp = $userClass->getParticularUserData($user_id);
        $userData = $temp['userData'];
        $response['userData'] = $userData;
        unset($response['file_name']);
        $userClass->apiResponse($response);
    }*/
    else if ($type == "updateBillingAddress") {
        $requiredFeilds = array('user_id','fname', 'lname', 'address', 'country', 'state', 'city', 'pincode', 'contact');
        $response = RequiredFields($_POST, $requiredFeilds);
        if ($response[Status] != Success) {
            $userClass->apiResponse($response);
            return false;
        }
        $user_id = trim($_POST['user_id']);
        $fname = trim($_POST['fname']);
        $lname = trim($_POST['lname']);
        $address = trim($_POST['address']);
        $country = trim($_POST['country']);
        $state = trim($_POST['state']);
        $city = trim($_POST['city']);
        $pincode = trim($_POST['pincode']);
        $contact = trim($_POST['contact']);
        $response = $userClass->updateBillingAddress($user_id,$fname,$lname,$address,$country,$state,$city,$pincode,$contact);
        if ($response[Status] != Success) {
            $userClass->apiResponse($response);
            return false;
        }
        $temp = $userClass->getParticularUserData($user_id);
        $userData = $temp['userData'];
        $response['userData'] = $userData;
        unset($response['file_name']);
        $userClass->apiResponse($response);
    }
    else if ($type == "Login") {
        $requiredFeilds = array('email', 'password');
        $response = RequiredFields($_POST, $requiredFeilds);
        if ($response[Status] != Success) {
            $userClass->apiResponse($response);
            return false;
        }
        $email = trim($_POST['email']);
        $password = trim($_POST['password']);
        $response = $userClass->userLogin($email, $password);
        if ($response[Status] != Success) {
            $userClass->apiResponse($response);
            return false;
        }
        session_start();
        $_SESSION['exquisuits_user_id'] = $response['userData']['user_id'];
        $_SESSION['exquisuits_user_fname'] = $response['userData']['name'];
        $_SESSION['exquisuits_user_email'] = $response['userData']['email'];
        $userClass->apiResponse($response);
    }
    else if ($type == "forgotPassword") {
        $requiredFeilds = array('email');
        $response = RequiredFields($_POST, $requiredFeilds);
        if ($response[Status] != Success) {
            $userClass->apiResponse($response);
            return false;
        }
        $email = $_POST['email'];
        $response = $userClass->checkEmailExistance($email);
        if ($response[STATUS] == Success) {
            $response[Status] = Error;
            $response[Message] = "E-Mail is not Registered with us...";
            $userClass->apiResponse($response);
            return false;
        }
        $response = $userClass->sendMaill($email);
        $userClass->apiResponse($response);
    }
    else if ($type == "changeForgotPassword") {
        session_start();
        $requiredfields = array('new_password');
        $response = RequiredFields($_POST, $requiredfields);
        if ($response['Status'] == 'Failure') {
            $userClass->apiResponse($response);
            return false;
        }
        $email = trim($_SESSION['capd']);
        $new_password = trim($_POST['new_password']);
        $response = $userClass->changeForgotPassword($new_password, $email);
        if ($response[STATUS] == Error) {
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    }
    else if($type == "getParticularUserData"){
        $requiredfields = array('user_id');
        $response = RequiredFields($_POST, $requiredfields);
        if ($response['Status'] == 'Failure') {
            $userClass->apiResponse($response);
            return false;
        }
        $user_id = trim($_POST['user_id']);
        $response = $userClass->getParticularUserData($user_id);
        if($response[Status] == Error){
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    }
    else {
        $response[Status] = Error;
        $response[Message] = "502 UnAuthorized Access";
        $userClass->apiResponse($response);
    }
}
?>