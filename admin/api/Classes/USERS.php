<?php
namespace Modals;
require_once "CONNECT.php";
class USERS
{
    private $link = null;
    private $link2 = null;
    public $CurrentDateTime = null;
    function __construct()
    {
        $this->link = new CONNECT();
        $this->link2 = new CONNECT();
        $this->CurrentDateTime = date("Y-m-d h:i:s");
    }
    public function RegisterUser($fname,$lname,$countryId,$stateId,$cityId,$dob,$height,$stylist,$gender,$language,
    $weight,$email, $password){
        if($this->link->Connect() && $this->link2->Connect2()){
            $file_response = $this->uploadPicture();
            if($file_response[Status] == Error){
                $this->link->response[Status] == Error;
                $this->link->response[Message] == $file_response[Message];
            }else {
                $file_name = $file_response['file_name'];
                if($file_name != ""){
                    $file_name = MainServer."/admin/api/Files/users/".$file_name;
                }
                $query = "insert into wp_customers(name,dob,height,profile_pic,email,password,gender,weight,site_name,
                app_type,b_country,b_state,b_city,driver_licence,face_login,email_receive,lang,stylist) values ('$fname $lname',
                '$dob','$height Inches','$file_name','$email','$password','$gender','$weight Pounds','Benjamin',
                'Website','$countryId','$stateId','$cityId','false','false','false','$language','$stylist')";
                $result = mysqli_query($this->link2->Connect2(), $query);
                if ($result) {
                    $user_id = $this->link2->lastId2();
                    $query2 = "insert into benj_users(user_id,email,password,fname,lname,gender,dob,height,
                    weight,user_profile,stylist,city,state,country,status) values ('$user_id','$email','$password',
                    '$fname','$lname','$gender','$dob','$height Inches','$weight Pounds','$file_name','$stylist',
                    '$cityId','$stateId','$countryId','1')";
                    $result2 = mysqli_query($this->link->Connect(), $query2);
                    if ($result2) {
                        $this->link->response[Status] = Success;
                        $this->link->response[Message] = "User Registered Successfully";
                        $this->link->response['lastId'] = $user_id;
                    }
                    else{
                        $this->link->response[Status] = Error;
                        $this->link->response[Message] = $this->link->sqlError();
                    }
                } else {
                    $this->link->response[Status] = Error;
                    $this->link->response[Message] = $this->link2->sqlError2();
                }
            }
        }
        else{
            $this->link->response[Status] = Error;
            $this->link->response[Message] = $this->link->sqlError();
        }
        return $this->link->response;
    }
    public function checkEmailExistance($email)
    {
        if($this->link->Connect() && $this->link2->Connect2()){
            $query = "select * from wp_customers where email = '$email'";
            $result = mysqli_query($this->link2->Connect2(),$query);
            if($result){
                $num = mysqli_num_rows($result);
                if($num>0){
                    $this->link->response[Status] = Error;
                    $this->link->response[Message] = "Email Already Registered";
                }
                else{
                    $query2 = "select * from benj_users where email = '$email'";
                    $result2 = mysqli_query($this->link->Connect(),$query2);
                    if($result2){
                        $num2 = mysqli_num_rows($result2);
                        if($num2>0){
                            $this->link->response[Status] = Error;
                            $this->link->response[Message] = "Email Already Registered";
                        }
                        else{
                            $this->link->response[Status] = Success;
                            $this->link->response[Message] = "New User";
                        }
                    }
                    else{
                        $this->link->response[Status] = Error;
                        $this->link->response[Message] = $this->link->sqlError();
                    }
                }
            }
            else{
                $this->link->response[Status] = Error;
                $this->link->response[Message] = $this->link2->sqlError2();
            }
        }
        else{
            $this->link->response[Status] = Error;
            $this->link->response[Message] = $this->link->sqlError();
        }
        return $this->link->response;
    }
    public function getParticularUserData($userId)
    {
        if($this->link2->Connect2()){
            $query = "select * from wp_customers where user_id = '$userId'";
            $result = mysqli_query($this->link2->Connect2(),$query);
            if($result){
                $num = mysqli_num_rows($result);
                if($num>0){
                    $userData = mysqli_fetch_assoc($result);
                    $this->link->response[Status] = Success;
                    $this->link->response[Message] = "User Data Found";
                    $this->link->response['userData'] = $userData;
                }
                else{
                    $this->link->response[Status] = Error;
                    $this->link->response[Message] = "UnAuthorized User";
                }
            }
            else{
                $this->link->response[Status] = Error;
                $this->link->response[Message] = $this->link2->sqlError2();
            }
        }
        else{
            $this->link->response[Status] = Error;
            $this->link->response[Message] = $this->link2->sqlError2();
        }
        return $this->link->response;
    }
    /*public function updateProfile($user_id, $fname, $lname, $contact, $gender, $dob, $height, $weight){
        if($this->link->Connect() && $this->link2->Connect2()){
            $file_response = $this->uploadPicture();
            if($file_response[Status] == Error){
                $this->link->response[Status] == Error;
                $this->link->response[Message] == $file_response[Message];
            }else {
                $file_name = $file_response['file_name'];
                if($file_name != "") {
                    $file_name = MainServer."/api/Files/users/".$file_name;
                    $query = "update wp_customers set name='$fname $lname',mobile='$contact',gender='$gender',dob='$dob',
                    height='$height Inches',weight='$weight Kilograms',profile_pic='$file_name' where user_id = '$user_id'";
                }else{
                    $query = "update wp_customers set name='$fname $lname',mobile='$contact',gender='$gender',dob='$dob',
                    height='$height Inches',weight='$weight Kilograms' where user_id = '$user_id'";
                }
                $result = mysqli_query($this->link2->Connect2(), $query);
                if ($result) {
                    $file_name = $file_response['file_name'];
                    if($file_name != "") {
                        $file_name = MainServer."/api/Files/users/".$file_name;
                        $query2 = "update users set fname='$fname',lname='$lname',contact='$contact',gender='$gender',dob='$dob',
                        height_in='$height',weight='$weight',user_profile='$file_name' where user_id = '$user_id'";
                    }else{
                        $query2 = "update users set fname='$fname',lname='$lname',contact='$contact',gender='$gender',dob='$dob',
                        height_in='$height',weight='$weight' where user_id = '$user_id'";
                    }
                    $result2 = mysqli_query($this->link->Connect(), $query2);
                    if ($result2) {
                        $this->link->response[Status] = Success;
                        $this->link->response[Message] = "User Detail Updated Successfully";
                    } else {
                        $this->link->response[Status] = Error;
                        $this->link->response[Message] = $this->link->sqlError();
                    }
                } else {
                    $this->link->response[Status] = Error;
                    $this->link->response[Message] = $this->link2->sqlError2();
                }
            }
        }
        else{
            $this->link->response[Status] = Error;
            $this->link->response[Message] = $this->link->sqlError();
        }
        return $this->link->response;
    }*/
    public function updateBillingAddress($user_id,$fname,$lname,$address,$country,$state,$city,$pincode,$contact){
        if($this->link->Connect() && $this->link2->Connect2()){
            $query = "update wp_customers set name='$fname $lname',mobile='$contact',b_country='$country',
            b_state='$state',b_city='$city',address='$address',pincode='$pincode' where user_id = '$user_id'";
            $result = mysqli_query($this->link2->Connect2(), $query);
            if ($result) {
                $query2 = "update benj_users set fname='$fname',lname='$lname',contact='$contact',country='$country',
                state='$state',city='$city',address='$address',pincode='$pincode' where user_id = '$user_id'";
                $result2 = mysqli_query($this->link->Connect(), $query2);
                if ($result2) {
                    $this->link->response[Status] = Success;
                    $this->link->response[Message] = "Billing Address Updated Successfully";
                } else {
                    $this->link->response[Status] = Error;
                    $this->link->response[Message] = $this->link->sqlError();
                }
            } else {
                $this->link->response[Status] = Error;
                $this->link->response[Message] = $this->link2->sqlError2();
            }
        }
        else{
            $this->link->response[Status] = Error;
            $this->link->response[Message] = $this->link->sqlError();
        }
        return $this->link->response;
    }
    public function userLogin($email,$password)
    {
        if($this->link2->Connect2()){
            $query = "select * from wp_customers where email='$email' and password='$password'";
            $result = mysqli_query($this->link2->Connect2(),$query);
            if($result){
                $num = mysqli_num_rows($result);
                if($num>0){
                    $userData = mysqli_fetch_assoc($result);
                    $this->link->response[Status] = Success;
                    $this->link->response[Message] = "Login Success";
                    $this->link->response['userData'] = $userData;
                    $this->storeDatatoTable($userData);
                }
                else{
                    $this->link->response[Status] = Error;
                    $this->link->response[Message] = "Invalid User Credential";
                }
            }
            else{
                $this->link->response[Status] = Error;
                $this->link->response[Message] = $this->link2->sqlError2();
            }
        }
        else{
            $this->link->response[Status] = Error;
            $this->link->response[Message] = $this->link2->sqlError2();
        }
        return $this->link->response;
    }
    public function uploadPicture(){
        if(isset($_FILES['user_profile'])){
            $file_name = $_FILES['user_profile']['name'];
            $file_tmp = $_FILES['user_profile']['tmp_name'];
            $file_ext=strtolower(end(explode('.',$file_name)));
            $expensions= array("jpeg","jpg","png","gif");
            if(in_array($file_ext,$expensions)=== false){
                $this->link->response[Status] = Error;
                $this->link->response[Message] = "File Should Be JPG, PNG, GIF Formats Only";
            }
            else{
                $file_name = "File".rand(000000,999999).".".$file_ext;
                if(move_uploaded_file($file_tmp,"Files/users/".$file_name)){
                    $this->link->response[Status] = Success;
                    $this->link->response[Message] = "File Uploaded Successfully";
                    $this->link->response['file_name'] = $file_name;
                }
                else{
                    $this->link->response[Status] = Error;
                    $this->link->response[Message] = "Error While Uploading File";
                }
            }
        }
        else{
            $this->link->response[Status] = Success;
            $this->link->response[Message] = "File Not Selected";
            $this->link->response['file_name'] = "";
        }
        return $this->link->response;
    }
    public function sendMaill($email)
    {
        $to = $email;
        $subject = "Account Password Reset";
        $message = '<div style="width:100%;background:#ccc;padding-bottom:36px"><div style="width:80%;margin: 0 10%">
        <div style="background:#eee;border-bottom:1px solid #ccc;height:63px;padding:10px 0 0 10px"><img style="float:left;
        height:50px" src="http://scan2fit.com/benjamin/images/logo/logo.png" alt="Benjamin Custom Suits" />
        <p style="float: left;color:#666;font-size:15px;font-weight:bold;margin-left:10px">Hello ! User</p>
        </div><div style="background:#fff;margin-top:-15px;padding-left:69px;padding-bottom:35px"><p style="color:green;
        line-height:40px;font-weight:bold;font-size:15px">Greetings of the Day,</p>
        <p>Welcome To Benjamin Custom Suits</p><p>Here Is The Change Password link . Click here to Change your Account Password.</p>
        <a style="text-decoration:none" href="'.MainServer.'/admin/api/registerUser.php?_='.$email.'" >
        <input type="button" style="background:#d14130;border-radius:3px;color: white;font-weight:600;margin: 10px 0;
        padding: 10px 20px" value="Change Password" /></a>
        <div style="background:#eee;border:1px solid #ccc;height:63px;padding:0 0 10px 10px">
        <p style="font-weight:bold;color:#666">Thanks & Regards<br><br>Benjamin Custom Suits Team</p>
        </div></div></div>';
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        // More headers
        $headers .= 'From: <scan2fit.com>' . "\r\n";
        if(mail($to,$subject,$message,$headers)){
            $this->link->response[Status] = Success;
            $this->link->response[Message] = "Password Reset Link has been sent to your Registered E-Mail Address";
        }
        else{
            $this->link->response[Status] = Error;
            $this->link->response[Message] = "Server Error !!! Please Try After Some Time";
        }
        return $this->link->response;
    }
    public function changeForgotPassword($newPassword,$email){
        if($this->link2->connect2()) {
            $query = "select * from wp_customers where email = '$email'";
            $result = mysqli_query($this->link2->connect2(), $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($this->link2->connect2(), "UPDATE wp_customers SET password='$newPassword'
                    WHERE email='$email'");
                    if ($update) {
                        $this->link->response[STATUS] = Success;
                        $this->link->response[MESSAGE] = "Password Has Been Changed Successfully";
                    } else {
                        $this->link->response[STATUS] = Error;
                        $this->link->response[MESSAGE] = $this->link2->sqlError2();
                    }
                } else {
                    $this->link->response[STATUS] = Error;
                    $this->link->response[MESSAGE] = "Invalid User";
                }
            } else {
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link2->sqlError2();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }
    public function storeDatatoTable($userData)
    {
        $link = $this->link->connect();
        if($link) {
            $email = $userData['email'];
            $query = "select * from benj_users where email = '$email'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                $user_id = $userData['user_id'];
                $name = explode(" ",$userData['name']);
                $fname = $name[0];
                $lname = $name[1];
                $password = $userData['password'];
                $contact = $userData['mobile'];
                $gender = $userData['gender'];
                $dob = $userData['dob'];
                $height = $userData['height'];
                $weight = $userData['weight'];
                $stylist = $userData['stylist'];
                $address = $userData['address'];
                $pincode = $userData['pincode'];
                $city = $userData['b_city'];
                $state = $userData['b_state'];
                $country = $userData['b_country'];
                $user_profile = $userData['profile_pic'];
                $status = '1';
                if ($num > 0) {
                    //update my table data
                    $query = "update benj_users set user_id='$user_id',fname='$fname',lname='$lname',
                    password='$password',contact='$contact',gender='$gender',dob='$dob',height='$height',
                    weight='$weight',user_profile='$user_profile',status='$status',stylist='$stylist',
                    address='$address',pincode='$pincode',city='$city',state='$state',country='$country' 
                    where email = '$email'";
                } else {
                    //insert new row in my table
                    $query = "insert into benj_users (user_id,fname,lname,password,contact,gender,dob,height,weight,
                    user_profile,status,email,stylist,pincode,address,country,state,city) values('$user_id','$fname','$lname','$password','$contact','$gender',
                    '$dob','$height','$weight','$user_profile','$status','$email','$stylist','$pincode','$address',
                    '$country','$state','$city')";
                }
                $result = mysqli_query($this->link->Connect(),$query);
                if($result){
                    $this->link->response[STATUS] = Success;
                    $this->link->response[MESSAGE] = "Login Success";
                }
                else{
                    $this->link->response[STATUS] = Error;
                    $this->link->response[MESSAGE] = $this->link->sqlError();
                }
            } else {
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }
    public function apiResponse($response){
        header("Content-Type:application/json");
        echo json_encode($response);
    }
}