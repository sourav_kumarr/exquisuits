<?php
session_start();
$user_id = "";
if(isset($_SESSION['exquisuits_user_id'])){
	$user_id = $_SESSION['exquisuits_user_id'];
}
echo "<input type='hidden' id='user_id' value='".$user_id."' />";
?>
<html lang="zxx">
<head>
<title>Trajes a Medida Online - Exquisuits by de Juana</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Sewing Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/font-awesome.css" rel="stylesheet"> 
<link href="css/simpleLightbox.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/text-style.css"> <!--Banner-text-style -->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/custom.css" type="text/css" rel="stylesheet">
<!--fonts-->
<link href="//fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
<link href="css/main.css" rel="stylesheet" type="text/css" media="all" />
<!--//fonts-->


<script type="text/javascript">
    function googleTranslateElementInit() {
        new google.translate.TranslateElement({
            pageLanguage: 'en', includedLanguages: 'en,es'
        }, 'google_translate_element');
    }
</script>
<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

</head>
<body>
<!--banner start here-->
<div class="backloader" style="display: none;">
    <div class="loader"></div>
</div>

<div class="banner" id="home">
   <div class="header">
		<div class="header-main">
		   	<div class="container-fluid">
			<nav class="navbar navbar-default">
					<div class="navbar-header" style="width: 100%;text-align: center;">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>

						<a class="logo a-reset" href="https://www.exquisuits.com/" title="Trajes a medida Online – Exquisuits">
							<img src="images/exquisuits_logoweb-m.png" alt="Trajes a medida Online – Exquisuits">
						</a>
						<!-- <h1><a  href="index.html">Sewing<span>Great look in Outfit</span></a></h1> -->
					</div>
					<!-- navbar-header -->
					<!--<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav navbar-right">
							
							<li class="scroll hvr-outline-in"><a href="index.html">Home</a></li>
							<li><a class="scroll hvr-outline-in header-about" href="#about">About</a></li>

							<li><a class="scroll hvr-outline-in" href="#works">Customer Reviews</a></li>
							<?php /*
							if($user_id != ""){
								*/?>
								<li><a href='customize.php' class="hvr-outline-in" >Order Now</a></li>
								<?php
/*							}else{
								*/?>
								<li><a class="hvr-outline-in" data-toggle="modal"  data-target="#myModalLogin">Order Now</a></li>
								<?php
/*							}
							*/?>
							<li><a class="scroll hvr-outline-in" href="#gallery">Gallery</a></li>
							<li><a class="scroll hvr-outline-in" href="#contact">Contact</a></li>
							<?php
/*							if($user_id != ""){
						 		*/?>
								<li><a class="scroll hvr-outline-in" onclick="logout()">Logout</a></li>
								<li onclick="opencart()"><a class="hvr-outline-in" >
									<span class="fa fa-shopping-cart fa-2x" style="color:white; cursor:pointer;">
										
									<label id="cartCount" class="badge" style="position:absolute;margin-top:-10px;">0</label></a>
									</span>
								</li>
								<?php /*
							} 
							else{
								*/?>
								<li class="scroll hvr-outline-in">
									<a class="scroll hvr-rectangle-in-outline-in" data-toggle="modal"  data-target="#myModalLogin" onclick="loginCheck()" >
									Login/Register </a>
								</li>
								
								<?php
/*						 	}
						 	*/?>
						 	<li><div id="google_translate_element"></div></li>
					  	</ul>
					</div>-->
					<div class="clearfix"> </div>	
				</nav>
			 <div class="clearfix"> </div>
		   </div>
		</div>
<div class="container">
		   <div class="banner-text">
		   	  <section class="cd-intro">
		<h2 class="cd-headline letters scale">
			<span class="color-gold">clothes that need to be</span><br>
            <span class="cd-new-color">Sewing,mending</span><br>
                <span class="cd-new-color">applique,alteration
				<!--<b>mending</b>
				<b>applique</b>
				<b>alteration</b>-->
			</span>
		</h2>
	</section> 
		<div class="botton-agileits">
			<a href="customize.php" class="hvr-rectangle-in heading-text start-designing-btn">START DESIGNING</a>
		</div>
			<div class="agileits_w3layouts_call_mail">
					<ul>
						<li><i class="fa fa-phone" aria-hidden="true"></i>(+34) 944 914 137</li>
						<li><i class="fa fa-envelope-o" aria-hidden="true"></i><a href="mailto:maria@example.com">info@exquisuits.com</a></li>
					</ul>
			</div>
			<div class="banner-icons-agileinfo">
				<ul class="agileits_social_list">
					<li><a href="https://www.facebook.com/Exquisuits/" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
					<li><a href="https://twitter.com/exquisuits" class="agile_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
					<li><a href="https://pinterest.com/exquisuits/" class="w3_agile_dribble"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
				</ul>
			</div>
		</div>  
		</div>
	</div>
</div>
<!--banner end here-->

<!-- Modal1 -->
	<div class="modal fade" id="myModal1" tabindex="-1" role="dialog">
		<div class="modal-dialog">
		<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4>Exquisuits</h4>
					<img src="images/a1.jpg" alt="" class="img-responsive">
					<h3 class="subheading-w3l">Welcome to <span>Exquisuits</span> Designs</h3>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting.</p>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="myModalLogin" tabindex="-1" role="dialog">
		<div class="modal-dialog">
		<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="heading-text">Login Your Account</h4>
								<div class="panel panel-login">
									<div class="panel-heading  login-register ">
										<div class="row ">
											<div class="col-xs-6 text-center">
												<a href="#" class="active login-form-link sub-heading-text">Login</a>
											</div>
											<div class="col-xs-6 text-center">
												<a href="#" class="register-form-link sub-heading-text">Register</a>
											</div>
										</div>
										<hr>
									</div>
									<div class="panel-body">
										<div class="row">
												<p class="error" style="color: red; text-align: center; backface-visibility: hidden;"></p>
											<div class="col-lg-12">
												<form id="login-form" role="form" style="display: block;">
													<div class="form-group">
														<input type="text" name="username" id="logemail" tabindex="1" class="form-control" placeholder="Username" value="">
													</div>
													<div class="form-group">
														<input type="password" name="password" id="logpassword" tabindex="2" class="form-control" placeholder="Password">
													</div>
													<div class="form-group">
														<div class="row">
															<div class="col-sm-6">
																<div class="text-center">
																	<!-- <a href="https://phpoll.com/recover" tabindex="5" class="forgot-password">Forgot Password?</a> -->
																</div>
															</div>

															<div class="col-sm-6 ">
																<input type="button" name="login-submit" id="loginbtn" tabindex="4" class="form-control btn btn-login normal-text" value="Log In" style="background-color:#1b5a7c;color:white;">
															</div>
															<div class="col-sm-12 text-center" style="padding-top:10px;"><span 
															class="normal-text" > New to Benjamin? Sign up<a href="#" class="active register-form-link normal-text"> Here</a></span></div>
														</div>
													</div>
												</form>
												<form id="register-form" role="form" style="display: none;">
													<div class="form-group col-lg-6">
														<input type="text" name="firstname" id="fname" tabindex="1" class="form-control normal-text" placeholder="Firstname">
													</div>

													<div class="form-group col-lg-6">
														<select onchange="setdate()" class="form-control countries form-select-box normal-text" tabindex="2" id="countryId" onchange="country()">
															<option value="Select Country ">Select Country</option>
														</select>
													</div>

													<div class="form-group col-lg-6">
														<input type="text" name="lastname" id="lname" tabindex="1" class="form-control normal-text" placeholder="Lastname" value="">
													</div>


													<div class="form-group col-lg-6">
														<select class="form-control states form-select-box normal-text" id="stateId" tabindex="2">
															<option value="Select State ">Select State </option>
														</select>														
													</div> 

													<div class="form-group col-lg-6">
														<input type="file" name="img" id="file0" tabindex="2" class="form-control normal-text" value="">
													</div>

													<div class="form-group col-lg-6" id="cityfeild">
														<select class="form-control cities form-select-box normal-text" id="cityId" tabindex="2">
															<option value="Select City ">Select City </option>														  
														</select>	
													</div>
													

													<div class="form-group col-lg-2" style="padding-right:1px!important;">
														<select class="form-control form-select-box normal-text" id="weightparam" onchange="f2()" tabindex="2">
															<option value="">Parameter</option>
															  <option value="Pounds">Pounds</option>
															  <option value="Kilograms">Kilograms</option>
															  <option value="Stones">Stones</option>
														</select>
													</div>

													

													<div class="form-group col-lg-4"  id="weightp" style="padding-left:1px!important;">
														<input type='text' name='weight' id='weight' class='form-control normal-text' placeholder='Enter Weight' tabindex="2">
													</div>
													<div class="form-group col-lg-6">
														<input type="text" name="dob" id="inputdob" tabindex="1" class="form-control form_date normal-text" 
														placeholder="dob">														
													</div>
													<div class="form-group col-lg-6">
														<input type="text" name="stylist" id="stylist" tabindex="1" class="form-control normal-text" placeholder="stylist">
													</div>

													<div class="form-group col-lg-6">
														<select class="form-control form-select-box normal-text" id="gender" tabindex="1">
															<option value="">Select Gender </option>
															<option value="Male">Male</option>
															<option value="Female">Female</option>
														</select>
													</div>
													<div class="form-group col-lg-6">
														<select class="form-control form-select-box normal-text" id="language" tabindex="1">
															<option value="">Select Language</option>
															<option value="English(US)">English(US)</option>
															<option value="English(UK)">English(UK)</option>
															<option value="FRENCE">FRENCE</option>
														</select>
													</div>
													
													<div class="form-group col-lg-2" style="padding-right:11px!important;">
														<select class="form-control form-select-box normal-text" id="heightparam" onchange="f1()" style="padding-right: 2px" 
														tabindex="2">
															<option value="">Parameter</option>
															  <option value="inch">Inch</option>
															  <option value="Centimeter">Centimeter</option>
															  <option value="Feets">Feets</option>
														</select>														
													</div>

													<div class="form-group col-lg-4"  id="heightp" style="padding-left:6px!important;">
														<input type="text" placeholder="Height" id="height" class="form-control normal-text" tabindex="2">
													</div>

													<div class="form-group col-lg-12 ">
														<input type="email" name="email" id="email" tabindex="2" class="form-control normal-text" placeholder="Email Address" value="">
													</div>

													<div class="form-group col-lg-12 ">
														<input type="password" name="password" id="password" tabindex="2" class="form-control normal-text" placeholder="Password">
													</div>
													<div class="form-group">
															<div class="col-sm-4 ">
																<input type="button" name="register-submit" id="register-submit" tabindex="4" 
																class="form-control btn btn-register normal-text" style="background-color:#1b5a7c;color:white;" 
																value="Register Now" onclick="register()">
															</div><br/><br/>
															<span class="normal-text">You have already account So <a href="#" class="active login-form-link normal-text">Login</a></span>
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>
					</div>
				</div>
			</div>
	<!-- //Modal1 -->
	</div>
	</div>
	<div id="loginNecessary" class="modal fade in" tabindex="-1" role="dialog" style="display: ;" aria-hidden="false">
			<div class="modal-dialog" style="margin-top: 180px!important">
				<div class="modal-content">
						<h4 style="margin-top:35px;">Exquisuits</h4>
						<h3 class="subheading-w3l" style="text-align: center!important;margin-bottom:50px!important;">Please login <span>First</span> </h3>
						<a href="index.php"><p style="text-align:center;padding:12px;background:#E91F30;width:100%;color:#fff;font-size:24px;"
						>OK</p></a>
					</div>
				</div>
			</div>
	</div>

<script type="text/javascript"></script>