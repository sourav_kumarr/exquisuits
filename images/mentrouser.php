<?php
include("header2.php");
session_start();
if(!isset($_SESSION['exquisuits_user_id'])){
    header("Location:index.php");
    exit();
}
echo "<input type='hidden' value='".$_SESSION['exquisuits_user_id']."' id='user_id' />";

?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
<link rel="stylesheet" href="login/css/login.css" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" type="text/css" rel="stylesheet" />
<link href="//fonts.googleapis.com/css?family=Roboto+Condensed:400,400i,700,700i" rel="stylesheet" type="text/css" media="all"/>
<style>
    .loadimage{
        display: none;
        height: 31px;
        left: 140px;
        position: absolute;
        top: 530px;
    }
    .cartbtn {
        position: fixed;
        right: 10px;
        top: 300px;
        width: 200px;
        z-index: 999999;
        display: none;
    }
    .site-footer {
        background-color: #1e2921;
    }

    .social h1,
    .social a
    .header,
    .shop-name {
        color: #ffffff;
    }

    .shop-name {
        fill: #ffffff;
    }

    .site-footer,
    .site-footer .muted small,
    .site-footer .muted a,
    .site-footer .list-column a,
    .payment-icons li,
    .social-icons li a,
    .site-header__logo {
        color: #ffffff;
    }

    .site-footer hr,
    .site-footer .grid__item {
        border-color: #27372c;
    }

    .footer-wrapper hr.full-width {
        width: 200%;
        margin-left: -50%;
        margin-top: 0;
    }
    #left_container
    {
        width:145px;
        height:100%;
        overflow:scroll;
        position:absolute;
    }
    .left_container_img
    {
        border-top : 10px solid rgb(140,140,140);
        border-left : 10px solid rgb(140,140,140);
        border-right : 10px solid rgb(140,140,140);
        padding:10px;
        text-align: center;
        cursor:pointer;
    }
    .left_container_img_last
    {
        border : 10px solid rgb(140,140,140);
    }
    .left_container_img img
    {
        width:75px;
    }
    .panel
    {
        position:absolute;
        top:110px;
        left:160px;
        width:400px;
        border:1px solid #e3e3e3;
        box-shadow: 5px 5px 0 rgba(0, 0, 0, 0.05);
        -webkit-box-shadow: 5px 5px 0 rgba(0, 0, 0, 0.05);
        -moz-box-shadow:    5px 5px 0 rgba(0, 0, 0, 0.05);
        display:none;
    }
    .heading
    {
        background-color:#F8F8F8;
        padding:10px;
    }
    .title
    {
        text-align:left;
        display:inline-block;
        font-size:18px;
    }
    .close
    {
        float:right;
        width:20px;
        cursor:pointer;
    }
    .content_inner
    {
        display: block;
        height: 80px;
        border-top:1px solid #e3e3e3;
        cursor:pointer;
        background-color:#F8F8F8;
    }
    .content_inner.active
    {
        background-color:#F0F0F0 ;
    }
    .content_inner:hover
    {
        background-color:#F0F0F0 ;
    }
    .content_inner img
    {
        float:left;
        width:75px;
    }
    .content_inner div
    {
        font-size:18px;
        font-weight:bold;
        margin-top:20px;
    }
    .content_inner div span
    {
        font-size:12px;
        font-weight:normal;
    }
    .content_inner.fabric{
        height:120px;
    }
    .content_inner.fabric img{
        width:118px;
    }
    .badge {
        background: red none repeat scroll 0 0;
        margin-left: -15px;
        margin-top: -27px;
    }
    .progdiv {
        left: 40%;
        position: absolute;
        top: 50%;
        width: 30%;
        display:none;
    }
    .pload{
        left: 40%;
        position: absolute;
        top: 46%;
        width: 30%;
        display:none;
        text-align: center;
    }
</style>
<body class="main" style="overflow-x:hidden">
    <div id="main-container" class="">
        <div id="left_container">
            <div id="tr-type-container" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_type')"><img id="img_type" src="images/tr_type/type.svg" /><div>Type</div></div>
            <div id="fabric-icon-div" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_fabric')"><img id="img_fabric" src="images/tr_fabric/Black Pique.jpg" width="60" height="60" /><div>Fabric</div></div>
            <div id="tr-fit-container" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_fit')"><img id="img_fit" src="images/tr_fit/fit.svg" /><div>Fit</div></div>
            <div id="tr-sidePoc-container" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_sidepocket')"><img id="img_sidepocket" src="images/tr_sidepocket/sidepocket.svg" /><div>Side Pocket</div></div>
            <div id="tr-backPoc-container" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_backpocket')"><img id="img_backpocket" src="images/tr_backpocket/backpocket.svg" /><div>Back Pocket</div></div>
            <div id="tr-seam-container" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_seam')"><img id="img_seam" src="images/tr_seam/seam.svg" /><div>Seam</div></div>
            <div id="tr-coin-container" class="left_container_img left_container_img_last" onclick="camerAnim(this.id);showPanel('panel_coinpocket')"><img id="img_coinpocket" src="images/tr_coinpocket/coinpocket.svg" /><div>Coin Pocket</div></div>
            <div id="button-icon-div" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_button')"><img id="img_button" src="images/tr_button/Sorrento-white.png" /><div>Button</div></div>
            <div id="thread-icon-div" class="left_container_img left_container_img_last" onclick="camerAnim(this.id);showPanel('panel_thread')"><img id="img_thread" src="images/tr_thread/White.png" /><div>Thread</div></div>
        </div>
        <p class="pload">Please Wait ... Object Is Loading</p>
        <div class="progress progdiv">
            <div id="prog" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40"
                 aria-valuemin="0" aria-valuemax="100" style="width:0%">
                0%
            </div>
        </div>
        <!-- Type -->
        <div id="panel_type" class="panel mol_panel" >
            <div class="heading">
                <div class="title">Type</div>
                <img id="tr_type_close" class="close" src="images/close.png" onclick="closePanel();camerAnim(this.id);"/>
            </div>
            <div class="content">
                <div id="MT-Type_Regular" onclick="meshChange(this.id);" class="content_inner type active seamer" title="type|type-chinos.svg" >
                    <img src="images/tr_type/type-chinos.svg" />
                    <div>Chinos</div>
                </div>
                <div id="MT-Type_Shorts" onclick="meshChange(this.id);" class="content_inner type" title="type|type-shorts.svg">
                    <img src="images/tr_type/type-shorts.svg" />
                    <div>Shorts</div>
                </div>
            </div>
        </div>
        <!-- Fit -->
        <div id="panel_fit" class="panel mol_panel" >
            <div class="heading">
                <div class="title">Fit</div>
                <img id="tr_fit_close" class="close" src="images/close.png" onclick="closePanel();camerAnim(this.id);"/>
            </div>
            <div class="content">
                <div id="MT-Fit_Tapered" onclick="meshChange(this.id);" class="content_inner fit active" title="fit|fit-tapered.svg" >
                    <img src="images/tr_fit/fit-tapered.svg" />
                    <div>Tapered</div>
                </div>
                <div id="MT-Fit_Regular" onclick="meshChange(this.id);" class="content_inner fit" title="fit|fit-regular.svg" >
                    <img src="images/tr_fit/fit-regular.svg" />
                    <div>Regular</div>
                </div>
                <div id="MT-Fit_Loose" onclick="meshChange(this.id);" class="content_inner fit" title="fit|fit-loose.svg">
                    <img src="images/tr_fit/fit-loose.svg" />
                    <div>Loose</div>
                </div>
            </div>
        </div>
        <!-- Fabric -->
        <div id="panel_fabric" class="panel mol_panel" >
            <div class="heading">
                <div class="title">Fit</div>
                <img id="fabric-sub-close" class="close" src="images/close.png" onclick="closePanel();camerAnim(this.id);"/>
            </div>
            <div class="content" style="overflow-y:scroll;height:500px;">
                <div id="fa_0" onclick="textureChange(this.id);" class="content_inner fabric active" title="fabric|Black Pique.jpg">
                    <img src="images/tr_fabric/Black Pique.jpg" />
                    <div>Black Pique</div>
                </div>
                <div id="fa_1" onclick="textureChange(this.id);" class="content_inner fabric" title="fabric|Black Twill.jpg">
                    <img src="images/tr_fabric/Black Twill.jpg" width="60" height="60" />
                    <div>Black Twill</div>
                </div>
                <div id="fa_2" onclick="textureChange(this.id);" class="content_inner fabric" title="fabric|Blood red Pique.jpg">
                    <img src="images/tr_fabric/Blood red Pique.jpg" width="60" height="60" />
                    <div>Blood red Pique</div>
                </div>
                <div id="fa_3" onclick="textureChange(this.id);" class="content_inner fabric" title="fabric|Brown Check.jpg">
                    <img src="images/tr_fabric/Brown Check.jpg" width="60" height="60" />
                    <div>Brown Check</div>
                </div>
                <div id="fa_4" onclick="textureChange(this.id);" class="content_inner fabric" title="fabric|Brown HB Twill.jpg">
                    <img src="images/tr_fabric/Brown HB Twill.jpg" width="60" height="60" />
                    <div>Brown HB Twill</div>
                </div>
                <div id="fa_5" onclick="textureChange(this.id);" class="content_inner fabric" title="fabric|Brown Pique.jpg">
                    <img src="images/tr_fabric/Brown Pique.jpg" width="60" height="60" />
                    <div>Brown Pique</div>
                </div>
                <div id="fa_6" onclick="textureChange(this.id);" class="content_inner fabric" title="fabric|Brown Stripe.jpg">
                    <img src="images/tr_fabric/Brown Stripe.jpg" width="60" height="60" />
                    <div>Brown Stripe</div>
                </div>
                <div id="fa_7" onclick="textureChange(this.id);" class="content_inner fabric" title="fabric|Brown Twill.jpg">
                    <img src="images/tr_fabric/Brown Twill.jpg" width="60" height="60" />
                    <div>Brown Twill</div>
                </div>
                <div id="fa_8" onclick="textureChange(this.id);" class="content_inner fabric" title="fabric|Dark Blue Tusser.jpg">
                    <img src="images/tr_fabric/Dark Blue Tusser.jpg" width="60" height="60" />
                    <div>Dark Blue Tusser</div>
                </div>
                <div id="fa_9" onclick="textureChange(this.id);" class="content_inner fabric" title="fabric|Dark Brown Tusser.jpg">
                    <img src="images/tr_fabric/Dark Brown Tusser.jpg" width="60" height="60" />
                    <div>Dark Brown Tusser</div>
                </div>
                <div id="fa_10" onclick="textureChange(this.id);" class="content_inner fabric" title="fabric|Dark Gray Plain.jpg">
                    <img src="images/tr_fabric/Dark Gray Plain.jpg" width="60" height="60" />
                    <div>Dark Gray Plain</div>
                </div>
                <div id="fa_11" onclick="textureChange(this.id);" class="content_inner fabric" title="fabric|Dark Gray Tusser.jpg">
                    <img src="images/tr_fabric/Dark Gray Tusser.jpg" width="60" height="60" />
                    <div>Dark Gray Tusser</div>
                </div>
                <div id="fa_12" onclick="textureChange(this.id);" class="content_inner fabric" title="fabric|Gray Check.jpg">
                    <img src="images/tr_fabric/Gray Check.jpg" width="60" height="60" />
                    <div>Gray Check</div>
                </div>
                <div id="fa_13" onclick="textureChange(this.id);" class="content_inner fabric" title="fabric|Gray HB Twill.jpg">
                    <img src="images/tr_fabric/Gray HB Twill.jpg" width="60" height="60" />
                    <div>Gray HB Twill</div>
                </div>
                <div id="fa_14" onclick="textureChange(this.id);" class="content_inner fabric" title="fabric|Gray Stripe.jpg">
                    <img src="images/tr_fabric/Gray Stripe.jpg" width="60" height="60" />
                    <div>Gray Stripe</div>
                </div>
                <div id="fa_15" onclick="textureChange(this.id);" class="content_inner fabric" title="fabric|Light Gray Plain.jpg">
                    <img src="images/tr_fabric/Light Gray Plain.jpg" width="60" height="60" />
                    <div>Light Gray Plain</div>
                </div>
                <div id="fa_16" onclick="textureChange(this.id);" class="content_inner fabric" title="fabric|Navy Check.jpg">
                    <img src="images/tr_fabric/Navy Check.jpg" width="60" height="60" />
                    <div>Navy Check</div>
                </div>
            </div>
        </div>
        <!-- Side Pocket -->
        <div id="panel_sidepocket" class="panel mol_panel" >
            <div class="heading">
                <div class="title">Side Pocket</div>
                <img id="tr_side_close" class="close" src="images/close.png" onclick="closePanel();camerAnim(this.id);"/>
            </div>
            <div class="content">
                <div id="MT-Side_Pocket_Rounded" onclick="meshChange(this.id);" class="content_inner sidepocket active" title="sidepocket|sidepocket-rounded.svg" >
                    <img src="images/tr_sidepocket/sidepocket-rounded.svg" />
                    <div>Rounded</div>
                </div>
                <div id="MT-Side_Pocket_Straight" onclick="meshChange(this.id);" class="content_inner sidepocket" title="sidepocket|sidepocket-straight.svg" >
                    <img src="images/tr_sidepocket/sidepocket-straight.svg" />
                    <div>Straight</div>
                </div>
                <div id="MT-Side_Pocket_Beveled" onclick="meshChange(this.id);" class="content_inner sidepocket" title="sidepocket|sidepocket-beveled.svg">
                    <img src="images/tr_sidepocket/sidepocket-beveled.svg" />
                    <div>Beveled</div>
                </div>
            </div>
        </div>
        <div id="panel_backpocket" class="panel mol_panel" style="min-height:500px">
            <div class="heading">
                <div class="title">back Pocket</div>
                <img id="back_pocket_close" class="close" src="images/close.png" onclick="closePanel();camerAnim(this.id);"/>
            </div>
            <div class="content">
                <div class="toggle" style="max-height: 500px;overflow-y: scroll;">
                    <div class="panel-group" id="accordion">
                        <div class=" panel-default">
                            <div id="toogle-click-div" class="">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Types</a>
                            </div>
                            <div id="collapse1" class="panel-collapse collapse in">
                                <div id="MT-Back_Pocket_no" onclick="meshChange(this.id);" class="content_inner backpocket active" title="backpocket|backpocket.svg" >
                                    <img src="images/tr_backpocket/backpocket.svg" />
                                    <div>No Thanks</div>
                                </div>

                                <div id="MJ-BPoc_V" onclick="meshChange(this.id);" class="content_inner backpocket" title="backpocket|backpocket.svg" >
                                    <img src="images/tr_backpocket/left.svg" />
                                    <div>
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse31">Left Pocket</a>
                                    </div>
                                </div>
                                <div id="collapse31" class="panel-collapse collapse">
                                    <div id="MT-Back_Pocket_L-SingleWelt" onclick="meshChange(this.id);" class="content_inner backpocket" title="backpocket|backpocket.svg" >
                                        <img src="images/tr_backpocket/single-without-button.svg" />
                                        <div>Single Welt</div>
                                    </div>
                                    <div id="MT-Back_Pocket_L-btn" onclick="meshChange(this.id);" class="content_inner backpocket" title="backpocket|backpocket.svg" >
                                        <img src="images/tr_backpocket/without-hook.svg" />
                                        <div>Welt With Button</div>
                                    </div>
                                    <div id="MT-Back_Pocket_L-hook" onclick="meshChange(this.id);" class="content_inner backpocket" title="backpocket|backpocket.svg" >
                                        <img src="images/tr_backpocket/single-with-button.svg" />
                                        <div>Welt With Hook</div>
                                    </div>
                                </div>
                                <div id="MJ-BPoc_U" onclick="meshChange(this.id);" class="content_inner backpocket active" title="backpocket|backpocket.svg" >
                                    <img src="images/tr_backpocket/right.svg" />
                                    <div>
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse32">Right Pocket</a>
                                    </div>
                                </div>
                                <div id="collapse32" class="panel-collapse collapse">
                                    <div id="MT-Back_Pocket_R-SingleWelt" onclick="meshChange(this.id);" class="content_inner backpocket" title="backpocket|backpocket.svg" >
                                        <img src="images/tr_backpocket/single-without-button.svg" />
                                        <div>Single Welt</div>
                                    </div>
                                    <div id="MT-Back_Pocket_R-btn" onclick="meshChange(this.id);" class="content_inner backpocket" title="backpocket|backpocket.svg" >
                                        <img src="images/tr_backpocket/without-hook.svg" />
                                        <div>Welt With Button</div>
                                    </div>
                                    <div id="MT-Back_Pocket_R-hook" onclick="meshChange(this.id);" class="content_inner backpocket" title="backpocket|backpocket.svg" >
                                        <img src="images/tr_backpocket/single-with-button.svg" />
                                        <div>Welt With Hook</div>
                                    </div>
                                </div>
                                <div id="MJ-BPoc_B" onclick="meshChange(this.id);" class="content_inner backpocket" title="backpocket|backpocket.svg" >
                                    <img src="images/tr_backpocket/both.svg" />
                                    <div><a data-toggle="collapse" data-parent="#accordion" href="#collapse33">Both Pocket</a></div>
                                </div>
                                <div id="collapse33" class="panel-collapse collapse">
                                    <div id="MT-Back_Pocket_SingleWelt" onclick="meshChange(this.id);" class="content_inner backpocket" title="backpocket|backpocket.svg" >
                                        <img src="images/tr_backpocket/single-without-button.svg" />
                                        <div>Single Welt</div>
                                    </div>
                                    <div id="MT-Back_Pocket_btn" onclick="meshChange(this.id);" class="content_inner backpocket" title="backpocket|backpocket.svg" >
                                        <img src="images/tr_backpocket/without-hook.svg" />
                                        <div>Welt With Button</div>
                                    </div>
                                    <div id="MT-Back_Pocket_hook" onclick="meshChange(this.id);" class="content_inner backpocket" title="backpocket|backpocket.svg" >
                                        <img src="images/tr_backpocket/single-with-button.svg" />
                                        <div>Welt With Hook</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Seam -->
        <div id="panel_seam" class="panel mol_panel" >
            <div class="heading seam_chinos">
                <div class="title">Seam For Chinos</div>
                <img id="tr_back_close" class="close" src="images/close.png" onclick="closePanel();camerAnim(this.id);"/>
            </div>
            <div class="content seam_chinos">
                <div id="MT-Type_Regular_T" onclick="seamChange(this.id);" class="content_inner backpocket active" title="backpocket|backpocket.svg" >
                    <img src="images/tr_seam/seam-hidden.svg" />
                    <div>Hidden Seam</div>
                </div>
                <div id="MT-Type_Regular_TS" onclick="seamChange(this.id);" class="content_inner backpocket" title="backpocket|backpocket.svg" >
                    <img src="images/tr_seam/seam-fell.svg" />
                    <div>fell Seam</div>
                </div>
            </div>
            <div class="heading seam_rapered">
                <div class="title">Seam For Tapered</div>
                <img id="tr_back_close" class="close" src="images/close.png" onclick="closePanel();camerAnim(this.id);"/>
            </div>
            <div class="content seam_rapered">
                <div id="MT-Fit_Tapered_T" onclick="seamChange(this.id);" class="content_inner backpocket active" title="backpocket|backpocket.svg" >
                    <img src="images/tr_seam/seam-hidden.svg" />
                    <div>Hidden Seam</div>
                </div>
                <div id="MT-Fit_Tapered_TS" onclick="seamChange(this.id);" class="content_inner backpocket" title="backpocket|backpocket.svg" >
                    <img src="images/tr_seam/seam-fell.svg" />
                    <div>fell Seam</div>
                </div>
            </div>
            <div class="heading seam_loose">
                <div class="title">Seam For Loose</div>
                <img id="tr_back_close" class="close" src="images/close.png" onclick="closePanel();camerAnim(this.id);"/>
            </div>
            <div class="content seam_loose">
                <div id="MT-Fit_Loose_T" onclick="seamChange(this.id);" class="content_inner backpocket active" title="backpocket|backpocket.svg" >
                    <img src="images/tr_seam/seam-hidden.svg" />
                    <div>Hidden Seam</div>
                </div>
                <div id="MT-Fit_Loose_TS" onclick="seamChange(this.id);" class="content_inner backpocket" title="backpocket|backpocket.svg" >
                    <img src="images/tr_seam/seam-fell.svg" />
                    <div>fell Seam</div>
                </div>
            </div>
            <div class="heading seam_shorts">
                <div class="title">Seam For Shorts</div>
                <img id="tr_back_close" class="close" src="images/close.png" onclick="closePanel();camerAnim(this.id);"/>
            </div>
            <div class="content seam_shorts">
                <div id="MT-Type_Shorts_T" onclick="seamChange(this.id);" class="content_inner backpocket active" title="backpocket|backpocket.svg" >
                    <img src="images/tr_seam/seam-hidden.svg" />
                    <div>Hidden Seam</div>
                </div>
                <div id="MT-Type_Shorts_TS" onclick="seamChange(this.id);" class="content_inner backpocket" title="backpocket|backpocket.svg" >
                    <img src="images/tr_seam/seam-fell.svg" />
                    <div>fell Seam</div>
                </div>
            </div>
            <div class="heading seam_regular">
                <div class="title">Seam For Regular</div>
                <img id="tr_back_close" class="close" src="images/close.png" onclick="closePanel();camerAnim(this.id);"/>
            </div>
            <div class="content seam_regular">
                <div id="MT-Type_Regular_T" onclick="seamChange(this.id);" class="content_inner backpocket active" title="backpocket|backpocket.svg" >
                    <img src="images/tr_seam/seam-hidden.svg" />
                    <div>Hidden Seam</div>
                </div>
                <div id="MT-Type_Regular_TS" onclick="seamChange(this.id);" class="content_inner backpocket" title="backpocket|backpocket.svg" >
                    <img src="images/tr_seam/seam-fell.svg" />
                    <div>fell Seam</div>
                </div>
            </div>
        </div>
        <!-- coin Pocket -->
        <div id="panel_coinpocket" class="panel mol_panel" >
            <div class="heading">
                <div class="title">Coin Pocket</div>
                <img id="tr_coin_close" class="close" src="images/close.png" onclick="closePanel();camerAnim(this.id);"/>
            </div>
            <div class="content">
                <div id="MT-Coin_Pocket_nothanks" onclick="meshChange(this.id);" class="content_inner coinpocket active" title="coinpocket|coinpocket-no.svg" >
                    <img src="images/tr_coinpocket/coinpocket-no.svg" />
                    <div>No Thanks</div>
                </div>
                <div id="MT-Coin_Pocket" onclick="meshChange(this.id);" class="content_inner coinpocket" title="coinpocket|coinpocket-yes.svg" >
                    <img src="images/tr_coinpocket/coinpocket-yes.svg" />
                    <div>Yes</div>
                </div>
            </div>
        </div>
        <!-- Button -->
        <div id="panel_button" class="panel mol_panel" >
            <div class="heading">
                <div class="title">Button</div>
                <img class="close" src="images/close.png" onclick="closePanel()"/>
            </div>
            <div class="content" style="overflow-y:scroll;height:500px;">
                <div id="f1f4e5" onclick="buttonColor(this.id);" class="content_inner button active" title="button|Sorrento-white.png" >
                    <img src="images/tr_button/Sorrento-white.png" />
                    <div>Sorrento white</div>
                </div>
                <div id="161817" onclick="buttonColor(this.id);" class="content_inner button" title="button|Sorrento-black.png" >
                    <img src="images/tr_button/Sorrento-black.png" />
                    <div>Sorrento black</div>
                </div>
                <div id="f27597" onclick="buttonColor(this.id);" class="content_inner button" title="button|Langa-Cerise.png" >
                    <img src="images/tr_button/Langa-Cerise.png" />
                    <div>Langa Cerise</div>
                </div>
                <div id="ece1d5" onclick="buttonColor(this.id);" class="content_inner button" title="button|Tineo-Ivory.png" >
                    <img src="images/tr_button/Tineo-Ivory.png" />
                    <div>Tineo Ivory</div>
                </div>
                <div id="55161e" onclick="buttonColor(this.id);" class="content_inner button" title="button|Coria-Wine.png" >
                    <img src="images/tr_button/Coria-Wine.png" />
                    <div>Coria Wine</div>
                </div>
                <div id="8a5c61" onclick="buttonColor(this.id);" class="content_inner button" title="button|Sorrento-maroon.png" >
                    <img src="images/tr_button/Sorrento-maroon.png" />
                    <div>Sorrento maroon</div>
                </div>
                <div id="243a2e" onclick="buttonColor(this.id);" class="content_inner button" title="button|Sorrento-military green.png" >
                    <img src="images/tr_button/Sorrento-military green.png" />
                    <div>Sorrento-military green</div>
                </div>
                <div id="ae89b4" onclick="buttonColor(this.id);" class="content_inner button" title="button|Langa-Light Purple.png" >
                    <img src="images/tr_button/Langa-Light Purple.png" />
                    <div>Langa-Light Purple</div>
                </div>
                <div id="707370" onclick="buttonColor(this.id);" class="content_inner button" title="button|Sorrento-light gray.png" >
                    <img src="images/tr_button/Sorrento-light gray.png" />
                    <div>Sorrento-light gray</div>
                </div>
                <div id="4a67af" onclick="buttonColor(this.id);" class="content_inner button" title="button|Langa-Blue.png" >
                    <img src="images/tr_button/Langa-Blue.png" />
                    <div>Langa-Blue</div>
                </div>
                <div id="2b564d" onclick="buttonColor(this.id);" class="content_inner button" title="button|Langa-Green.png" >
                    <img src="images/tr_button/Langa-Green.png" />
                    <div>Langa-Green</div>
                </div>
                <div id="21204c" onclick="buttonColor(this.id);" class="content_inner button" title="button|Coria-Dark Blue.png" >
                    <img src="images/tr_button/Coria-Dark Blue.png" />
                    <div>Coria-Dark Blue</div>
                </div>
                <div id="1b1a1b" onclick="buttonColor(this.id);" class="content_inner button" title="button|Coria-Black.png" >
                    <img src="images/tr_button/Coria-Black.png" />
                    <div>Coria-Black</div>
                </div>
                <div id="772326" onclick="buttonColor(this.id);" class="content_inner button" title="button|Coria-Wine1.png" >
                    <img src="images/tr_button/Coria-Wine1.png" />
                    <div>Coria-Wine1</div>
                </div>
                <div id="bf653b" onclick="buttonColor(this.id);" class="content_inner button" title="button|Langa-Orange.png" >
                    <img src="images/tr_button/Langa-Orange.png" />
                    <div>Langa-Orange</div>
                </div>
                <div id="e7dcd1" onclick="buttonColor(this.id);" class="content_inner button" title="button|Girona-Ivory.png" >
                    <img src="images/tr_button/Girona-Ivory.png" />
                    <div>Girona-Ivory</div>
                </div>
                <div id="9ba0a6" onclick="buttonColor(this.id);" class="content_inner button" title="button|Girona-Light Gray.png" >
                    <img src="images/tr_button/Girona-Light Gray.png" />
                    <div>Girona-Light Gray</div>
                </div>
                <div id="3e2a22" onclick="buttonColor(this.id);" class="content_inner button" title="button|Coria-Brown.png" >
                    <img src="images/tr_button/Coria-Brown.png" />
                    <div>Coria-Brown</div>
                </div>
                <div id="06082d" onclick="buttonColor(this.id);" class="content_inner button" title="button|Coria-Dark Blue1.png" >
                    <img src="images/tr_button/Coria-Dark Blue1.png" />
                    <div>Coria-Dark Blue1</div>
                </div>
                <div id="72275a" onclick="buttonColor(this.id);" class="content_inner button" title="button|Langa-Dark Purple.png" >
                    <img src="images/tr_button/Langa-Dark Purple.png" />
                    <div>Langa-Dark Purple</div>
                </div>
                <div id="61b53a" onclick="buttonColor(this.id);" class="content_inner button" title="button|Langa-Lime.png" >
                    <img src="images/tr_button/Langa-Lime.png" />
                    <div>Langa-Lime</div>
                </div>
                <div id="8d8c8c" onclick="buttonColor(this.id);" class="content_inner button" title="button|Silver-(Push).png" >
                    <img src="images/tr_button/Silver-(Push).png" />
                    <div>Silver-(Push)</div>
                </div>
                <div id="a89860" onclick="buttonColor(this.id);" class="content_inner button" title="button|Gold-(Push).png" >
                    <img src="images/tr_button/Gold-(Push).png" />
                    <div>Gold-(Push)</div>
                </div>
                <div id="61b53a" onclick="buttonColor(this.id);" class="content_inner button" title="button|Langa-Lime.png" >
                    <img src="images/tr_button/Langa-Lime.png" />
                    <div>Langa-Lime</div>
                </div>
                <div id="5b5d62" onclick="buttonColor(this.id);" class="content_inner button" title="button|Black-(Push).png" >
                    <img src="images/tr_button/Black-(Push).png" />
                    <div>Black-(Push)</div>
                </div>
                <div id="b1ae99" onclick="buttonColor(this.id);" class="content_inner button" title="button|Ivory-(Push).png" >
                    <img src="images/tr_button/Ivory-(Push).png" />
                    <div>Ivory-(Push)</div>
                </div>
            </div>
        </div>
        <!-- Thread -->
        <div id="panel_thread" class="panel mol_panel" >
            <div class="heading">
                <div class="title">Thread</div>
                <img class="close" src="images/close.png" onclick="closePanel()"/>
            </div>
            <div class="content" style="overflow-y:scroll;height:500px;">
                <div id="e7e8e7" onclick="threadColor(this.id);" class="content_inner thread active" title="thread|White.png" >
                    <img src="images/tr_thread/White.png" />
                    <div>White</div>
                </div>
                <div id="c0c0bb" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Cream White.png" >
                    <img src="images/tr_thread/Cream White.png" />
                    <div>Cream White</div>
                </div>
                <div id="f5d84b" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Yellow.png" >
                    <img src="images/tr_thread/Yellow.png" />
                    <div>Yellow</div>
                </div>
                <div id="d8e09f" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Bright yellow.png" >
                    <img src="images/tr_thread/Bright yellow.png" />
                    <div>Bright yellow</div>
                </div>
                <div id="e09544" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Orange.png" >
                    <img src="images/tr_thread/Orange.png" />
                    <div>Orange</div>
                </div>
                <div id="d3a5b2" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Pink.png" >
                    <img src="images/tr_thread/Pink.png" />
                    <div>Pink</div>
                </div>
                <div id="c14d70" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Hot Pink.png" >
                    <img src="images/tr_thread/Hot Pink.png" />
                    <div>Hot Pink</div>
                </div>
                <div id="b64243" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Cherry Red.png" >
                    <img src="images/tr_thread/Cherry Red.png" />
                    <div>Cherry Red</div>
                </div>
                <div id="872d2d" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Deep Red.png" >
                    <img src="images/tr_thread/Deep Red.png" />
                    <div>Deep Red</div>
                </div>
                <div id="7f5450" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Mauve.png" >
                    <img src="images/tr_thread/Mauve.png" />
                    <div>Mauve</div>
                </div>
                <div id="5f3c49" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Deep Purple.png" >
                    <img src="images/tr_thread/Deep Purple.png" />
                    <div>Deep Purple</div>
                </div>
                <div id="4c2126" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Bordeaux Red.png" >
                    <img src="images/tr_thread/Bordeaux Red.png" />
                    <div>Bordeaux Red</div>
                </div>
                <div id="6e4e7d" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Purple.png" >
                    <img src="images/tr_thread/Purple.png" />
                    <div>Purple</div>
                </div>
                <div id="634572" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Distinct Purple.png" >
                    <img src="images/tr_thread/Distinct Purple.png" />
                    <div>Distinct Purple</div>
                </div>
                <div id="ab8eaf" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Lavender.png" >
                    <img src="images/tr_thread/Lavender.png" />
                    <div>Lavender</div>
                </div>
                <div id="7aa353" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Bright Green.png" >
                    <img src="images/tr_thread/Bright Green.png" />
                    <div>Bright Green</div>
                </div>
                <div id="6d9045" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Lime.png" >
                    <img src="images/tr_thread/Lime.png" />
                    <div>Lime</div>
                </div>
                <div id="466b4c" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Racing Green.png" >
                    <img src="images/tr_thread/Racing Green.png" />
                    <div>Racing Green</div>
                </div>
                <div id="3a392c" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Olive Green.png" >
                    <img src="images/tr_thread/Olive Green.png" />
                    <div>Olive Green</div>
                </div>
                <div id="508294" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Deep Turqouise.png" >
                    <img src="images/tr_thread/Deep Turqouise.png" />
                    <div>Deep Turqouise</div>
                </div>
                <div id="8693b0" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Baby Blue.png" >
                    <img src="images/tr_thread/Baby Blue.png" />
                    <div>Baby Blue.png</div>
                </div>
                <div id="7086a6" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Sky Blue.png" >
                    <img src="images/tr_thread/Sky Blue.png" />
                    <div>Sky Blue.png</div>
                </div>
                <div id="455780" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Azure Blue.png" >
                    <img src="images/tr_thread/Azure Blue.png" />
                    <div>Azure Blue.png</div>
                </div>
                <div id="2b2e3f" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Ocean Blue.png" >
                    <img src="images/tr_thread/Ocean Blue.png" />
                    <div>Ocean Blue.png</div>
                </div>
                <div id="747270" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Silver Gray.png" >
                    <img src="images/tr_thread/Silver Gray.png" />
                    <div>Silver Gray.png</div>
                </div>
                <div id="62615e" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Steel Gray.png" >
                    <img src="images/tr_thread/Steel Gray.png" />
                    <div>Steel Gray.png</div>
                </div>
                <div id="564b3c" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Medium Brown.png" >
                    <img src="images/tr_thread/Medium Brown.png" />
                    <div>Medium Brown.png</div>
                </div>
                <div id="27231e" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Coffee.png" >
                    <img src="images/tr_thread/Coffee.png" />
                    <div>Coffee.png</div>
                </div>
                <div id="1d1e1d" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Black.png" >
                    <img src="images/tr_thread/Black.png" />
                    <div>Black.png</div>
                </div>
            </div>
        </div>
    </div>
</body>
<script>
    var selectedtype = "regular";
    var selectedfabric = "fa_0";
    var selectedfit = "tapered";
    var selectedsidepocketval = "rounded";
    var selectedbackpocket = "both_single_welt_with_hook";
    var selectedseam = "hidden_seam";
    var selectedcoinpocket = "no";
    var selectedbutton = "f1f4e5";
    var selectedthread = "c0c0bb";
</script>
<script src="js/libs/jquery-1.11.3.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="js/libs/uiblock.js"></script>
<script src="js/libs/TweenMax.js"></script>
<script src="js/libs/three.min.js"></script>
<script src="js/libs/OrbitControls.js"></script>
<script src="js/libs/lzma.js"></script>
<script src="js/libs/ctm.js"></script>
<script src="js/libs/CTMLoader.js"></script>
<script src="js/m-Trouser_JSON.js"></script>
<script src="js/m-Trouser.js"></script>
<script src="js/m-Trouser_Cam.js"></script>
<script>
    $( document ).ready(function() {
        console.log( "ready!" );
    });
    $(function(){
        $( ".content_inner" ).click(function() {
            var container_title = $(this).attr("title");
            var title = container_title.split("|");
            var container = title[0];
            var image = title[1];
            var src = "images/tr_"+container+"/"+image;
            $("."+container).removeClass("active");
            $("#img_"+container).attr("src",src);
            $(this).addClass("active");
            if(container == "type" || container == "fit")
            {
                $(".fit").removeClass("seamer");
                $(".type").removeClass("seamer");
                $(this).addClass("seamer");
            }
        });
    })
    function showPanel(id)
    {
        $(".mol_panel").css("display","none");
        $("#" + id).css("display","block");
        if(id == "panel_seam")
        {
            var eleid = $(".seamer.active").attr("id");
            if(eleid == "MT-Type_Regular")
            {
                $(".seam_chinos").show();
                $(".seam_rapered").hide();
                $(".seam_loose").hide();
                $(".seam_shorts").hide();
                $(".seam_regular").hide();
            }
            if(eleid == "MT-Type_Shorts")
            {
                $(".seam_chinos").hide();
                $(".seam_rapered").hide();
                $(".seam_loose").hide();
                $(".seam_shorts").show();
                $(".seam_regular").hide();
            }
            if(eleid == "MT-Fit_Tapered")
            {
                $(".seam_chinos").hide();
                $(".seam_rapered").show();
                $(".seam_loose").hide();
                $(".seam_shorts").hide();
                $(".seam_regular").hide();
            }
            if(eleid == "MT-Fit_Regular")
            {
                $(".seam_chinos").hide();
                $(".seam_rapered").hide();
                $(".seam_loose").hide();
                $(".seam_shorts").hide();
                $(".seam_regular").show();
            }
            if(eleid == "MT-Fit_Loose")
            {
                $(".seam_chinos").hide();
                $(".seam_rapered").hide();
                $(".seam_loose").show();
                $(".seam_shorts").hide();
                $(".seam_regular").hide();
            }
        }
    }
    function closePanel()
    {
        $(".mol_panel").css("display","none");
        controls.target = new THREE.Vector3(0, 0, 0);
        var a= cube.position.x;
        var b = cube.position.z;
        var c = cube.position.y;
        TweenMax.to(cube.position,2,{x:0,z:0,y:0,onUpdate:function(){

        }});
        var x = camera.position.x;
        var z = camera.position.z;
        var y = camera.position.y;
        TweenMax.to(camera.position,2,{x:40,z:40,y:10,onUpdate:function(){
            camera.updateProjectionMatrix();
            camera.lookAt(scene.position);
        }});
    }
    function addToCart(){
        var product_name = "Trouser"+Math.floor(Math.random()*10000);
        var user_id = $("#user_id").val();
        var url = "admin/api/orderProcess.php";
        $.post(url,{"type":"addToCart_tr","product_type":"men_trouser","product_name":product_name,"product_price":"450"
        ,"user_id":user_id,"quantity":"1","total_amount":"450","tr_type":selectedtype,"tr_fabric":selectedfabric,
        "tr_fit":selectedfit,"tr_sidepocket":selectedsidepocketval,"tr_backpocket":selectedbackpocket,
        "tr_seam":selectedseam,"tr_coinpocket":selectedcoinpocket,"tr_button_color":selectedbutton,
        "tr_thread_color":selectedthread},function(data){
            var Status = data.Status;
            if(Status == "Success"){
                showCartCount(data.cartCount);
                window.location="cart.php";
            }
        }).fail(function(){
            $(".modal-title").html("<label style='color:red'>Error Occured</label>");
            $(".modal-body").html("Server Error !!! Please Try After Some Time...");
            $("#modal").modal("show");
        });
    }
    function getCartCount(){
        var user_id = $("#user_id").val();
        var url = "admin/api/orderProcess.php";
        $.post(url,{"type":"getCartCount","user_id":user_id},function(data){
            var Status = data.Status;
            if(Status == "Success"){
                showCartCount(data.count);
            }
        }).fail(function(){
            $(".modal-title").html("<label style='color:red'>Error Occured</label>");
            $(".modal-body").html("Server Error !!! Please Try After Some Time...");
            $("#modal").modal("show");
        });
    }
    function showCartCount(count){
        $("#cartCount").html(count);
    }
    function opencart(){
        window.location="cart.php";
    }
    getCartCount();
</script>
<input type="button" value="Add to Cart" onclick="addToCart()" class="cartbtn" />
<div id="modal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body">
                <p>Some text in the modal.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<?php include("footer2.php"); ?>